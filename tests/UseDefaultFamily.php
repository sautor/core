<?php

namespace Sautor\Core\Tests;

use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Inscricao;
use Sautor\Core\Models\Pessoa;
use Sautor\Core\Models\Relacao;

use function Sautor\anoLetivo;

trait UseDefaultFamily
{
    private $admin = null;

    private $seePeople = null;

    private $managePeople = null;

    private $manageGroups = null;

    private $father = null;

    private $mother = null;

    private $son = null;

    private $daughter = null;

    private $ze = null;

    private $topGroupAdmin = null;

    private $middleGroupAdmin = null;

    private $bottomGroupAdmin = null;

    private $topGroupMember = null;

    private $middleGroupMember = null;

    private $bottomGroupMember = null;

    private $topGroup = null;

    private $middleGroup = null;

    private $bottomGroup = null;

    protected function setUpDefaultFamily()
    {
        $this->createUsers();
        $this->createRelationships();
        $this->createGroups();
        $this->createRegistrations();
        $this->assignPermissions();
    }

    private function createUsers(): void
    {
        [
            $this->admin,
            $this->seePeople,
            $this->managePeople,
            $this->manageGroups,
            $this->father,
            $this->mother,
            $this->son,
            $this->daughter,
            $this->topGroupAdmin,
            $this->middleGroupAdmin,
            $this->bottomGroupAdmin,
            $this->topGroupMember,
            $this->middleGroupMember,
            $this->bottomGroupMember] = Pessoa::factory()->count(14)->create();

        $this->ze = Pessoa::factory()->create([
            'nome' => 'José de '.fake()->lastName.' e '.fake()->lastName,
        ]);
    }

    private function createRelationships(): void
    {
        $pairs = [
            [$this->father, $this->son],
            [$this->father, $this->daughter],
            [$this->mother, $this->son],
            [$this->mother, $this->daughter],
        ];
        foreach ($pairs as $pair) {
            Relacao::create([
                'pessoa1_id' => $pair[0]->id,
                'pessoa2_id' => $pair[1]->id,
                'relacao' => 'pai',
            ]);
        }
    }

    private function createGroups(): void
    {
        $this->topGroup = Grupo::factory()->create();
        $this->middleGroup = Grupo::factory()->create([
            'grupo_pai' => $this->topGroup->id,
        ]);
        $this->bottomGroup = Grupo::factory()->create([
            'grupo_pai' => $this->middleGroup->id,
        ]);
    }

    private function createRegistrations(): void
    {
        $registrations = [
            [$this->topGroupAdmin, $this->topGroup, true],
            [$this->middleGroupAdmin, $this->middleGroup, true],
            [$this->bottomGroupAdmin, $this->bottomGroup, true],
            [$this->topGroupMember, $this->topGroup, false],
            [$this->middleGroupMember, $this->middleGroup, false],
            [$this->bottomGroupMember, $this->bottomGroup, false],
            [$this->son, $this->middleGroup, false],
        ];
        foreach ($registrations as $reg) {
            Inscricao::create([
                'pessoa_id' => $reg[0]->id,
                'grupo_id' => $reg[1]->id,
                'responsavel' => $reg[2],
                'ano_letivo' => count($reg) > 3 && $reg[3] === false ? null : anoLetivo(),
            ]);
        }
    }

    private function assignPermissions(): void
    {
        $this->admin->assignRole('administrador');
        $this->seePeople->givePermissionTo('ver pessoas');
        $this->managePeople->givePermissionTo('gerir pessoas');
        $this->manageGroups->givePermissionTo('gerir grupos');
    }
}
