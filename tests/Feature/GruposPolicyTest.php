<?php

namespace Sautor\Core\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Tests\TestCase;
use Sautor\Core\Tests\UseDefaultFamily;

class GruposPolicyTest extends TestCase
{
    use RefreshDatabase;
    use UseDefaultFamily;

    protected function setUp(): void
    {
        parent::setUp();

        \Artisan::call('permissions:init');

        $this->setUpDefaultFamily();

        // Make middle group invisible
        $this->middleGroup->update([
            'visibilidade' => 'oculto',
        ]);
    }

    /**
     * Test grupo policy view.
     *
     * @return void
     */
    public function testView()
    {
        // Top and bottom groups should be visible by everyone
        foreach (['admin', 'manageGroups', 'seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze',
            'topGroupMember', 'middleGroupMember', 'bottomGroupMember',
            'topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin', ] as $user) {
            $this->assertTrue($this->{$user}->can('view', $this->topGroup), $user.' -> Top');
            $this->assertTrue($this->{$user}->can('view', $this->bottomGroup), $user.' -> Bottom');
        }

        // Middle can be viewed by admin, managers and members
        foreach (['admin', 'manageGroups', 'son', 'middleGroupMember', 'topGroupAdmin', 'middleGroupAdmin'] as $user) {
            $this->assertTrue($this->{$user}->can('view', $this->middleGroup), $user.' -> Middle');
        }
        // Otherwise can't
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'daughter', 'ze', 'topGroupMember', 'bottomGroupMember', 'bottomGroupAdmin'] as $user) {
            $this->assertFalse($this->{$user}->can('view', $this->middleGroup), $user.' -> Middle');
        }
    }

    /**
     * Test grupo policy details.
     *
     * @return void
     */
    public function testDetails()
    {
        // Admin can view
        $this->assertTrue($this->admin->can('details', $this->topGroup), 'Admin -> Top');
        $this->assertTrue($this->admin->can('details', $this->middleGroup), 'Admin -> Middle');
        $this->assertTrue($this->admin->can('details', $this->bottomGroup), 'Admin -> Bottom');
        // As well as managers and members
        $this->assertTrue($this->manageGroups->can('details', $this->topGroup), 'Manage Groups -> Top');
        $this->assertTrue($this->topGroupAdmin->can('details', $this->topGroup), 'Top Admin -> Top');
        $this->assertTrue($this->topGroupMember->can('details', $this->topGroup), 'Top Member -> Top');
        $this->assertTrue($this->middleGroupAdmin->can('details', $this->middleGroup), 'Middle Admin -> Middle');
        $this->assertTrue($this->middleGroupMember->can('details', $this->middleGroup), 'Middle Member -> Middle');
        $this->assertTrue($this->son->can('details', $this->middleGroup), 'Son -> Middle');
        $this->assertTrue($this->manageGroups->can('details', $this->middleGroup), 'Manage Groups -> Middle');
        $this->assertTrue($this->topGroupAdmin->can('details', $this->middleGroup), 'Top Admin -> Middle');
        $this->assertTrue($this->bottomGroupAdmin->can('details', $this->bottomGroup), 'Bottom Admin -> Bottom');
        $this->assertTrue($this->bottomGroupMember->can('details', $this->bottomGroup), 'Bottom Member -> Bottom');
        $this->assertTrue($this->manageGroups->can('details', $this->bottomGroup), 'Manage Groups -> Bottom');
        $this->assertTrue($this->topGroupAdmin->can('details', $this->bottomGroup), 'Top Admin -> Bottom');
        $this->assertTrue($this->middleGroupAdmin->can('details', $this->bottomGroup), 'Middle Admin -> Bottom');

        // Can not otherwise
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'middleGroupMember',
            'bottomGroupMember', 'middleGroupAdmin', 'bottomGroupAdmin', ] as $user) {
            $this->assertFalse($this->{$user}->can('details', $this->topGroup), $user.' -> Top');
        }
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'daughter', 'ze', 'topGroupMember',
            'bottomGroupMember', 'bottomGroupAdmin', ] as $user) {
            $this->assertFalse($this->{$user}->can('details', $this->middleGroup), $user.' -> Middle');
        }
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', ] as $user) {
            $this->assertFalse($this->{$user}->can('details', $this->bottomGroup), $user.' -> Bottom');
        }
    }

    /**
     * Test grupo policy create.
     *
     * @return void
     */
    public function testCreate()
    {
        // Can if admin
        $this->assertTrue($this->admin->can('create', Grupo::class), 'Admin');
        $this->assertTrue($this->manageGroups->can('create', Grupo::class), 'Manage groups');

        // Can if manage any group
        foreach (['topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin'] as $manager) {
            $this->assertTrue($this->{$manager}->can('create', Grupo::class), $manager);
        }

        // Can NOT else
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', 'bottomGroupMember', ] as $manager) {
            $this->assertFalse($this->{$manager}->can('create', Grupo::class), $manager);
        }
    }

    /**
     * Test grupo policy create.
     *
     * @return void
     */
    public function testUpdate()
    {
        // Can if manager
        foreach (['admin', 'manageGroups', 'topGroupAdmin'] as $manager) {
            $this->assertTrue($this->{$manager}->can('update', $this->topGroup), $manager.' -> Top');
        }
        foreach (['admin', 'manageGroups', 'topGroupAdmin', 'middleGroupAdmin'] as $manager) {
            $this->assertTrue($this->{$manager}->can('update', $this->middleGroup), $manager.' -> Middle');
        }
        foreach (['admin', 'manageGroups', 'topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin'] as $manager) {
            $this->assertTrue($this->{$manager}->can('update', $this->bottomGroup), $manager.' -> Bottom');
        }
        // Can not otherwise
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', 'bottomGroupMember', 'middleGroupAdmin', 'bottomGroupAdmin', ] as $user) {
            $this->assertFalse($this->{$user}->can('update', $this->topGroup), $user.' -> Top');
        }
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', 'bottomGroupMember', 'bottomGroupAdmin', ] as $user) {
            $this->assertFalse($this->{$user}->can('update', $this->middleGroup), $user.' -> Middle');
        }
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', 'bottomGroupMember', ] as $user) {
            $this->assertFalse($this->{$user}->can('update', $this->bottomGroup), $user.' -> Bottom');
        }
    }

    /**
     * Test grupo policy destroy.
     *
     * @return void
     */
    public function testDelete()
    {
        // Can if admin
        $this->assertTrue($this->admin->can('delete', $this->topGroup), 'Admin -> Top');
        $this->assertTrue($this->admin->can('delete', $this->middleGroup), 'Admin -> Middle');
        $this->assertTrue($this->admin->can('delete', $this->bottomGroup), 'Admin -> Bottom');

        // Can if permission
        $this->assertTrue($this->manageGroups->can('delete', $this->topGroup), 'Manage Groups -> Top');
        $this->assertTrue($this->manageGroups->can('delete', $this->middleGroup), 'Manage Groups -> Middle');
        $this->assertTrue($this->manageGroups->can('delete', $this->bottomGroup), 'Manage Groups -> Bottom');

        // Can NOT else
        foreach (['seePeople', 'managePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember',
            'middleGroupMember', 'bottomGroupMember', 'topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin', ] as $user) {
            $this->assertFalse($this->{$user}->can('delete', $this->topGroup), $user.' -> Top');
            $this->assertFalse($this->{$user}->can('delete', $this->middleGroup), $user.' -> Middle');
            $this->assertFalse($this->{$user}->can('delete', $this->bottomGroup), $user.' -> Bottom');
        }
    }
}
