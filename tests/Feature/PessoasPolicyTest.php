<?php

namespace Sautor\Core\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Sautor\Core\Models\Pessoa;
use Sautor\Core\Tests\TestCase;
use Sautor\Core\Tests\UseDefaultFamily;

class PessoasPolicyTest extends TestCase
{
    use RefreshDatabase;
    use UseDefaultFamily;

    protected function setUp(): void
    {
        parent::setUp();

        \Artisan::call('permissions:init');
        $this->setUpDefaultFamily();
    }

    /**
     * Test pessoa policy view.
     *
     * @return void
     */
    public function testView()
    {
        // Can if self
        $this->assertTrue($this->son->can('view', $this->son), 'Son -> Son');

        // Can if admin...
        $this->assertTrue($this->admin->can('view', $this->son), 'Admin -> Son');
        // ...or if can see or manage people
        $this->assertTrue($this->seePeople->can('view', $this->son), 'See people -> Son');
        $this->assertTrue($this->managePeople->can('view', $this->son), 'Manage people -> Son');
        // ...but not the other way round
        $this->assertFalse($this->son->can('view', $this->admin), 'Son -> Admin');
        $this->assertFalse($this->son->can('view', $this->seePeople), 'Son -> See people');
        $this->assertFalse($this->son->can('view', $this->managePeople), 'Son -> Manage people');

        // Can NOT be seen by Zé...
        $this->assertFalse($this->ze->can('view', $this->son), 'Zé -> Son');
        // ...NOR can see Zé
        $this->assertFalse($this->son->can('view', $this->ze), 'Zé -> Son');

        // FAMILY TESTS
        // Can NOT if sibling
        $this->assertFalse($this->daughter->can('view', $this->son), 'Daughter -> Son');
        $this->assertFalse($this->son->can('view', $this->daughter), 'Son -> Daughter');
        // Can NOT if linked
        $this->assertFalse($this->father->can('view', $this->mother), 'Father -> Mother');
        $this->assertFalse($this->mother->can('view', $this->father), 'Mother -> Father');
        // Can if ancestor
        $this->assertTrue($this->father->can('view', $this->son), 'Father -> Son');
        $this->assertTrue($this->mother->can('view', $this->son), 'Mother -> Son');
        $this->assertTrue($this->father->can('view', $this->daughter), 'Father -> Daughter');
        $this->assertTrue($this->mother->can('view', $this->daughter), 'Mother -> Daughter');
        // Can NOT if descendant
        $this->assertFalse($this->son->can('view', $this->father), 'Son -> Father');
        $this->assertFalse($this->son->can('view', $this->mother), 'Son -> Mother');

        // GROUP TEST
        // If I am in the same group, I can view the person
        $this->assertFalse($this->middleGroupMember->can('view', $this->son), 'Middle member -> Son');
        // ...but NOT their family!
        $this->assertFalse($this->middleGroupMember->can('view', $this->daughter), 'Middle member -> Daughter');
        $this->assertFalse($this->middleGroupMember->can('view', $this->father), 'Middle member -> Father');
        $this->assertFalse($this->middleGroupMember->can('view', $this->mother), 'Middle member -> Mother');
        // TODO: Descendant test
        // But I can if the group allows it
        \Setting::set('grupo-'.$this->middleGroup->id.'-view-members-details', true);
        \Setting::save();
        $this->assertTrue($this->middleGroupMember->can('view', $this->son), 'Middle member -> Son (Group allows)');
        // ...but NOT their family!
        $this->assertFalse($this->middleGroupMember->can('view', $this->daughter), 'Middle member -> Daughter (Group allows)');
        $this->assertFalse($this->middleGroupMember->can('view', $this->father), 'Middle member -> Father (Group allows)');
        $this->assertFalse($this->middleGroupMember->can('view', $this->mother), 'Middle member -> Mother (Group allows)');
        // TODO: Descendant test
        // If I manage the group, I can view the person
        $this->assertTrue($this->middleGroupAdmin->can('view', $this->son), 'Middle admin -> Son');
        // ...I can view ancestors
        $this->assertTrue($this->middleGroupAdmin->can('view', $this->father), 'Middle admin -> Father');
        $this->assertTrue($this->middleGroupAdmin->can('view', $this->mother), 'Middle admin -> Mother');
        // ...and I can view descendants
        // TODO: Descendant tests
        // ...but NOT brothers!
        $this->assertFalse($this->middleGroupAdmin->can('view', $this->daughter), 'Middle admin -> Daughter');
        // Same if I manage a group above
        $this->assertTrue($this->topGroupAdmin->can('view', $this->son), 'Top admin -> Son');
        $this->assertTrue($this->topGroupAdmin->can('view', $this->father), 'Top admin -> Father');
        $this->assertTrue($this->topGroupAdmin->can('view', $this->mother), 'Top admin -> Mother');
        $this->assertFalse($this->topGroupAdmin->can('view', $this->daughter), 'Top admin -> Daughter');
        // TODO: Descendant tests
        // But NOT if below!
        $this->assertFalse($this->bottomGroupAdmin->can('view', $this->son), 'Bottom admin -> Son');
        $this->assertFalse($this->bottomGroupAdmin->can('view', $this->father), 'Bottom admin -> Father');
        $this->assertFalse($this->bottomGroupAdmin->can('view', $this->mother), 'Bottom admin -> Mother');
        $this->assertFalse($this->bottomGroupAdmin->can('view', $this->daughter), 'Bottom admin -> Daughter');
        // TODO: Descendant tests
    }

    /**
     * Test pessoa policy update.
     *
     * @return void
     */
    public function testUpdate()
    {
        // Can if self
        $this->assertTrue($this->son->can('update', $this->son), 'Son -> Son');

        // Can if admin...
        $this->assertTrue($this->admin->can('update', $this->son), 'Admin -> Son');
        // ...or if can manage people
        $this->assertTrue($this->managePeople->can('update', $this->son), 'Manage people -> Son');
        // ...but not the other way round
        $this->assertFalse($this->son->can('update', $this->admin), 'Son -> Admin');
        $this->assertFalse($this->son->can('update', $this->managePeople), 'Son -> Manage people');
        // ...or if I can only see people
        $this->assertFalse($this->seePeople->can('update', $this->son), 'See people -> Son');

        // Can NOT be updated by Zé...
        $this->assertFalse($this->ze->can('update', $this->son), 'Zé -> Son');
        // ...NOR can update Zé
        $this->assertFalse($this->son->can('update', $this->ze), 'Zé -> Son');

        // FAMILY TESTS
        // Can NOT if sibling
        $this->assertFalse($this->daughter->can('update', $this->son), 'Daughter -> Son');
        $this->assertFalse($this->son->can('update', $this->daughter), 'Son -> Daughter');
        // Can NOT if linked
        $this->assertFalse($this->father->can('update', $this->mother), 'Father -> Mother');
        $this->assertFalse($this->mother->can('update', $this->father), 'Mother -> Father');
        // Can if ancestor
        $this->assertTrue($this->father->can('update', $this->son), 'Father -> Son');
        $this->assertTrue($this->mother->can('update', $this->son), 'Mother -> Son');
        $this->assertTrue($this->father->can('update', $this->daughter), 'Father -> Daughter');
        $this->assertTrue($this->mother->can('update', $this->daughter), 'Mother -> Daughter');
        // Can NOT if descendant
        $this->assertFalse($this->son->can('update', $this->father), 'Son -> Father');
        $this->assertFalse($this->son->can('update', $this->mother), 'Son -> Mother');

        // GROUP TEST
        // If I am in the same group, I can NOT update the person
        $this->assertFalse($this->middleGroupMember->can('update', $this->son), 'Middle member -> Son');
        // ...and NOT their family!
        $this->assertFalse($this->middleGroupMember->can('update', $this->daughter), 'Middle member -> Daughter');
        $this->assertFalse($this->middleGroupMember->can('update', $this->father), 'Middle member -> Father');
        $this->assertFalse($this->middleGroupMember->can('update', $this->mother), 'Middle member -> Mother');
        // TODO: Descendant test
        // If I manage the group, I can update the person
        $this->assertTrue($this->middleGroupAdmin->can('update', $this->son), 'Middle admin -> Son');
        // ...I can update ancestors
        $this->assertTrue($this->middleGroupAdmin->can('update', $this->father), 'Middle admin -> Father');
        $this->assertTrue($this->middleGroupAdmin->can('update', $this->mother), 'Middle admin -> Mother');
        // ...and I can update descendants
        // TODO: Descendant tests
        // ...but NOT brothers!
        $this->assertFalse($this->middleGroupAdmin->can('update', $this->daughter), 'Middle admin -> Daughter');
        // Same if I manage a group above
        $this->assertTrue($this->topGroupAdmin->can('update', $this->son), 'Top admin -> Son');
        $this->assertTrue($this->topGroupAdmin->can('update', $this->father), 'Top admin -> Father');
        $this->assertTrue($this->topGroupAdmin->can('update', $this->mother), 'Top admin -> Mother');
        $this->assertFalse($this->topGroupAdmin->can('update', $this->daughter), 'Top admin -> Daughter');
        // TODO: Descendant tests
        // But NOT if below!
        $this->assertFalse($this->bottomGroupAdmin->can('update', $this->son), 'Bottom admin -> Son');
        $this->assertFalse($this->bottomGroupAdmin->can('update', $this->father), 'Bottom admin -> Father');
        $this->assertFalse($this->bottomGroupAdmin->can('update', $this->mother), 'Bottom admin -> Mother');
        $this->assertFalse($this->bottomGroupAdmin->can('update', $this->daughter), 'Bottom admin -> Daughter');
        // TODO: Descendant tests
    }

    /**
     * Test pessoa policy store.
     *
     * @return void
     */
    public function testStore()
    {
        // Can if admin
        $this->assertTrue($this->admin->can('store', Pessoa::class), 'Admin');

        // Can if manages people
        $this->assertTrue($this->managePeople->can('store', Pessoa::class), 'Manage people');

        // Can if manage any group
        foreach (['manageGroups', 'topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin'] as $manager) {
            $this->assertTrue($this->{$manager}->can('store', Pessoa::class), $manager);
        }

        // Can NOT else
        foreach (['seePeople', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember', 'middleGroupMember', 'bottomGroupMember'] as $manager) {
            $this->assertFalse($this->{$manager}->can('store', Pessoa::class), $manager);
        }
    }

    /**
     * Test pessoa policy destroy.
     *
     * @return void
     */
    public function testDestroy()
    {
        // Can if admin
        $this->assertTrue($this->admin->can('destroy', $this->ze), 'Admin');

        // Can if manage people
        $this->assertTrue($this->managePeople->can('destroy', $this->ze), 'Manage people');

        // Can NOT else
        foreach (['seePeople', 'manageGroups', 'father', 'mother', 'son', 'daughter', 'ze', 'topGroupMember', 'middleGroupMember',
            'bottomGroupMember', 'topGroupAdmin', 'middleGroupAdmin', 'bottomGroupAdmin', ] as $manager) {
            $this->assertFalse($this->{$manager}->can('destroy', $this->ze), $manager);
        }

        // BUT admin can not destroy self!
        $this->assertFalse($this->admin->can('destroy', $this->admin), 'Admin -> Admin');
    }
}
