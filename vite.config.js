import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'

import vue from '@vitejs/plugin-vue2'

export default defineConfig({
  plugins: [
    laravel([
      'resources/sass/main.scss',
      'resources/sass/errors.scss',
      'resources/sass/maintenance.scss',
      'resources/js/app.js'
    ]),
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          includeAbsolute: false
        }
      }
    })
  ],
  resolve: {
    alias: {
      vue: 'vue/dist/vue.esm.js'
    }
  },
  build: {
    rollupOptions: {
      output: {
        entryFileNames: 'assets/[name].js',
        chunkFileNames: 'assets/[name].js',
        assetFileNames: 'assets/[name].[ext]'
      }
    }
  }
})
