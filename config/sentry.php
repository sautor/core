<?php

return [
    'dsn' => env('SENTRY_LARAVEL_DSN'),

    // capture release as git sha
    // 'release' => json_decode(file_get_contents( __DIR__.'/../composer.json'))->version,

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,
];
