module.exports = {
  'extends': [
    'standard',
    'plugin:vue/essential'
  ],
  'rules': {
    'vue/script-indent': ['error', 2, { 'baseIndent': 1 }]
  },
  'overrides': [
    {
      'files': ['*.vue'],
      'rules': {
        'indent': 'off'
      }
    }
  ]
}
