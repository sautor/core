<?php

Route::get('/', 'GruposController@index')->name('index');
Route::get('noticias', 'GruposController@noticias')->name('noticias');
Route::get('grupos', 'GruposController@grupos')->name('grupos');

Route::middleware('auth')->group(function () {
    Route::post('/', 'GruposController@store')->name('store');
    Route::put('grupos', 'GruposController@restore')->name('restore');
    Route::get('membros', 'GruposController@membros')->name('membros');
    Route::get('personalizar', 'GruposController@personalizar')->name('personalizar');
    Route::post('personalizar', 'GruposController@postPersonalizar')->name('personalizar.update');
    Route::get('definicoes', 'GruposController@definicoes')->name('definicoes');
    Route::post('definicoes', 'GruposController@postDefinicoes')->name('definicoes.update');
    Route::delete('/', 'GruposController@delete')->name('delete');

    Route::prefix('noticias')->name('noticias.')->namespace('Grupos')->group(function () {
        Route::get('admin', 'NoticiasController@index')->name('admin');
        Route::get('create', 'NoticiasController@create')->name('create');
        Route::get('{noticia}/edit', 'NoticiasController@edit')->name('edit');
        Route::delete('{noticia}', 'NoticiasController@destroy')->name('destroy');
    });

    Route::prefix('paginas')->name('paginas.')->namespace('Grupos')->group(function () {
        Route::get('/', 'PaginasController@index')->name('admin');
        Route::get('create', 'PaginasController@create')->name('create');
        Route::get('{pagina}/edit', 'PaginasController@edit')->name('edit');
        Route::delete('{pagina}', 'PaginasController@destroy')->name('destroy');
    });

    Route::prefix('inscricoes')->name('inscricoes.')->group(function () {
        Route::post('/', 'InscricoesController@store')->name('store');
        Route::delete('{inscricao}', 'InscricoesController@destroy')->name('destroy');
    });
});

Route::get('{slug}', 'Grupos\PaginasController@show')->where('slug', '(.*)')->name('paginas.show');
