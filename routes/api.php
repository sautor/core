<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Hash;
use Sautor\Core\Models\Grupo;

Route::middleware(['auth:api', 'scope:perfil'])->get('/eu', 'EuController@apiGet');

Route::get('pesquisar', 'Api\SearchController')->name('api.search');

Route::name('files.')->prefix('ficheiros')->group(function () {
    Route::get('{path}', 'Api\FileController@show')->where('path', '(.*)')->name('show');
    Route::post('{path}', 'Api\FileController@upload')->where('path', '(.*)')->name('upload');
});

Route::name('api.2fa.')->prefix('api/2fa')->middleware(['auth:api'])->group(function () {
    Route::post('/', 'EuController@enable2FA')->name('enable');
    Route::post('confirm', 'EuController@confirm2FA')->name('confirm');
    Route::delete('/', 'EuController@disable2FA')->name('disable');
});

Route::namespace('Api')->name('api.')->middleware(['auth:api'])->group(function () {
    Route::post('pessoas/unique/{column}', 'PessoasController@unique')->name('unique');
    Route::get('pessoas/nic/{nic}', 'PessoasController@fromNic')->name('from-nic');

    Route::name('grupos.')->prefix('grupos')->middleware('scope:grupos')->group(function () {
        Route::get('geridos', 'GruposController@managed')->name('geridos');

        Route::name('grupo.')->prefix('{grupo}')->group(function () {
            //
        });
    });

    Route::name('noticias.')->prefix('noticias')->middleware('scope:noticias')->group(function () {
        Route::post('/', 'NoticiasController@store')->name('store');
        Route::put('{noticia}', 'NoticiasController@update')->name('update');
    });

    Route::name('paginas.')->prefix('paginas')->middleware('scope:paginas')->group(function () {
        Route::post('/', 'PaginasController@store')->name('store');
        Route::put('{pagina}', 'PaginasController@update')->name('update');
        Route::get('valid-slug', 'PaginasController@validSlug')->name('valid-slug');
        Route::get('modelos/{grupo?}', 'PaginasController@templates')->name('modelos');
    });

    Route::name('eventos.')->prefix('eventos')->middleware('scope:eventos')->group(function () {
        Route::post('/', 'EventosController@store')->name('store');
        Route::put('{evento}', 'EventosController@update')->name('update');
        Route::delete('{evento}', 'EventosController@destroy')->name('destroy');
    });
});

Route::domain('{grupo:dominio}')->where(['grupo' => '[a-z0-9.]+'])->name('api.')->group(function () {
    Route::get('/domain-check', function (Grupo $grupo) {
        $response = Response::make(Hash::make($grupo->id.$grupo->dominio));
        $response->header('Content-Type', 'text/plain');

        return $response;
    })->name('domain-check');
});
