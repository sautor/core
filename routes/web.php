<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('www.'.config('app.domain'))->group(function () {
    Route::get('{slug?}', function ($slug = null) {
        return redirect(config('app.url').($slug ?? ''), 301);
    })->where('slug', '(.*)');
});

// Authentication Routes
Route::get('entrar', 'Auth\LoginController@showLoginForm')->middleware('guest')->name('login');
Route::post('entrar', 'Auth\LoginController@login')->middleware('guest');
Route::post('sair', 'Auth\LoginController@logout')->name('logout');

// Grupos
Route::prefix('grupos/{grupo}')->name('grupo.')->group(__DIR__.'/group.php');

Route::domain(config('app.domain'))->group(function () {
    // Social Auth routes
    Route::namespace('Auth')->prefix('auth/{provider}')->name('social.')->group(function () {
        Route::get('/', 'LoginController@socialRedirect')->name('redirect');
        Route::get('callback', 'LoginController@socialCallback')->name('callback');

        // Social Link routes
        Route::get('link', 'SocialController@link')->name('link')->middleware('auth');
        Route::post('link', 'SocialController@linkConfirm')->name('linkConfirm')->middleware('auth');
        Route::delete('link', 'SocialController@unlink')->name('unlink')->middleware('auth');
    });

    // Password Reset Routes...
    Route::namespace('Auth')->prefix('password')->name('password.')->middleware('guest')->group(function () {
        Route::get('reset', 'ForgotPasswordController@showLinkRequestForm')->name('request');
        Route::post('email', 'ForgotPasswordController@sendResetLinkEmail')->name('email');
        Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('reset');
        Route::post('reset', 'ResetPasswordController@reset');
    });

    Route::middleware('maintenance')->group(function () {
        Route::get('/', 'HomeController@index')->name('home');

        // Noticias
        Route::prefix('noticias')->name('noticias.')->group(function () {
            Route::get('/', 'NoticiasController@index')->name('index');
            Route::get('{noticia}', 'NoticiasController@show')->name('show');
        });

        // Pessoas
        Route::resource('pessoas', 'PessoasController', ['except' => ['index']]);
        Route::resource('pessoas.passos', 'PassosController', ['only' => ['store', 'destroy']]);
        Route::resource('pessoas.consentimentos', 'ConsentimentosController', ['only' => ['store', 'show', 'destroy']]);
        Route::prefix('pessoas')->name('pessoas.')->middleware('auth')->group(function () {
            Route::patch('{pessoa}/foto', 'PessoasController@updatePhoto')->name('updateFoto');
            Route::get('{pessoa}/enviar-link', 'PessoasController@sendPasswordResetLink')->name('sendResetLink');
            Route::post('{pessoa}/relacoes', 'RelacoesController@store')->name('relacoes.store');
            Route::delete('{pessoa}/relacoes/{pessoa2}', 'RelacoesController@destroy')->name('relacoes.destroy');

            Route::post('{pessoa}/permissoes', 'PermissionsController@addPermission')->name('permissions.add');
            Route::delete('{pessoa}/permissoes/{permission}', 'PermissionsController@removePermission')->name('permissions.remove');
            Route::post('{pessoa}/papeis', 'PermissionsController@addRole')->name('permissions.addRole');
            Route::delete('{pessoa}/papeis/{role}', 'PermissionsController@removeRole')->name('permissions.removeRole');
        });

        // Eu
        Route::middleware(['auth'])->prefix('eu')->name('me.')->group(function () {
            Route::get('/', 'EuController@conta')->name('account');
            Route::get('perfil', 'EuController@perfil')->name('profile');
            Route::get('edit', 'EuController@editPerfil')->name('editProfile');
            Route::get('grupos', 'EuController@grupos')->name('groups');
            Route::post('password', 'EuController@changePassword')->name('changePassword');
            Route::get('consentimento', 'ConsentimentosController@showRequest')->name('showConsentRequest');
            Route::post('consentimento', 'ConsentimentosController@storeRequest')->name('storeConsentRequest');
        });

        // Admin
        Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function () {
            Route::get('/', 'AdminController@index')->name('dashboard')->middleware(['permission:ver pessoas|gerir pessoas|gerir grupos|gerir permissões|gerir notícias|gerir páginas|gerir opções|gerir eventos']);
            Route::get('pessoas', 'AdminController@pessoas')->name('people')->middleware(['permission:ver pessoas|gerir pessoas']);
            Route::resource('noticias', 'Admin\\NoticiasController', ['except' => ['show', 'store', 'update']])->middleware(['permission:gerir notícias']);
            Route::resource('paginas', 'Admin\\PaginasController', ['except' => ['show', 'store', 'update']])->middleware(['permission:gerir páginas']);
            Route::get('eventos', 'Admin\\EventosController@index')->name('eventos')->middleware(['permission:gerir eventos']);
            Route::get('permissoes', 'AdminController@permissoes')->name('permissoes')->middleware(['permission:gerir permissões']);

            Route::middleware(['permission:gerir grupos'])->prefix('grupos')->group(function () {
                Route::get('/', 'Admin\\GruposController@index')->name('groups');
                Route::post('/', 'Admin\\GruposController@store')->name('groups.store');
                Route::put('restore', 'Admin\\GruposController@restore')->name('groups.restore');
            });

            Route::middleware(['permission:gerir opções'])->group(function () {
                Route::get('extensoes', 'Admin\\AddonController@index')->name('extensoes');
                Route::post('extensoes/{key}', 'Admin\\AddonController@enable')->name('extensoes.enable');
                Route::delete('extensoes/{key}', 'Admin\\AddonController@disable')->name('extensoes.disable');

                Route::get('personalizar', 'Admin\\OpcoesController@personalizar')->name('personalizar');
                Route::post('personalizar', 'Admin\\OpcoesController@updatePersonalizar')->name('personalizar.update');

                Route::get('redirects', 'AdminController@redirects')->name('redirects');

                Route::get('definicoes', 'Admin\\OpcoesController@definicoes')->name('definicoes');
                Route::post('definicoes', 'Admin\\OpcoesController@updateDefinicoes')->name('definicoes.update');
            });

            Route::middleware(['permission:gerir domínios'])->get('dominios', 'AdminController@domains')->name('domains');
        });

        // 2FA
        Route::view('2fa-required', 'two-factor::notice')->name('2fa.notice');

        // Media
        Route::get('media/{uuid}', 'MediaController@show')->name('media');



        // Page catch all
        Route::get('{slug}', 'PaginasController@show')
            ->where('slug', '(.*)')->name('paginas.show');
    });
});

Route::domain('{grupo}.'.config('app.domain'))->group(function () {
    Route::get('{slug?}', function (Sautor\Core\Models\Grupo $grupo, $slug = null) {
        return redirect(config('app.url').route('grupo.index', $grupo, false).($slug ? '/'.$slug : ''));
    })->where('slug', '(.*)');
});

Route::domain('{grupo:dominio}')->where(['grupo' => '[a-z0-9.]+'])->name('grupo-dominio.')->middleware('maintenance')->group(function () {
    Route::get('noticias/{noticia}', 'GruposController@noticia')->where(['noticia' => '[0-9]+'])->name('noticia');

    require __DIR__.'/group.php';
});
