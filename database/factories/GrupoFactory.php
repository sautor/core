<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Sautor\Core\Models\Grupo;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\Sautor\Core\Models\Grupo>
 */
class GrupoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\Illuminate\Database\Eloquent\Model>
     */
    protected $model = Grupo::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nome' => fake()->company,
            'descricao' => fake()->paragraph,
            'slug' => fake()->slug,
            'url' => fake()->optional()->url,
            'email' => fake()->optional()->companyEmail,
            'visibilidade' => 'visivel',
        ];
    }
}
