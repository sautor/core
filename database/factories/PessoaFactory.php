<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Sautor\Core\Models\Pessoa;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\Sautor\Core\Models\Pessoa>
 */
class PessoaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\Illuminate\Database\Eloquent\Model>
     */
    protected $model = Pessoa::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['male', 'female']);

        return [
            'nic' => fake()->uuid,
            'nome' => fake()->name($gender),
            'email' => fake()->unique()->safeEmail,
            'password' => bcrypt('secret'),
            'morada' => fake()->optional()->address,
            'telefone' => fake()->optional()->phoneNumber,
            'data_nascimento' => fake()->optional()->date(),
            'profissao' => fake()->optional()->jobTitle,
            'nus' => fake()->uuid,
            'nif' => fake()->taxpayerIdentificationNumber,
            'genero' => strtoupper($gender[0]),
            'remember_token' => \Illuminate\Support\Str::random(10),
        ];
    }
}
