<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Sautor\Core\Models\Grupo;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('grupos', function (Blueprint $table) {
            $table->enum('visibilidade', ['visivel', 'nao_listado', 'oculto'])->after('visivel')->default('visivel');
        });

        Grupo::where('visivel', false)->get()->each(function (Grupo $grupo) {
            $grupo->visibilidade = 'oculto';
            $grupo->save();
        });

        Schema::table('grupos', function (Blueprint $table) {
            $table->dropColumn('visivel');
        });

    }

    public function down(): void
    {
        Schema::table('grupos', function (Blueprint $table) {
            $table->boolean('visivel')->after('visibilidade')->default(true);
        });

        Grupo::where('visibilidade', '!=', 'visivel')->get()->each(function (Grupo $grupo) {
            $grupo->visivel = false;
            $grupo->save();
        });

        Schema::table('grupos', function (Blueprint $table) {
            $table->dropColumn('visibilidade');
        });
    }
};
