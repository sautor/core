<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsentimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consentimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('digital')->default(false);
            $table->string('ficheiro')->nullable();
            $table->ipAddress('ip')->nullable();
            $table->integer('pessoa_id')->unsigned();
            $table->foreign('pessoa_id')->references('id')->on('pessoas')->onDelete('cascade');
            $table->integer('assinado_por_id')->unsigned()->nullable();
            $table->foreign('assinado_por_id')->references('id')->on('pessoas')->onDelete('cascade');

            $table->integer('added_by_id')->unsigned()->nullable();
            $table->foreign('added_by_id')->references('id')->on('pessoas')->onDelete('cascade');
            $table->dateTime('revoked_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consentimentos');
    }
}
