<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPaginasTableAddTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paginas', function (Blueprint $table) {
            $table->longText('conteudo')->nullable()->change();
            $table->string('page_template_id')->nullable();
            $table->json('options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paginas', function (Blueprint $table) {
            $table->dropColumn('options');
        });
        Schema::table('paginas', function (Blueprint $table) {
            $table->dropColumn('page_template_id');
        });
        Schema::table('paginas', function (Blueprint $table) {
            $table->longText('conteudo')->nullable(false)->change();
        });
    }
}
