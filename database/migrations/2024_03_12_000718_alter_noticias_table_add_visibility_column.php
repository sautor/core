<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Sautor\Core\Models\Noticia;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('noticias', function (Blueprint $table) {
            $table->enum('visibilidade', ['publico', 'nao_listado', 'privado'])->after('publico')->default('publico');
        });

        Noticia::where('publico', false)->get()->each(function (Noticia $noticia) {
            $noticia->visibilidade = 'privado';
            $noticia->save();
        });

        Schema::table('noticias', function (Blueprint $table) {
            $table->dropColumn('publico');
        });
    }

    public function down(): void
    {
        Schema::table('noticias', function (Blueprint $table) {
            $table->boolean('visivel')->after('visibilidade')->default(true);
        });

        Noticia::where('publico', 'privado')->get()->each(function (Noticia $noticia) {
            $noticia->publico = false;
            $noticia->save();
        });

        Schema::table('noticias', function (Blueprint $table) {
            $table->dropColumn('visibilidade');
        });
    }
};
