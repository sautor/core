<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relacoes', function (Blueprint $table) {
            $table->integer('pessoa1_id')->unsigned();
            $table->integer('pessoa2_id')->unsigned();
            $table->primary(['pessoa1_id', 'pessoa2_id']);
            $table->enum('relacao', ['pai', 'avo', 'padrinho', 'padrasto']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relacoes');
    }
}
