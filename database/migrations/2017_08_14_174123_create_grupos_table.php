<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('nome_curto', 20)->nullable();
            $table->string('descricao', 5000)->default('');
            $table->string('url')->nullable();
            $table->string('email')->nullable();
            $table->boolean('ministerio')->default(false);
            $table->boolean('visivel')->default(true);
            $table->integer('grupo_pai')->unsigned()->nullable();
            $table->foreign('grupo_pai')->references('id')->on('grupos')->onDelete('set null');
            $table->integer('ordem')->unsigned()->nullable();
            $table->string('designacao_responsavel')->nullable();
            $table->string('designacao_grupo')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
