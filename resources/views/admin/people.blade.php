@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Pessoas
        </h1>
        @can('store', \Sautor\Core\Models\Pessoa::class)
            <div class="admin-layout__header__actions">
                <a href="{{ route('pessoas.create') }}" class="button button--responsive">
                    <span class="far fa-user-plus mr-2"></span>
                    Registar
                </a>
            </div>
        @endcan
    </header>

    <x-people.list full :pessoas="$people" :meta="function($p) { return $p->grupos->pluck('nome')->implode(', '); }" />

    @if(method_exists($people, 'links'))
        <div class="mt-4">
            {{ $people->links() }}
        </div>
    @endif
@endsection