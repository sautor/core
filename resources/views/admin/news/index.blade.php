@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Notícias
        </h1>
        <div class="admin-layout__header__actions">
            <a href="{{ route('admin.noticias.create') }}" class="button button--responsive">
                <span class="far fa-plus mr-2"></span>
                Criar
            </a>
        </div>
    </header>

    <x-news.list :news="$news" :meta="function ($n) {
        $output = $n->created_at->isoFormat('LL');
        if ($n->grupo) $output .= ' · '.$n->grupo->nome_curto;
        return $output;
     } " :actions="function ($n) {
        return [
            ['label' => 'Editar', 'icon' => 'fas fa-pencil', 'link' => route('admin.noticias.edit', $n)]
        ];
     }" />

    @if(method_exists($news, 'links'))
        <div class="mt-4">
            {{ $news->links() }}
        </div>
    @endif

@endsection
