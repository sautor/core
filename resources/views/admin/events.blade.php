@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Eventos
        </h1>
    </header>

    <events-list :value='{!! $events->toJson() !!}' controller-url="{{ route('api.eventos.store') }}">
        <div class="text-center text-grey-300">
            Loading…
        </div>
    </events-list>
@endsection