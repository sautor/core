@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Permissões
        </h1>
    </header>

    <div class="alert alert--info -mt-6 mb-6" role="alert">
        Para atribuir funções e permissões a uma pessoa, aceda à sua ficha pessoal.
    </div>

    <h3 class="title title--sm mb-4">Funções</h3>

    <div class="grid grid-cols-1 2xl:grid-cols-2 gap-3">
        @foreach($roles as $role)
            <div class="bg-white rounded-lg shadow px-8 md:px-6 py-4 -mx-8 md:mx-0">
                <h4 class="title title--xs mb-3">{{ ucfirst($role->name) }}</h4>
                <div class="grid md:grid-cols-3 gap-4">
                    <div>
                        <h5 class="font-accent text-sm text-gray-700 mb-2">Permissões</h5>
                        <ul class="admin-text-list">
                            @foreach($role->permissions as $permission)
                                <li>{{ ucfirst($permission->name) }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-span-2">
                        <h5 class="font-accent text-sm text-gray-700 mb-2">Pessoas</h5>

                        <x-people.list full :pessoas="\Sautor\Core\Models\Pessoa::role($role)->get()" />
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <h3 class="title title--sm mt-6 mb-4">Permissões</h3>

    <div class="grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-3">
        @foreach($permissions as $permission)
            <div class="bg-white rounded-lg shadow px-8 md:px-6 py-4 -mx-8 md:mx-0">
                <h4 class="title title--xs mb-3">{{ ucfirst($permission->name) }}</h4>

                <h5 class="font-accent text-sm text-gray-700 mb-2">Pessoas</h5>

                <x-people.list full :pessoas="\Sautor\Core\Models\Pessoa::permission($permission)->get()" />
            </div>
        @endforeach
    </div>
@endsection