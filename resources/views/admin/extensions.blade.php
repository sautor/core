@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Extensões
        </h1>
    </header>

    @unless($addons->isEmpty())
        <div class="sautor-list">
            @foreach($addons as $addon)
                <div class="sautor-list__item">
                    <div class="sautor-list__item__status {{ $addon->isEnabled() ? 'sautor-list__item__status--success sautor-list__item__status--glow' : 'sautor-list__item__status--danger' }}"></div>
                    <div class="sautor-list__item__logo">
                     <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                          <span class="fad fa-{{ $addon->icon }}"></span>
                     </span>
                    </div>
                    <div class="sautor-list__item__data">
                        <p class="sautor-list__item__name">
                            <span class="font-weight-bold">{{ $addon->label }}</span>
                            @if($addon->isGroupAddon())
                                <span class="badge badge--info ml-2"><span class="fas fa-users"></span> Extensão de grupo</span>
                            @elseif($addon->isTogglablePerGroup())
                                <span class="badge badge--success ml-2"><span class="fas fa-toggle-on"></span> Ativável por grupo</span>
                            @endif
                        </p>
                        @unless(empty($addon->description))
                            <p class="sautor-list__item__meta">{{ $addon->description }}</p>
                        @endunless
                    </div>
                    @if($addon->isEnabled())
                        <form action="{{ route('admin.extensoes.disable', $addon->key) }}" method="POST" class="sautor-list__item__actions">
                            @method('DELETE')
                            @csrf
                            <button type="submit">
                                <span class="far fa-times"></span>
                            </button>
                        </form>
                    @else
                        <form action="{{ route('admin.extensoes.enable', $addon->key) }}" method="POST" class="sautor-list__item__actions">
                            @csrf
                            <button type="submit">
                                <span class="far fa-check"></span>
                            </button>
                        </form>
                    @endif
                </div>
            @endforeach
        </div>
    @else
        <p class="sautor-list--empty">
            Não existem extensões instaladas
        </p>
    @endunless
@endsection