@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Personalizar
        </h1>
    </header>

    <form method="POST" enctype="multipart/form-data">
    @csrf

        <section class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Identidade visual</h3>
            </header>
            <div class="admin-settings__section__content admin-settings__section__content--empty">
                <h6 class="font-headline mb-2 text-gray-600">Logótipo</h6>
                <div class="admin-settings__section__content admin-settings__section__body">

                    <div class="field">
                        <label class="label">
                            Logótipo
                            <span class="label__optional">(opcional)</span>
                        </label>
                        <div class="control">
                            <setting-file-picker name="site-logo" path="logos" accept="image/*" title="Escolher logótipo…" value="{{ Setting::get('site-logo') }}"></setting-file-picker>
                        </div>
                        <small class="field__help">
                            É recomendada uma imagem em formato PNG ou SVG, com fundo transparente.
                        </small>
                    </div>

                    <div class="field">
                        <label class="label">
                            Logótipo invertido
                            <span class="label__optional">(opcional)</span>
                        </label>
                        <div class="control">
                            <setting-file-picker name="site-logo-invert" path="logos" accept="image/*" title="Escolher logótipo invertido…" value="{{ Setting::get('site-logo-invert') }}" invert></setting-file-picker>
                        </div>
                        <small class="field__help">
                            É recomendada uma imagem em formato PNG ou SVG, com fundo transparente, para ser usada sobre fundos escuros ou da cor abaixo. Se não existir, será usado o logótipo principal.
                        </small>
                    </div>

                    <div class="field">
                        <label class="label">
                            Logomarca
                            <span class="label__optional">(opcional)</span>
                        </label>
                        <div class="control">
                            <setting-file-picker name="site-wordmark" path="logos" accept="image/*" title="Escolher logomarca…" value="{{ Setting::get('site-wordmark') }}" invert></setting-file-picker>
                        </div>
                        <small class="field__help">
                            É recomendada uma imagem em formato PNG ou SVG, com fundo transparente, com uma proporção horizontal e para ser usada sobre fundos escuros ou da cor abaixo. Se não existir, será usado o logótipo invertido.
                        </small>
                    </div>

                    <div class="field">
                        <label class="label">Cor</label>
                        <setting-color-picker class="mt-2" name="site-color" value="{{ Setting::get('site-color') ?? '#0099ff' }}" {{ Setting::has('site-color') ? 'is-setting' : '' }} />
                    </div>

                    <div class="field">
                        <label class="label">
                            Imagem do cabeçalho
                            <span class="label__optional">(opcional)</span>
                        </label>
                        <div class="control">
                            <setting-file-picker name="site-hero" path="imagens" accept="image/*" title="Escolher imagens do cabeçalho…" value="{{ Setting::get('site-hero') }}" multiple></setting-file-picker>
                        </div>
                    </div>

                </div>

                <h6 class="font-headline mb-2 mt-3 text-gray-600">Tipografia</h6>
                <div class="admin-settings__section__content admin-settings__section__body">
                    <typography-options name="setting[site-typography]" value="{{ Setting::get('site-typography') }}" />
                </div>
            </div>
        </section>

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Menu</h3>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <menu-editor name="setting[menu]" value="{{ Setting::get('menu', '[]') }}"></menu-editor>
                </div>
            </div>
        </div>

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Destaque na página inicial</h3>
                <p class="admin-settings__section__desc">
                    Este destaque aparecerá na página inicial, no lugar da fotografia de topo.
                </p>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <x-forms.input name="setting[highlight-title]" label="Título" :value="Setting::get('highlight-title')" />
                    <x-forms.input name="setting[highlight-text]" label="Texto" :value="Setting::get('highlight-text')" />
                    <x-forms.input name="setting[highlight-link]" label="Ligação" :value="Setting::get('highlight-link')" type="url" />

                    @foreach(['highlight-logo' => ['logótipo', 'logos'], 'highlight-bg' => ['imagem de fundo', 'imagens']] as $name => [$label, $path])
                        <div class="field">
                            <label class="label">
                                {{ ucfirst($label) }}
                                <span class="label__optional">(opcional)</span>
                            </label>
                            <div class="control">
                                <setting-file-picker name="{{ $name }}" path="{{ $path }}" accept="image/*" title="Escolher {{ $label }}…" value="{{ Setting::get($name) }}"></setting-file-picker>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="admin-settings__actions">
            <button type="submit" class="button button--primary">Guardar</button>
        </div>
    </form>
@endsection