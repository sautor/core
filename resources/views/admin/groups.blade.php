@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Grupos
        </h1>
        @can('create', \Sautor\Core\Models\Grupo::class)
            <div class="admin-layout__header__actions">
                <button type="button" class="button button--responsive" @click.prevent="openModal('addGroup')">
                    <span class="far fa-plus mr-2"></span>
                    Adicionar
                </button>
            </div>
        @endcan
    </header>

    <x-groups.list full :grupos="$grupos" :badge="function($g) {
                                    $output = '';
                                    if($g->visibilidade === 'nao_listado') { $output .= ' <span class=\'badge\'>Não listado</span>'; }
                                    if($g->visibilidade === 'oculto') { $output .= ' <span class=\'badge\'>Oculto</span>'; }
    return $output !== '' ? $output : null;
    }" />

    @if($gruposEliminados->isNotEmpty())
        <h2 class="title title--sm mt-6 mb-4">Grupos eliminados</h2>
        <x-groups.list full :grupos="$gruposEliminados" :actions="function($g) {
                        return [
                            ['icon' => 'far fa-fw fa-undo', 'label' => 'Restaurar', 'modal' => 'restoreModal'.$g->id]
                        ];
                    }" />

        @foreach($gruposEliminados as $grp)
            <x-groups.restore-modal :group="$grp" />
        @endforeach
    @endif

    <x-groups.add-modal :slugs="$slugs" />
@endsection
