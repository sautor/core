@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header">
        <h1 class="admin-layout__header__title">
            Domínios
        </h1>
    </header>

    <livewire:domains />
@endsection

@push('styles')
    @livewireStyles
@endpush

@push('scripts')
    @livewireScripts
@endpush
