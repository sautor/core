@extends('layouts.basic')

@section('content')

    <content-editor
            controller-url="/api/paginas"
            edit-url="/admin/paginas/{id}/edit"
            delete-url="/admin/paginas/{id}"
            templates-url="/api/paginas/modelos"
            base-url="{{ url('/') }}"
            has-slug
            parent-key="pagina_pai"
    ></content-editor>

@endsection
