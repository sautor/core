@extends('layouts.basic')

@section('content')

    <content-editor
            :value='{!! $pagina->toJson() !!}'
            controller-url="/api/paginas"
            edit-url="/admin/paginas/{id}/edit"
            delete-url="/admin/paginas/{id}"
            templates-url="/api/paginas/modelos"
            base-url="{{ url('/') }}"
            has-slug
            parent-key="pagina_pai"
            :children='{!! $pagina->filhos()->select(['id', 'titulo', 'slug'])->get()->toJson() !!}'
    ></content-editor>

@endsection
