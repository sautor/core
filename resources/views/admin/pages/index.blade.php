@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Páginas
        </h1>
        <div class="admin-layout__header__actions">
            <a href="{{ route('admin.paginas.create') }}" class="button button--responsive">
                <span class="far fa-plus mr-2"></span>
                Criar
            </a>
        </div>
    </header>

    <x-pages.list :pages="$pages" />

    @if(method_exists($pages, 'links'))
        <div class="mt-4">
            {{ $pages->links() }}
        </div>
    @endif

@endsection