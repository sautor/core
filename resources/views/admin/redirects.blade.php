@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header">
        <h1 class="admin-layout__header__title">
            Redirecionamentos
        </h1>
    </header>

    <livewire:redirects />
@endsection

@push('styles')
    @livewireStyles
@endpush

@push('scripts')
    @livewireScripts
@endpush
