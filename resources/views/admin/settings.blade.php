@extends('layouts.admin')

@section('admin-content')
    <header class="admin-layout__header md:gap-3">
        <h1 class="admin-layout__header__title">
            Definições
        </h1>
    </header>

    <form method="POST" enctype="multipart/form-data">
    @csrf

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Modo de manutenção</h3>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <x-forms.checkbox name="setting[man]" label="Ativo" :value="Setting::get('man')" />
                    <x-forms.input name="setting[man-title]" label="Título" :value="Setting::get('man-title')" />
                    <x-forms.input name="setting[man-subtitle]" label="Subtítulo" :value="Setting::get('man-subtitle')" />
                    <div class="field">
                        <label class="label">Conteúdo</label>
                        <div class="control">
                            <small-content-editor name="setting[man-content]" value="{{ Setting::get('man-content') }}"></small-content-editor>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Proteção de dados e Privacidade</h3>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <x-forms.checkbox name="setting[gdpr-blockData]" label="Bloquear o acesso a pessoas sem consentimento ativo" :value="Setting::get('gdpr-blockData')" />
                    <x-forms.checkbox name="setting[gdpr-requireForLogin]" label="Obrigar os utilizadores a consentir para poder entrar" :value="Setting::get('gdpr-requireForLogin')" />
                    <div class="field">
                        <label class="label">Informação</label>
                        <div class="control">
                            <small-content-editor name="setting[gdpr-info]" value="{{ Setting::get('gdpr-info') }}"></small-content-editor>
                        </div>
                    </div>
                    <x-forms.textarea name="setting[gdpr-authorization]" label="Autorização" :value="Setting::get('gdpr-authorization')" />
                    <x-forms.textarea name="setting[gdpr-authorization-adult]" label="Autorização para documento - adultos" :value="Setting::get('gdpr-authorization-adult')" />
                    <x-forms.textarea name="setting[gdpr-authorization-minor]" label="Autorização para documento - menores" :value="Setting::get('gdpr-authorization-minor')" />

                    <x-forms.input name="setting[privacy-policy-link]" label="Ligação para a Política de Privacidade" :value="Setting::get('privacy-policy-link')" type="url" />
                </div>
            </div>
        </div>

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Banner de Cookies</h3>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <x-forms.checkbox name="setting[cookie-banner]" label="Mostrar banner de cookies" :value="Setting::get('cookie-banner')" />
                    <x-forms.input name="setting[cookie-duration]" label="Duração do cookie" :value="Setting::get('cookie-duration')" after="dias" type="number" :options="['placeholder' => 30, 'min' => 1, 'step' => 1]" />
                    <x-forms.input name="setting[cookie-title]" label="Título" :value="Setting::get('cookie-title')" />
                    <div class="field">
                        <label class="label">Conteúdo</label>
                        <div class="control">
                            <small-content-editor name="setting[cookie-content]" value="{{ Setting::get('cookie-content') }}"></small-content-editor>
                        </div>
                    </div>
                    <x-forms.input name="setting[cookie-more-link]" label="Ligação da Política de Cookies" :value="Setting::get('cookie-more-link')" type="url" />
                </div>
            </div>
        </div>

        <div class="admin-settings__section">
            <header class="admin-settings__section__header">
                <h3 class="admin-settings__section__title">Contactos</h3>
            </header>
            <div class="admin-settings__section__content">
                <div class="admin-settings__section__body">
                    <x-forms.textarea name="setting[contact-address]" label="Morada" :value="Setting::get('contact-address')" />
                    <x-forms.input name="setting[contact-email]" label="Endereço de email" :value="Setting::get('contact-email')" type="email" before="<span class='far fa-at fa-fw'></span>" />
                    <x-forms.input name="setting[social-facebook]" label="Facebook" :value="Setting::get('social-facebook')" type="url" before="<span class='fab fa-facebook fa-fw'></span>" />
                    <x-forms.input name="setting[social-instagram]" label="Instagram" :value="Setting::get('social-instagram')" type="url" before="<span class='fab fa-instagram fa-fw'></span>" />
                    <x-forms.input name="setting[social-twitter]" label="Twitter" :value="Setting::get('social-twitter')" type="url" before="<span class='fab fa-twitter fa-fw'></span>" />
                    <x-forms.input name="setting[social-youtube]" label="YouTube" :value="Setting::get('social-youtube')" type="url" before="<span class='fab fa-youtube fa-fw'></span>" />
                    <x-forms.input name="setting[social-spotify]" label="Spotify" :value="Setting::get('social-spotify')" type="url" before="<span class='fab fa-spotify fa-fw'></span>" />
                </div>
            </div>
        </div>

        <div class="admin-settings__actions">
            <button type="submit" class="button button--primary">Guardar</button>
        </div>
    </form>
@endsection