@extends('layouts.admin')

@section('admin-content')
    <div class="grid grid-cols-3 gap-4">
        @foreach($cards as $card)
            <div class="rounded bg-white shadow">
                <div class="flex gap-x-4 p-4 items-center">
                    <div class="text-gray-400 text-xl text-center px-2">
                        <span class="fa-fw far {{ $card['icon'] }}"></span>
                    </div>
                    <div>
                        <p class="text-sm font-medium text-gray-500">{{ $card['label'] }}</p>
                        <p class="text-xl font-bold">{{ $card['value'] }}</p>
                    </div>
                </div>
                <div class="bg-gray-100 px-4 py-2">
                    <a class="text-sm font-medium text-primary-500 hover:text-primary-600" href="{{ $card['url'] }}">{{ $card['link_label'] ?? 'Ver mais…' }}</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection