@extends('layouts.app')

@section('content')
    <div class="group-admin-layout__container">
        <aside class="side-nav">
            <h6 class="side-nav__topic">{{ $grupo->nome_curto }}</h6>
            <h6 class="side-nav__title">Gestão do grupo</h6>

            @foreach($privateMenu as $menuItem)

                <a href="{{ @$menuItem['absolute_route'] ? route($menuItem['route'], $grupo) : $grupo->route($menuItem['route'], ...($menuItem['params'] ?? [])) }}"
                   class="side-nav__item @if(Route::currentRouteName() === $menuItem['route']) side-nav__item--active @endif ">
                    <span class="side-nav__item__icon fa-fw {{ $menuItem['icon'] }}"></span>
                    <span class="side-nav__item__label">
                        {{ $menuItem['label'] }}
                    </span>
                </a>

            @endforeach

            <a href="{{ $grupo->route('index') }}"
               class="side-nav__item">
                <span class="side-nav__item__icon fa-fw far fa-arrow-left"></span>
                <span class="side-nav__item__label">
                    Voltar
                </span>
            </a>
        </aside>

        <div class="group-admin-layout__content">
            @yield('group-content')
        </div>
    </div>
@endsection
