@livewire('notifications')
<!-- Scripts -->
<script>
  window.sautor = {
    environment: '{{ config('app.env') }}',
    dsn: {!! config('sentry.dsn') ? "'".config('sentry.dsn')."'" : 'null' !!},
    @if(Auth::guest())
    user: null,
    @else
    user: {
      id: '{{ Auth::id() }}',
      email: '{{ Auth::user()->email }}',
      username: '{{ Auth::user()->nome_exibicao }}'
    },
    @endif
  }
</script>

<script defer src="{{ asset('fa/brands.min.js') }}"></script>
<script defer src="{{ asset('fa/duotone.min.js') }}"></script>
<script defer src="{{ asset('fa/regular.min.js') }}"></script>
<script defer src="{{ asset('fa/solid.min.js') }}"></script>
<script defer src="{{ asset('fa/fontawesome.min.js') }}"></script>

@filamentScripts
@vite(['resources/js/app.js'])
@stack('scripts')

@if(config('app.env') === 'production' && config('services.google_analytics.tracking_id'))
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.google_analytics.tracking_id') }}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ config('services.google_analytics.tracking_id') }}');
  </script>
@endif

</body>
</html>
