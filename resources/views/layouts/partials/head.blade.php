<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>
    @if(isset($grupo) and $grupo->site_group)
      {{ $grupo->site_group->nome }}
      &middot;
    @endif
    {{ config('app.name') }}
  </title>

<!-- Styles -->
  @filamentStyles
  @vite(['resources/sass/main.scss'])

    @include('layouts.partials.custom-styles')

    @stack('styles')
</head>
<body class="{{ implode(" ", $bodyClass) }}">
