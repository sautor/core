@php
    $site_group = isset($grupo) ? $grupo->site_group : null;
    $isSiteGroup = isset($grupo) && $grupo->site_group && $grupo->is($grupo->site_group);
@endphp

@empty($basicFooter)
    @include('groups.partials.footer')
@endempty

<div class="main-footer">
  @if(empty($basicFooter) && !($site_group && \Setting::get('grupo-'.$site_group->id.'-short-footer')))
    <section class="main-footer__content">
      <a href="{{ url('/') }}" class="main-footer__logo">
          @if(Setting::get('site-logo-invert'))
              <img src="{{ Setting::get('site-logo-invert') }}" alt="{{ config('app.name') }}">
          @elseif(Setting::get('site-logo'))
              <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
          @else
              {{ config('app.name') }}
          @endif
      </a>

      @php
        $facebook = Setting::get('social-facebook');
        $instagram = Setting::get('social-instagram');
        $twitter = Setting::get('social-twitter');
        $youtube = Setting::get('social-youtube');
        $spotify = Setting::get('social-spotify');
        $has_social = $facebook || $instagram || $twitter || $youtube || $spotify;
        $social = array_filter(compact('facebook', 'instagram', 'twitter', 'youtube', 'spotify'));
      @endphp
      @if($has_social)
        <div class="main-footer__social">
          @foreach($social as $name => $link)
            <a href="{{ $link  }}">
              <span class="fab fa-{{ $name }}"></span>
            </a>
          @endforeach
        </div>
      @endif
      <div class="main-footer__contacts">
        @php
          $address = Setting::get('contact-address');
          $email = Setting::get('contact-email');
        @endphp
        @if($address)
          <address>{{ $address }}</address>
        @endif
        @if($email)
          <a href="mailto:{{ $email }}">{{ $email }}</a>
        @endif
      </div>
    </section>
  @endif

  <section class="main-footer__bottom">
    @php($privacy_link = Setting::get('privacy-policy-link'))
    @php($cookies_link = Setting::get('cookie-more-link'))
    @if($privacy_link or $cookies_link)
      <div class="main-footer__links">
        @if($privacy_link)
          <a href="{{ $privacy_link }}">Política de privacidade</a>
        @endif
        @if($cookies_link)
          <a href="{{ $cookies_link }}">Política de cookies</a>
        @endif
      </div>
    @endif
    <div class="main-footer__copyright">
      &copy; <strong>{{ config('app.name') }}</strong> {{ date('Y') }}
    </div>
  </section>
</div>
