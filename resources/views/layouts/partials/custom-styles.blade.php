@php
    if(isset($grupo) and $grupo->site_group and $grupo->site_group->cor) {
        $palette = Sautor\generate_palette($grupo->site_group->cor);
    } elseif (Setting::has('site-color')) {
        $palette = Sautor\generate_palette(Setting::get('site-color'));
    }
@endphp

@unless(empty($palette))
    <style>
        :root {
            --primary-50:  {{ $palette[0] }};
            --primary-100: {{ $palette[1] }};
            --primary-200: {{ $palette[2] }};
            --primary-300: {{ $palette[3] }};
            --primary-400: {{ $palette[4] }};
            --primary-500: {{ $palette[5] }};
            --primary-600: {{ $palette[6] }};
            --primary-700: {{ $palette[7] }};
            --primary-800: {{ $palette[8] }};
            --primary-900: {{ $palette[9] }};
        }
    </style>
@endunless

@if(Setting::has('site-color') or (isset($grupo) and $grupo->cor))
    <style>
        :root {
            @if(Setting::has('site-color')) --site-color: {{ Setting::get('site-color') }}; @endif
            @if(isset($grupo) and $grupo->cor) --group-color: {{ $grupo->cor }}; @endif
        }
</style>
@endif

@if(Setting::get('grupo-'.@$grupo->site_group->id.'-typography') or Setting::get('site-typography'))
@php
$typography = json_decode(Setting::get('grupo-'.@$grupo->site_group->id.'-typography') ?? Setting::get('site-typography'), true);
@endphp
<style>
:root {
    @unless(empty($typography['sansFamily'])) --sans-family: {{ $typography['sansFamily'] }}, sans-serif; @endunless
        @unless(empty($typography['headlineFamily'])) --headline-family: {{ $typography['headlineFamily'] }}, sans-serif; @endunless
        @unless(empty($typography['headlineWeight'])) --headline-weight: {{ $typography['headlineWeight'] }}; @endunless
        @unless(empty($typography['accentWeight'])) --accent-weight: {{ $typography['accentWeight'] }}; @endunless
}
</style>
@unless(empty($typography['stylesheetUrl']))
<link rel="stylesheet" href="{{ $typography['stylesheetUrl'] }}">
@endunless
@endif

@if(isset($grupo) and $grupo->site_group)
@if($grupo->site_group->icon ?? $grupo->site_group->logo)
@php($has_favicon = true)
<link rel="icon" type="image/png" href="{{ $grupo->site_group->icon ?? $grupo->site_group->logo }}">
<link rel="apple-touch-icon" href="{{ $grupo->site_group->icon ?? $grupo->site_group->logo }}">
@endif
@endif

@empty($has_favicon)
@if(file_exists(public_path('/img/favicon.png')))
<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">
<link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}">
@endif
@endempty
