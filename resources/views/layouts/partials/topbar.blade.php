<div class="topbar">
  <a href="{{ route('home') }}" class="topbar__brand">
      @if(Setting::get('site-wordmark'))
          <img src="{{ Setting::get('site-wordmark') }}" alt="{{ config('app.name') }}">
      @elseif(Setting::get('site-logo-invert'))
          <img src="{{ Setting::get('site-logo-invert') }}" alt="{{ config('app.name') }}">
      @elseif(Setting::get('site-logo'))
          <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
      @else
          {{ config('app.name') }}
      @endif
  </a>
    @unless(empty($grupo))
        <a href="{{ $grupo->route('index') }}" class="topbar__group">
            {{ $grupo->nome_curto }}
        </a>
    @endunless
  <div class="topbar__user">
    @auth
      <x-user-menu>
          <button class="topbar__user__profile" @click="toggle">
              <img src="{{ Auth::user()->foto }}" class="topbar__user__avatar" alt="{{ Auth::user()->nome_curto }}">
          </button>
      </x-user-menu>
    @endauth
    @guest
      <a href="{{ route('login') }}" class="topbar__user__login">
        <span class="far fa-lg fa-sign-in"></span>
      </a>
    @endguest

  </div>
</div>
