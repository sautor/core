@php($site_group = isset($grupo) ? $grupo->site_group : null)

@unless(empty($site_group))
  @include('layouts.partials.topbar')
@endunless

<div class="header @unless(empty($site_group)) header--group @endunless @unless(empty($headerSize)) header--{{ $headerSize }} @endunless ">
  <a class="header__logo" href="{{ isset($site_group) ? $site_group->route('index') : route('home') }}">
    @empty($site_group)
      @if(Setting::get('site-logo-invert'))
        <img src="{{ Setting::get('site-logo-invert') }}" alt="{{ config('app.name') }}">
      @elseif(Setting::get('site-logo'))
        <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
      @else
        {{ config('app.name') }}
      @endif
    @else
      @if($site_group->logo_horizontal)
        <img src="{{ $site_group->logo_horizontal }}" alt="{{ $site_group->nome }}">
      @elseif($site_group->logo)
        <img src="{{ $site_group->logo }}" alt="{{ $site_group->nome }}">
      @else
        {{ $site_group->nome }}
      @endif
    @endempty
  </a>


  <div class="header__nav">
    <button class="header__nav__button header__nav__button--toggle" @click="toggleMenu">
      <span class="far fa-bars"></span>
      Menu
    </button>

    @empty($site_group)
      <a class="header__nav__button" @click="toggleSearch">
        <span class="far fa-search"></span>
      </a>
      @if (Auth::guest())
        <a class="header__nav__button" href="{{ route('login') }}">
          <span class="far fa-sign-in"></span>
        </a>
      @else
        <x-user-menu>
          <button class="header__nav__button" @click="toggle">
            <img src="{{ Auth::user()->foto }}" alt="{{ Auth::user()->nomeCurto }}" class="header__nav__avatar">
          </button>
        </x-user-menu>
      @endif
    @endempty
  </div>

  <x-menu :group="$site_group ?? null" />

  @empty($site_group)
    <search-overlay :is-open="isSearchOpen" @close="isSearchOpen = false" />
  @endempty
</div>
