@if(Setting::get('cookie-banner'))
  <cookie-banner
    cookie-name="{{ config('app.cookie_layer_name') }}"
    cookie-duration="{{ Setting::get('cookie-duration', 30) }}"
    title="{{ Setting::get('cookie-title') }}"
    more-link="{{ Setting::get('cookie-more-link') }}"
  >
    {!! Setting::get('cookie-content') !!}
  </cookie-banner>
@endif
