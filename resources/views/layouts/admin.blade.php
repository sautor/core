@extends('layouts.app')

@section('content')
    <div class="admin-layout__container">
        <aside class="side-nav">
            <h6 class="side-nav__title">Administração</h6>
            @canany(['ver pessoas', 'gerir pessoas'])
                <a href="{{ route('admin.people') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.people') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-user-friends"></span>
                    <span class="side-nav__item__label">
                        Pessoas
                    </span>
                </a>
            @endcanany
            @can('gerir grupos')
                <a href="{{ route('admin.groups') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.groups') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-users"></span>
                    <span class="side-nav__item__label">
                        Grupos
                    </span>
                </a>
            @endcan
            @can('gerir notícias')
                <a href="{{ route('admin.noticias.index') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.noticias.index') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-newspaper"></span>
                    <span class="side-nav__item__label">
                        Notícias
                    </span>
                </a>
            @endcan
            @can('gerir páginas')
                <a href="{{ route('admin.paginas.index') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.paginas.index') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-file-alt"></span>
                    <span class="side-nav__item__label">
                        Páginas
                    </span>
                </a>
            @endcan
            @can('gerir eventos')
                <a href="{{ route('admin.eventos') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.eventos') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-calendar-alt"></span>
                    <span class="side-nav__item__label">
                        Eventos
                    </span>
                </a>
            @endcan
            @can('gerir permissões')
                <a href="{{ route('admin.permissoes') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.permissoes') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-shield-alt"></span>
                    <span class="side-nav__item__label">
                        Permissões
                    </span>
                </a>
            @endcan
            @can('gerir opções')
                @if(\AddonService::all()->isNotEmpty())
                    <a href="{{ route('admin.extensoes') }}"
                       class="side-nav__item @if(Route::currentRouteName() === 'admin.extensoes') side-nav__item--active @endif ">
                        <span class="side-nav__item__icon far fa-fw fa-puzzle-piece"></span>
                        <span class="side-nav__item__label">
                            Extensões
                        </span>
                    </a>
                @endif
                <a href="{{ route('admin.personalizar') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.personalizar') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-pencil-paintbrush"></span>
                    <span class="side-nav__item__label">
                        Personalizar
                    </span>
                </a>
                    <a href="{{ route('admin.redirects') }}"
                       class="side-nav__item @if(Route::currentRouteName() === 'admin.redirects') side-nav__item--active @endif ">
                        <span class="side-nav__item__icon far fa-fw fa-directions"></span>
                        <span class="side-nav__item__label">
                        Redirecionamentos
                    </span>
                    </a>
                <a href="{{ route('admin.definicoes') }}"
                   class="side-nav__item @if(Route::currentRouteName() === 'admin.definicoes') side-nav__item--active @endif ">
                    <span class="side-nav__item__icon far fa-fw fa-cogs"></span>
                    <span class="side-nav__item__label">
                        Definições
                    </span>
                </a>
            @endcan
            @can('gerir domínios')
                <a href="{{ route('admin.domains') }}"
                   class="side-nav__item">
                    <span class="side-nav__item__icon far fa-fw fa-server"></span>
                    <span class="side-nav__item__label">
                        Domínios
                    </span>
                </a>
            @endcan
            @can('ver logs')
                <a href="{{ route('log-viewer.index') }}"
                   class="side-nav__item">
                    <span class="side-nav__item__icon far fa-fw fa-bug"></span>
                    <span class="side-nav__item__label">
                        Ver logs
                    </span>
                </a>
            @endcan
        </aside>

        <div class="admin-layout__content">
            @yield('admin-content')
        </div>
    </div>
@endsection
