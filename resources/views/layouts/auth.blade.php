@include('layouts.partials.head')

<div id="app" class="auth-screen @unless(empty($grupo)) auth-screen--group @endunless ">

  <div id="app-content">

    <div class="auth-screen__logo">
        @unless(empty($grupo))
            @if($grupo->logo)
                <img src="{{ $grupo->logo }}" alt="{{ $grupo->nome }}" class="login-logo">
            @else
                {{ $grupo->nome }}
            @endif
        @else
            @if(Setting::get('site-logo-invert'))
                <img src="{{ Setting::get('site-logo-invert') }}" alt="{{ config('app.name') }}" class="login-logo">
            @elseif(Setting::get('site-logo'))
                <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}" class="login-logo">
            @else
                {{ config('app.name') }}
            @endif
        @endunless
    </div>

    <div class="auth-content">
        @unless(empty($grupo))
            <h4 class="auth-content__logo">
                @if(Setting::get('site-logo'))
                    <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
                @else
                    {{ config('app.name') }}
                @endif
            </h4>
        @endunless

        @yield('content')
    </div>
  </div>

</div>

@include('layouts.partials.foot')
