@include('layouts.partials.head')

<div id="app">

  @includeWhen(empty($noTopbar), 'layouts.partials.topbar')

  <div id="app_content">
    @yield('content')
  </div>

  @include('layouts.partials.footer', ['basicFooter' => empty($withFooter)])

  @include('layouts.partials.cookie-banner')
</div>

@include('layouts.partials.foot')
