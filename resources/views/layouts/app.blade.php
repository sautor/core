@include('layouts.partials.head')

<div id="app">

  @include('layouts.partials.header')

  <div id="app_content">
    @yield('content')
  </div>

  @include('layouts.partials.footer')
  @include('layouts.partials.cookie-banner')

</div>

@include('layouts.partials.foot')
