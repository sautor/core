@props(['id', 'name', 'scope', 'rules', 'label', 'help', 'required', 'value', 'cbVal', 'noField', 'options'])

@empty($noField)
<div class="field @if($errors->has($name)) field--invalid @endif">
@endempty
    <div class="checkbox">
        <div class="checkbox__input">
            <input type="checkbox" id="{{ $id ?? $name }}" name="{{ $name }}" value="{{ isset($cbVal) ? $cbVal : 1 }}"
            @foreach($options ?? [] as $key => $val) {{$key}}="{{$val}}" @endforeach
            @unless(empty($rules))
                v-validate="'{{ $rules }}'" data-vv-as="{{ $label ?? ucfirst($name) }}"
            @endunless
            @foreach($options ?? [] as $key => $val) {{$key}}="{{$val}}" @endforeach
            @if(old($name, $value ?? (isset($formModel) ? $formModel[$name] : null) ?? [])) checked @endif >
        </div>
        <div class="checkbox__label">
            <label for="{{ $id ?? $name }}">{{ $label ?? ucfirst($name) }}</label>
            @unless(empty($help))
                <small class="field__help"> {{ $help }} </small>
            @endunless
            @if ($errors->has($name))
                <span class="field__help field__help--invalid">
                    {{ $errors->first($name) }}
                </span>
            @endif
            @unless(empty($rules))
                <span class="field__help field__help--invalid" v-if="errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')" v-html="errors.first('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')"></span>
            @endunless
        </div>
    </div>
@empty($noField)
</div>
@endempty
