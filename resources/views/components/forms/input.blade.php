@props(['id', 'name', 'type', 'scope', 'required', 'before', 'after', 'class', 'rules', 'label', 'list', 'options', 'help', 'value'])

<div class="field @if($errors->has($name)) field--invalid @endif" :class="{ 'field--invalid': errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}') }">
    <label for="{{ $id ?? $name }}" class="label">
        {{ $label ?? ucfirst($name) }}
        @empty($required) <span class="label__optional">(opcional)</span> @endempty
    </label>

    <div class="control">

        @unless(empty($before))
            <div class="control__addon control__addon--before">
                <span class="control__addon__text">{!! $before !!}</span>
            </div>
        @endunless

        <input class="input {{ $class ?? '' }}" name="{{$name}}" id="{{ $id ?? $name }}" type="{{ $type ?? 'text' }}"
        @unless(empty($rules))
            v-validate="'{{ $rules }}'" data-vv-as="{{ $label ?? ucfirst($name) }}"
        @endunless
        @unless(empty($list)) list="{{ $name }}-list" @endunless
        @foreach($options ?? [] as $key => $val) {{$key}}="{{$val}}" @endforeach value="{{ old($name, $value ?? null) }}"
        />

        @unless(empty($after))
            <div class="control__addon control__addon--after">
                <span class="control__addon__text">{!! $after !!}</span>
            </div>
        @endunless

    </div>

    @unless(empty($help))
        <small class="field__help"> {{ $help }} </small>
    @endunless
    @if ($errors->has($name))
        <span class="field__help field__help--invalid">
            {{ $errors->first($name) }}
        </span>
    @endif
    @unless(empty($rules))
        <span class="field__help field__help--invalid" v-if="errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')" v-html="errors.first('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')"></span>
    @endunless
    @unless(empty($list))
        <datalist id="{{ $name }}-list">
            @foreach($list as $item)
                <option>{{ $item }}</option>
            @endforeach
        </datalist>
    @endunless
</div>
