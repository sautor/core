@props(['id', 'name', 'type', 'scope', 'required', 'before', 'after', 'class', 'rules', 'label', 'list', 'options', 'help', 'value'])

<div class="field @if($errors->has($name)) field--invalid @endif" :class="{ 'field--invalid': errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}') }">
    <label for="{{ $id ?? $name }}" class="label">
        {{ $label ?? ucfirst($name) }}
        @empty($required) <span class="label__optional">(opcional)</span> @endempty
    </label>

    <div class="control">

        @unless(empty($before))
            <div class="control__addon control__addon--before">
                <span class="control__addon__text">{!! $before !!}</span>
            </div>
        @endunless

            <select class="input {{ $class ?? '' }}" name="{{$name}}" id="{{ $id ?? $name }}"
                    @unless(empty($rules))
                    v-validate="'{{ $rules }}'" data-vv-as="{{ $label ?? ucfirst($name) }}"
            @endunless
            @foreach($options ?? [] as $key => $val) {{$key}}="{{$val}}" @endforeach >
            @empty($required)
                <option value="" @if(old($name, $value ?? (isset($formModel) ? $formModel[$name] : null) ?? null) == null) selected @endif >Nenhum</option>
            @endempty
            @foreach($list as $val => $lbl)
                <option value="{{ $val }}" @if(old($name, $value ?? (isset($formModel) ? $formModel[$name] : null) ?? null) == $val) selected @endif >{{ $lbl ?? $val }}</option>
                @endforeach
            </select>

                @unless(empty($after))
                    <div class="control__addon control__addon--after">
                        <span class="control__addon__text">{!! $after !!}</span>
                    </div>
                @endunless

    </div>

    @unless(empty($help))
        <small class="field__help"> {{ $help }} </small>
    @endunless
    @if ($errors->has($name))
        <span class="field__help field__help--invalid">
            {{ $errors->first($name) }}
        </span>
    @endif
    @unless(empty($rules))
        <span class="field__help field__help--invalid" v-if="errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')" v-html="errors.first('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')"></span>
    @endunless
</div>
