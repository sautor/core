@props(['id', 'name', 'accept', 'scope', 'required', 'class', 'rules', 'label', 'options', 'help', 'multiple'])

<div class="field @if($errors->has([$name.'.*', $name])) field--invalid @endif ">
    <label for="{{ $name }}" class="label">
        {{ $label ?? ucfirst($name) }}
        @empty($required) <span class="label__optional">(opcional)</span> @endempty
    </label>

    <div class="control">
        <input class="{{ $class ?? '' }} file-input" name="{{$name}}" id="{{ $id ?? $name }}"
               type="file" style="display:block;" @unless(empty($accept)) accept="{{ $accept }}" @endunless
               @unless(empty($rules))
               v-validate="'{{ $rules }}'" data-vv-as="{{ $label ?? ucfirst($name) }}"
        @endunless
        @if(@$multiple) multiple @endif
        @foreach($options ?? [] as $key => $val) {{$key}}="{{$val}}" @endforeach />
    </div>

    @unless(empty($help))
        <small class="field__help"> {!! $help !!} </small>
    @endunless
    @if ($errors->has($name.'.*'))
        <span class="field__help field__help--invalid">
            {{ $errors->first($name.'.*') }}
        </span>
    @endif
    @if ($errors->has($name))
        <span class="field__help field__help--invalid">
            {{ $errors->first($name) }}
        </span>
    @endif
    @unless(empty($rules))
        <span class="field__help field__help--invalid" v-if="errors.has('{{ (empty($scope) ? '' : ($scope.'.')).(@$multiple ? $name.'.*' : $name) }}')" v-html="errors.first('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')"></span>
    @endunless
</div>
