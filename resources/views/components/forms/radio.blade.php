@props(['id', 'name', 'scope', 'rules', 'label', 'list', 'required', 'value'])

<div class="field @if($errors->has($name)) field--invalid @endif" :class="{ 'field--invalid': errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}') }">
  <label class="label">
    {{ $label ?? ucfirst($name) }}
    @empty($required) <span class="label__optional">(opcional)</span> @endempty
  </label>
  @foreach($list as $val => $lbl)
    <div class="control">
      <div class="radio">
        <div class="radio__input">
          <input type="radio" id="{{ $name }}-{{ $val }}" name="{{ $name }}" value="{{ $val }}"
                 @if($loop->first and !empty($rules)) v-validate="'{{ $rules }}'" data-vv-as="{{ $label ?? ucfirst($name) }}" @endif
                 @if(old($name, $value ?? (isset($formModel) ? $formModel[$name] : null) ?? null) == $val) checked @endif >
        </div>
        <div class="radio__label">
          <label for="{{ $name }}-{{ $val }}">{{ $lbl }}</label>
        </div>
      </div>
    </div>
  @endforeach
  @unless(empty($help))
    <small class="field__help"> {{ $help }} </small>
  @endunless
  @if ($errors->has($name))
    <span class="field__help field__help--invalid">
            {{ $errors->first($name) }}
        </span>
  @endif
  @unless(empty($rules))
    <span class="field__help field__help--invalid" v-if="errors.has('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')" v-html="errors.first('{{ (empty($scope) ? '' : ($scope.'.')).$name }}')"></span>
  @endunless
</div>
