@props(['news', 'grupo'])

<section class="news-grid @if($news->isEmpty()) news-grid--empty @endif">

  @forelse($news as $news_item)
    <article class="news-grid__item">
      <div
          class="news-grid__item__image @unless($news_item->imagem) news-grid__item__image--no-thumb @endunless"
          @if($news_item->imagem) style="background-image: url({{$news_item->imagem}});" @endif
      >
        @unless($news_item->imagem)
          @if(isset($grupo) and ($grupo->icon ?? $grupo->logo))
            <img src="{{ $grupo->icon ?? $grupo->logo }}">
          @else
            <x-s-icon icon="newspaper" />
          @endif
        @endunless
        @if($news_item->grupo and (!isset($grupo) or !$news_item->grupo->is($grupo)) and ($news_item->grupo->icon ?? $news_item->grupo->logo))
          <div class="news-grid__item__image__ribbon">
            <img src="{{$news_item->grupo->icon ?? $news_item->grupo->logo}}" alt="{{$news_item->grupo->nome}}">
          </div>
        @endif
      </div>
      <div class="news-grid__item__body">
        <h1 class="news-grid__item__title">
          <a href="{{ $news_item->showRoute() }}">{{ $news_item->titulo }}</a>
          @if($news_item->grupo and (!isset($grupo) or !$news_item->grupo->is($grupo)))
            <small>{{ $news_item->grupo->nome }}</small>
          @endif
        </h1>
        <p class="news-grid__item__excerpt">{!! $news_item->excerto !!}</p>
        <p class="news-grid__item__date">
          Publicado a {{ $news_item->created_at->isoFormat('LL') }}
        </p>
      </div>
    </article>
  @empty
        Não existem notícias.
  @endforelse

</section>
