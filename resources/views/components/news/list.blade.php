@props(['news', 'meta', 'actions', 'full', 'emptyMessage'])

@unless($news->isEmpty())
    <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless news-list">
        @foreach($news as $article)
            <article class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    @if($article->imagem)
                        <img src="{{ $article->imagem }}" class="cover">
                    @else
                        <span class="sautor-list__item__logo__icon">
                                <span class="fas fa-newspaper"></span>
                            </span>
                    @endif
                </div>
                <a href="{{ $article->showRoute() }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">{{ $article->titulo }}</p>
                    @unless(empty($meta) or empty($m = $meta($article)))
                        <p class="sautor-list__item__meta">
                            {{ $m }}
                        </p>
                    @endunless
                </a>
                @unless((empty($actions) or empty($a = $actions($article))) and (Auth::guest() or !Auth::user()->can('view', $article)))
                    <div class="sautor-list__item__actions">
                        @unless(empty($actions) or empty($a))
                            @foreach($a as $action)
                                <a title="{{ $action['label'] }}"
                                   @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                                   @elseif(!empty($action['modal']))
                                   href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                        @endif
                                >
                                    <span class="{{ $action['icon'] }}"></span>
                                </a>
                            @endforeach
                        @endunless
                        @can('view', $article)
                            <a title="Ver" href="{{ $article->showRoute() }}">
                                <span class="far fa-angle-right"></span>
                            </a>
                        @endunless
                    </div>
                @endunless
            </article>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty">
        {{ empty($emptyMessage) ? 'Não existem notícias.' : $emptyMessage }}
    </section>
@endunless
