@props(['person', 'meta', 'badge', 'actions'])

<div class="people-show-component">
    <div class="sautor-list__item">
        <div class="sautor-list__item__photo">
            <img src="{{ $person->foto }}" alt="{{ $person->nome }}">
        </div>
        <a @can('view', $person) href="{{ route('pessoas.show', $person) }}"
           @endcan class="sautor-list__item__data">
            <p class="sautor-list__item__name">
                {{ $person->nome_exibicao }}
                @unless(empty($badge))
                    {!! $badge !!}
                @endunless
            </p>
            @unless(empty($meta) and (Auth::guest() or !Auth::user()->can('view', $person) or $person->hasActiveConsent()))
                <p class="sautor-list__item__meta">
                    {!! @$meta !!}
                    @can('view', $person)
                        @unless($person->hasActiveConsent())
                            <span class="badge badge-danger">
                  <span class="fas fa-file-contract"></span>
                  Sem consentimento
                </span>
                        @endunless
                    @endcan
                </p>
            @endunless
        </a>
        @unless(empty($actions) and (Auth::guest() or !Auth::user()->can('view', $person)))
            <div class="sautor-list__item__actions">
                @unless(empty($actions))
                    @foreach($actions as $action)
                        <a title="{{ $action['label'] }}"
                           @if(!empty($action['link']))
                               href="{{ $action['link'] }}"
                           @elseif(!empty($action['modal']))
                               href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                @endif
                        >
                            <span class="{{ $action['icon'] }}"></span>
                        </a>
                    @endforeach
                @endunless
                @can('view', $person)
                    <a title="Ver" href="{{ route('pessoas.show', $person) }}">
                        <span class="far fa-angle-right"></span>
                    </a>
                @endcan
            </div>
        @endunless
    </div>
</div>
