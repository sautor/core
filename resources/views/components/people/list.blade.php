@props(['pessoas', 'badge', 'meta', 'actions', 'empty-message', 'full'])

@unless($pessoas->isEmpty())
  <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless people-list">
    @foreach($pessoas as $pessoa)
      <div class="sautor-list__item">
        <div class="sautor-list__item__photo">
          <img src="{{ $pessoa->foto }}" alt="{{ $pessoa->nome }}">
        </div>
        <a @can('view', $pessoa) href="{{ route('pessoas.show', $pessoa) }}"
           @endcan class="sautor-list__item__data">
          <p class="sautor-list__item__name">
            {{ $pessoa->nome_exibicao }}
            @unless(empty($badge) or empty($b = $badge($pessoa)))
              {!! $b !!}
            @endunless
          </p>
          @unless((empty($meta) or empty($m = $meta($pessoa))) and (Auth::guest() or !Auth::user()->can('view', $pessoa) or $pessoa->hasActiveConsent()))
            <p class="sautor-list__item__meta">
              {!! @$m !!}
              @can('view', $pessoa)
              @unless($pessoa->hasActiveConsent())
                <span class="badge badge--danger">
                  <span class="fas fa-file-contract"></span>
                  Sem consentimento
                </span>
              @endunless
              @endcan
            </p>
          @endunless
        </a>
        @unless((empty($actions) or empty($a = $actions($pessoa))) and (Auth::guest() or !Auth::user()->can('view', $pessoa)))
          <div class="sautor-list__item__actions">
            @unless(empty($actions) or empty($a))
              @foreach($a as $action)
                <a title="{{ $action['label'] }}"
                   @if(!empty($action['link']))
                   href="{{ $action['link'] }}"
                   @elseif(!empty($action['modal']))
                   href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                        @endif
                >
                  <span class="{{ $action['icon'] }}"></span>
                </a>
              @endforeach
            @endunless
            @can('view', $pessoa)
              <a title="Ver" href="{{ route('pessoas.show', $pessoa) }}">
                <span class="far fa-angle-right"></span>
              </a>
            @endcan
          </div>
        @endunless
      </div>
    @endforeach
  </section>
@else
  <section class="sautor-list--empty">
    {{ empty($emptyMessage) ? 'Não estão registadas pessoas.' : $emptyMessage }}
  </section>
@endunless
