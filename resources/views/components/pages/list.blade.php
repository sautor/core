@props(['pages', 'full', 'emptyMessage', 'grupo'])

@unless($pages->isEmpty())
    <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless news-list">
        @foreach($pages as $page)
            <article class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    @if($page->imagem)
                        <img src="{{ $page->imagem }}" class="cover">
                    @else
                        <span class="sautor-list__item__logo__icon">
                                <span class="fas fa-file-alt"></span>
                            </span>
                    @endif
                </div>
                <a href="{{ $page->grupo ? $page->grupo->route('paginas.show', $page->slug) : route('paginas.show', $page->slug) }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">{{ $page->titulo }}</p>
                    <p class="sautor-list__item__meta">
                        {{ $page->slug }}
                        @if(empty($grupo) && $page->grupo)
                            &middot; {{ $page->grupo->nome_curto }}
                        @endif
                    </p>
                </a>
                    <div class="sautor-list__item__actions">
                        <a title="Editar" href="{{ empty($grupo) ? route('admin.paginas.edit', $page) : $grupo->route('paginas.edit', $page) }}">
                            <span class="fas fa-pencil"></span>
                        </a>
                            <a title="Ver" href="{{ $page->grupo ? $page->grupo->route('paginas.show', $page->slug) : route('paginas.show', $page->slug) }}">
                                <span class="far fa-angle-right"></span>
                            </a>
                    </div>
            </article>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty">
        {{ empty($emptyMessage) ? 'Não existem páginas.' : $emptyMessage }}
    </section>
@endunless
