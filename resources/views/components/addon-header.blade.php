@props(['url', 'icon', 'name'])

<header class="addon-header @unless(empty($nav)) addon-header--with-nav @endunless ">
    <div class="addon-header__bar">
        <a href="{{ @$url ?? '#' }}" class="addon-header__title">
            @isset($icon)
                <span class="addon-header__title__icon">
                <span class="{{ $icon }}"></span>
              </span>
            @endisset
            <span class="addon-header__title__title">
          {{ $name }}
        </span>
        </a>

        @unless(empty($nav))
            <nav class="addon-header__nav">
                {{ $nav }}
            </nav>
        @endunless
    </div>
</header>
