@props(['groups'])

<section class="groups-large-list">
  @forelse($groups as $grp)
    <article class="groups-large-list__item">
      <div class="groups-large-list__item__content">
        <h4 class="groups-large-list__item__name">
          {{ $grp->nome }}
        </h4>
        @unless(empty($grp->descricao))
          <div class="groups-large-list__item__description">
            {!! $grp->descricao !!}
          </div>
        @endunless
        <a class="button" href="{{ $grp->route('index') }}">
          Ver <span class="far fa-angle-right"></span>
        </a>
        {{-- TODO: THIS
        @if(Setting::get('grupo-'.$grp->id.'-show-leaders') and count($grp->responsaveis) > 0)
          <h6>{{ Sautor\pluralize($grp->designacao_responsavel) }}</h6>
          @component('pessoas.list-component', [
          'pessoas' => $grp->responsaveis,
          'meta' => function($p) { return $p->inscricao->cargo; }])
          @endcomponent
        @endif
        --}}
      </div>
      <div class="groups-large-list__item__image">
        @if($grp->logo)
          <img src="{{ $grp->logo }}" alt="{{ $grp->nome }}">
        @endif
      </div>
    </article>
  @empty
    @isset($empty)
      {{ $empty }}
    @else
      <p class="groups-large-list__empty">Não existem grupos</p>
    @endisset
  @endforelse
</section>
