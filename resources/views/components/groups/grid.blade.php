@props(['groups', 'small'])

<div class="groups-grid {{ isset($small) ? 'groups-grid--small' : '' }}">
    @foreach($groups as $group)
        <a href="{{ $group->route('index') }}" class="groups-grid__group">
            @if($group->logo)
                <img class="groups-grid__group__logo" src="{{$group->logo}}" alt="{{$group->nome}}">
            @else
                <span class="groups-grid__group__name">{{$group->nome}}</span>
            @endif
        </a>
    @endforeach
</div>
