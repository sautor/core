@props(['group', 'parent'])

<modal id="restoreModal{{ $group->id }}">
    <div class="modal__body">
        <h3 class="modal__title">
            Restaurar {{empty($parent) ? 'grupo' : $parent->designacao_grupo}}
        </h3>
        <p>
            Tem a certeza que quer restaurar <strong>{{ $group->nome }}</strong>?
        </p>
    </div>
    <form action="{{ empty($parent) ? route('admin.groups.restore') : $parent->route('restore') }}" method="POST" class="modal__footer">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $group->id }}">
        <button type="submit" class="button button--primary">Restaurar</button>
        <button type="button" class="button" @click.prevent="closeModal('restoreModal{{ $group->id }}')">Cancelar</button>
    </form>
</modal>
