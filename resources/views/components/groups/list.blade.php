@props(['grupos', 'badge', 'meta', 'actions', 'full'])

<section class="sautor-list @unless(empty($full)) sautor-list--full @endunless groups-list">
    @foreach($grupos as $grupo)
        <div class="sautor-list__item">
            <div class="sautor-list__item__logo">
                @if($grupo->icon ?? $grupo->logo)
                    <img src="{{ $grupo->icon ?? $grupo->logo }}" alt="{{ $grupo->nome }}">
                @else
                    <span class="sautor-list__item__logo__icon">
                        <span class="fad fa-users"></span>
                    </span>
                @endif
            </div>
            <a @can('view', $grupo) href="{{ $grupo->route('index') }}"
               @endcan class="sautor-list__item__data">
                <p class="sautor-list__item__name">
                    {{ $grupo->nome }}
                    @unless(empty($badge) or empty($b = $badge($grupo)))
                        {!! $b !!}
                    @endunless
                </p>
                @unless(empty($meta) or empty($m = $meta($grupo)))
                    <p class="sautor-list__item__meta">
                        {{ $m }}
                    </p>
                @endunless
            </a>

            @unless((empty($actions) or empty($a = $actions($grupo))) and (Auth::guest() or !Auth::user()->can('view', $grupo)))
                <div class="sautor-list__item__actions">
                    @unless(empty($actions) or empty($a))
                        @foreach($a as $action)
                            <a title="{{ $action['label'] }}"
                               @if(!empty($action['link']))
                               href="{{ $action['link'] }}"
                               @elseif(!empty($action['modal']))
                               href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                    @endif
                            >
                                <span class="{{ $action['icon'] }}"></span>
                            </a>
                        @endforeach
                    @endunless
                    @can('view', $grupo)
                        <a title="Ver" href="{{ $grupo->route('index') }}">
                            <span class="far fa-angle-right"></span>
                        </a>
                    @endunless
                </div>
            @endunless
        </div>
    @endforeach
</section>
