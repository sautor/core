@props(['parent', 'slugs'])

<modal id="addGroup">
    @php($scope = 'addGrupo')
    <form action="{{ empty($parent) ? route('admin.groups.store') : $parent->route('store') }}"
          method="POST" data-vv-scope="{{ $scope }}" @submit.prevent="validateForm($event, 'addGrupo')">
        @csrf
        <div class="modal__body">
            <h3 class="modal__title">Adicionar {{empty($parent) ? 'grupo' : $parent->designacao_grupo}}</h3>

            <x-forms.input name="nome" required rules="required" :scope="$scope" />

            <x-forms.input name="slug" label="Endereço público" required
                           :rules="'required|excluded:'.$slugs->implode(',').'|regex:^[a-z0-9]+(?:-[a-z0-9]+)*$'"
                           help="O endereço público só pode ter letras não acentuadas, números e hífenes."
                           :before="url('grupos').'/'" :scope="$scope"
            />
        </div>
        <div class="modal__footer">
            <button type="submit" class="button button--primary">Adicionar</button>
            <button type="button" class="button" @click.prevent="closeModal('addGroup')">Cancelar</button>
        </div>
    </form>
</modal>
