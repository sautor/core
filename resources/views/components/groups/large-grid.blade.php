@props(['groups'])

@if($groups->isNotEmpty())
<div class="groups-large-grid">
    @foreach($groups as $group)
        <div class="groups-large-grid__item">
            @if($group->logo)
                <img src="{{ $group->logo }}" class="groups-large-grid__item__logo">
            @elseif($group->pai->logo)
                <img src="{{ $group->pai->icon ?? $group->pai->logo }}" class="groups-large-grid__item__logo groups-large-grid__item__logo--parent">
            @else
                 <div class="groups-large-grid__item__icon">
                      <span class="far fa-users"></span>
                 </div>
            @endif
            <p class="groups-large-grid__item__name">
                 {{ $group->nome }}
            </p>
            @unless(empty($group->descricao))
              <div class="groups-large-grid__item__about">
                {!! $group->descricao !!}
              </div>
            @endunless
              <a class="groups-large-grid__item__link" href="{{ $group->route('index') }}">
                <span class="far fa-arrow-right"></span>
              </a>
        </div>
    @endforeach
</div>
@else
    <section class="sautor-list--empty">
        {{ empty($emptyMessage) ? 'Não existem grupos.' : $emptyMessage }}
    </section>
@endif
