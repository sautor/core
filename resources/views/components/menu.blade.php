<ul class="menu @empty($group) menu--invert @endif " :class="{ 'is-open': isMenuOpen }">
    @if($group && $group->pai)
      <li class="menu__item" @if($group->pai->cor) style="color: {{ $group->pai->cor }}"  @endif>
        <a href="{{ $group->pai->route('index') }}" class="menu__item__link">
          <span class="far fa-angle-left mr-1"></span>
          {{ $group->pai->nome_curto }}
        </a>
      </li>
    @endif

    @unless(empty($menu))
        @foreach($menu as $item)
            @isset($item->children)
              <dropdown tag="li" class="menu__item menu__item--has-dropdown" position="{{ $item->align ?? $loop->last ? 'right' : 'left' }}">
                <template v-slot:default="{ toggle }">
                  <a href="#" class="menu__item__link" @click="toggle">
                    {{ $item->label }}
                    <span class="far fa-angle-down ml-2"></span>
                  </a>
                </template>
                <template v-slot:dropdown>
                    @foreach($item->children as $child)
                      <a class="dropdown__item"
                         href="{{ $child->url }}">
                        @isset($child->icon)
                          <span class="dropdown__item__icon fa-fw {{ $child->icon }}"></span>
                        @endisset
                        {{ $child->label }}
                      </a>
                    @endforeach
                </template>
              </dropdown>
            @else
                <li class="menu__item">
                    <a href="{{ $item->url }}" class="menu__item__link">{{ $item->label }}</a>
                </li>
            @endisset
        @endforeach
    @endunless

    @if($addons->isNotEmpty())
        <dropdown tag="li" class="menu__item menu__item--has-dropdown" position="right">
            <template v-slot:default="{ toggle }">
                <a href="#" class="menu__item__link" @click="toggle">
                    <span class="far fa-plus"></span>
                </a>
            </template>
            <template v-slot:dropdown>
                @foreach($addons as $addon)
                    <a class="dropdown__item" href="{{ isset($group) ? $group->route($addon->getEntryRouteForGroup($group)) : route($addon->getEntryRoute()) }}">
                        <span class="dropdown__item__icon far fa-fw fa-{{ $addon->icon }}"></span>
                        {{ $addon->label }}
                    </a>
                @endforeach
            </template>
        </dropdown>
    @endif

    @manages($group)
        <li class="menu__item" @if($group->cor) style="color: {{ $group->cor }}"  @endif>
            <a href="{{ $group->route('membros') }}" class="menu__item__link">
                Gestão
            </a>
        </li>
    @endmanages
</ul>
