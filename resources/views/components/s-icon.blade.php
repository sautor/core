@props(['icon'])

@if(file_exists(public_path('/img/sprite.svg')))
  <svg>
    <use xlink:href="{{ asset('/img/sprite.svg') }}#icon"/>
  </svg>
@else
  <span class="fas fa-{{ isset($icon) ? $icon : 'church' }}"></span>
@endif
