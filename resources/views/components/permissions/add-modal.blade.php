@props(['otherPermissions', 'person'])

<modal id="addPermission" with-close>
    @php($scope = 'adicionar-permission')
    <form action="{{ route('pessoas.permissions.add', $person) }}" method="POST"
          data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
            <h3 class="modal__title">
                Conceder permissão
            </h3>

            <x-forms.select name="permission" label="Permissão" required rules="required" :list="$otherPermissions->pluck('name', 'name')->map(function ($item) { return ucfirst($item); })" :scope="$scope" />

        </div>

        <footer class="modal__footer">
            <button type="submit" class="button button--primary">Atribuir</button>
            <button type="button" class="button" @click.prevent="closeModal('addPermission')">Cancelar</button>
        </footer>
    </form>

</modal>