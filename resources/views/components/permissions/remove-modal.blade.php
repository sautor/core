@props(['permission', 'person'])

<modal id="removePermission{{ $permission->id }}">
    <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
            <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
            <h3 class="modal__confirmation__title">Remover permissão</h3>
            <p class="modal__confirmation__text">
                Tem a certeza que quer revogar a permissão <strong>{{ $permission->name }}</strong> de
                <strong>{{ $person->nome_curto }}</strong>?
            </p>
            @if($person->getPermissionsViaRoles()->contains('id', $permission->id))
                <p class="modal__confirmation__warning">
                    Esta pessoa continuará com esta permissão pois pertence a uma das suas funções.
                </p>
            @endif
        </div>
    </div>
    <form class="modal__footer" method="POST" action="{{ route('pessoas.permissions.remove', [$person, $permission]) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Remover</button>
        <button type="button" class="button" @click.prevent="closeModal('removePermission{{ $permission->id }}')">Cancelar</button>
    </form>
</modal>