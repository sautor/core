@props(['permissions', 'actions', 'empty_message', 'full'])

@unless($permissions->isEmpty())
    <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless roles-list">
        @foreach($permissions as $permission)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
            <span class="sautor-list__item__logo__icon">
                <span class="fas fa-shield-alt"></span>
            </span>
                </div>
                <div class="sautor-list__item__data">
                    <p class="sautor-list__item__name">{{ ucfirst($permission->name) }}</p>
                </div>

                @unless(empty($actions) or empty($a = $actions($permission)))
                    <div class="sautor-list__item__actions">
                        @unless(empty($actions) or empty($a))
                            @foreach($a as $action)
                                <a title="{{ $action['label'] }}"
                                   @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                                   @elseif(!empty($action['modal']))
                                   href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                        @endif
                                >
                                    <span class="{{ $action['icon'] }}"></span>
                                </a>
                            @endforeach
                        @endunless
                    </div>
                @endunless
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty roles-list--empty">
        {{ empty($empty_message) ? 'Esta pessoa não tem permissões.' : $empty_message }}
    </section>
@endunless
