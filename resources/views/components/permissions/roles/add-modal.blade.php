@props(['otherRoles', 'person'])

<modal id="addRole" with-close>
    @php($scope = 'adicionar-role')
    <form action="{{ route('pessoas.permissions.addRole', $person) }}" method="POST"
          data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
            <h3 class="modal__title">
                Atribuir função
            </h3>

            <x-forms.select name="role" label="Função" required rules="required" :list="$otherRoles->pluck('name', 'name')->map(function ($item) { return ucfirst($item); })" :scope="$scope" />

        </div>

        <footer class="modal__footer">
            <button type="submit" class="button button--primary">Atribuir</button>
            <button type="button" class="button" @click.prevent="closeModal('addRole')">Cancelar</button>
        </footer>
    </form>

</modal>