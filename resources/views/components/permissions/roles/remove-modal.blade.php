@props(['role', 'person'])

<modal id="removeRole{{ $role->id }}">
    <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
            <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
            <h3 class="modal__confirmation__title">Remover função</h3>
            <p class="modal__confirmation__text">
                Tem a certeza que quer remover a função <strong>{{ $role->name }}</strong> de
                <strong>{{ $person->nome_curto }}</strong>?
            </p>
            <div class="modal__confirmation__warning">
                <p>Esta pessoa perderá as seguintes permissões:</p>

                <ul class="mt-1 list-disc list-inside">
                    @foreach($role->permissions->pluck('name')->diff($person->permissions->pluck('name')) as $permission)
                        <li>{{ ucfirst($permission) }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <form class="modal__footer" method="POST" action="{{ route('pessoas.permissions.removeRole', [$person, $role]) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Remover</button>
        <button type="button" class="button" @click.prevent="closeModal('removeRole{{ $role->id }}')">Cancelar</button>
    </form>
</modal>