@props(['roles', 'actions', 'empty_message', 'full'])

@unless($roles->isEmpty())
    <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless roles-list">
        @foreach($roles as $role)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
            <span class="sautor-list__item__logo__icon">
                <span class="fas fa-user-tag"></span>
            </span>
                </div>
                <div class="sautor-list__item__data">
                    <p class="sautor-list__item__name">{{ ucfirst($role->name) }}</p>
                </div>

                @unless(empty($actions) or empty($a = $actions($role)))
                    <div class="sautor-list__item__actions">
                        @unless(empty($actions) or empty($a))
                            @foreach($a as $action)
                                <a title="{{ $action['label'] }}"
                                   @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                                   @elseif(!empty($action['modal']))
                                   href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                        @endif
                                >
                                    <span class="{{ $action['icon'] }}"></span>
                                </a>
                            @endforeach
                        @endunless
                    </div>
                @endunless
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty roles-list--empty">
        {{ empty($empty_message) ? 'Esta pessoa não tem funções atribuídas.' : $empty_message }}
    </section>
@endunless
