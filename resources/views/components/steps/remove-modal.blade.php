@props(['step'])

<modal id="removeStep{{ $step->id }}">
    <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
            <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
            <h3 class="modal__confirmation__title">Remover passo</h3>
            <p class="modal__confirmation__text">
                Tem a certeza que quer remover o passo <strong>{{ $step->nome }}</strong> de
                <strong>{{ $step->pessoa->nome_curto }}</strong>?
            </p>
        </div>
    </div>
    <form class="modal__footer" method="POST" action="{{ route('pessoas.passos.destroy', [$step->pessoa, $step]) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Remover</button>
        <button type="button" class="button" @click.prevent="closeModal('removeStep{{ $step->id }}')">Cancelar</button>
    </form>
</modal>