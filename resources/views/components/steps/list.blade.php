@props(['steps', 'actions', 'empty_message', 'full'])

@unless($steps->isEmpty())
    <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless steps-list">
        @foreach($steps as $step)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    @if($step->grupo and ($step->grupo->icon ?? $step->grupo->logo))
                        <img src="{{ $step->grupo->icon ?? $step->grupo->logo }}" alt="{{ $step->grupo->nome_curto }}">
                    @else
                        <span class="sautor-list__item__logo__icon">
                        <span class="fas fa-bookmark"></span>
                    </span>
                    @endif
                </div>
                <div class="sautor-list__item__data">
                    <p class="sautor-list__item__name">{{ $step->nome }}</p>
                    <p class="sautor-list__item__meta">
                        {{ $step->data->isoFormat("LL") }}
                        @if($step->grupo)
                        &middot; {{ $step->grupo->nome }}
                        @endif
                    </p>
                </div>

                @unless(empty($actions) or empty($a = $actions($step)))
                    <div class="sautor-list__item__actions">
                        @unless(empty($actions) or empty($a))
                            @foreach($a as $action)
                                <a title="{{ $action['label'] }}"
                                   @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                                   @elseif(!empty($action['modal']))
                                   href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                                        @endif
                                >
                                    <span class="{{ $action['icon'] }}"></span>
                                </a>
                            @endforeach
                        @endunless
                    </div>
                @endunless
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty steps-list--empty">
        {{ empty($empty_message) ? 'Não existem passos registados.' : $empty_message }}
    </section>
@endunless
