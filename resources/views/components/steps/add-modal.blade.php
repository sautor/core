@props(['person'])

<modal id="addStep" with-close>
    @php($scope = 'adicionar-passo')
    <form action="{{ route('pessoas.passos.store', $person) }}" method="POST"
          data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
            <h3 class="modal__title">
                Registar passo
            </h3>

            <x-forms.input name="nome" required rules="required" :list="['Batismo', 'Primeira Comunhão', 'Crisma']" :scope="$scope" />
            <x-forms.input name="data" required rules="required|date_format:yyyy-MM-dd" type="date" :scope="$scope" />

        </div>

        <footer class="modal__footer">
            <button type="submit" class="button button--primary">Registar</button>
            <button type="button" class="button" @click.prevent="closeModal('addStep')">Cancelar</button>
        </footer>
    </form>

</modal>