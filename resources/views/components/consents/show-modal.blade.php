@props(['consent', 'person'])

<modal id="consent{{ $consent->id }}" with-close>
  <div class="modal__body">
    <h3 class="modal__title">
      Consentimento
      <small class="text-gray-500 text-sm">
        {{ $consent->digital ? 'Digital' : 'Físico' }}
        @if($consent->revoked_at) <span class="badge badge--danger">Revogado</span> @endif
      </small>
    </h3>
    @if($consent->ip)
      <p class="consent-modal__item">
        <span class="consent-modal__item__label">Endereço IP</span>
        {{ $consent->ip }}
      </p>
    @endif
    @unless($consent->pessoa->is($person))
      <div class="consent-modal__item">
        <span class="consent-modal__item__label">Consentimento dado para</span>
        <x-people.show :person="$consent->pessoa" />
      </div>
    @endunless
    @if($consent->assinado_por_id)
      <div class="consent-modal__item">
        <span class="consent-modal__item__label">Assinado por</span>
        <x-people.show :person="$consent->assinado_por" />
      </div>
    @endif
    @if($consent->added_by_id)
      <div class="consent-modal__item">
        <span class="consent-modal__item__label">Registado por</span>
        <x-people.show :person="$consent->added_by" />
      </div>
    @endif
    <p class="consent-modal__item">
      <span class="consent-modal__item__label">Registado a</span>
      {{ $consent->created_at->isoFormat('LLL') }}
    </p>
    @if($consent->revoked_at)
      <p class="consent-modal__item">
        <span class="consent-modal__item__label">Revogado a</span>
        {{ $consent->revoked_at->isoFormat('LLL') }}
      </p>
    @endif
  </div>
  @if($consent->ficheiro)
    <footer class="modal__footer">
      <a href="{{ route('pessoas.consentimentos.show', [$consent->pessoa, $consent]) }}"
         class="button button--primary">
        <span class="far fa-file-download mr-1"></span>
        Transferir
      </a>
    </footer>
  @endif
</modal>
