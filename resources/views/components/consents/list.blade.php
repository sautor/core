@props(['person', 'consents', 'actions', 'empty_message', 'full'])

@if($person->descendentes()->exists())
  <div class="alert alert--info mb-6">
    Caso o consentimento tenha sido assinado por {{ $person->nome_curto }} para
    {!! $person->descendentes->map(function ($p){ return '<a href="'.route('pessoas.show', $p).'">'.e($p->nome_curto).'</a>'; })->join(', ', ' ou ') !!}
    deverá ser adicionado na respetiva ficha pessoal.
  </div>
@endif
@unless($consents->isEmpty())
  <section class="sautor-list @unless(empty($full)) sautor-list--full @endunless consents-list">
    @foreach($consents as $consent)
      <div class="sautor-list__item">
        <div class="sautor-list__item__logo">
          <span class="sautor-list__item__logo__icon">
              <span class="fas fa-file-contract"></span>
          </span>
        </div>
        <div class="sautor-list__item__data">
          <p class="sautor-list__item__name">
            Consentimento <strong>{{ $consent->digital ? "digital" : "físico" }}</strong>
            @if($consent->revoked_at)
              <span class="badge badge--danger">Revogado</span>
            @endif
          </p>
          <p class="sautor-list__item__meta">
            Registado a
            {{ $consent->created_at->isoFormat("LL") }}
            @unless($consent->pessoa->is($person))
              para {{ $consent->pessoa->nome_curto }}
            @endunless
            @if($consent->revoked_at)
              &middot; Revogado a {{ $consent->revoked_at->isoFormat("LL") }}
            @endif
          </p>
        </div>

        @unless(empty($actions) or empty($a = $actions($consent)))
          <div class="sautor-list__item__actions">
            @unless(empty($actions) or empty($a))
              @foreach($a as $action)
                @unless(empty($action))
                  <a title="{{ $action['label'] }}"
                     @if(!empty($action['link']))
                     href="{{ $action['link'] }}"
                     @elseif(!empty($action['modal']))
                     href="#" @click.prevent="openModal('{{ $action['modal'] }}')"
                      @endif
                  >
                    <span class="{{ $action['icon'] }}"></span>
                  </a>
                @endunless
              @endforeach
            @endunless
          </div>
        @endunless
      </div>
    @endforeach
  </section>
@else
  <section class="sautor-list--empty consentimentos-list--empty">
    {{ empty($empty_message) ? 'Não existem consentimentos.' : $empty_message }}
  </section>
@endunless
