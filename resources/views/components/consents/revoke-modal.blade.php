@props(['consent'])

<modal id="revokeConsent{{ $consent->id }}">
    <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
            <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
            <h3 class="modal__confirmation__title">Revogar consentimento</h3>
            <p class="modal__confirmation__text">
                Tem a certeza que quer revogar este consentimento ativo de
                <strong>{{ $consent->pessoa->nome_exibicao }}</strong>?
            </p>
            <p class="modal__confirmation__danger">
                <strong>Atenção!</strong> Remover este consentimento é uma ação irreversível.
                @if(Setting::get('gdpr-blockData'))
                    Ao removê-lo, perderá o acesso a toda a informação sobre esta pessoa.
                @endif
            </p>
        </div>
    </div>
    <form class="modal__footer" method="POST" action="{{ route('pessoas.consentimentos.destroy', [$consent->pessoa, $consent]) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Revogar</button>
        <button type="button" class="button" @click.prevent="closeModal('revokeConsent{{ $consent->id }}')">Cancelar</button>
    </form>
</modal>