@props(['person'])

<modal id="addConsent" with-close>
    @php($scope = 'adicionar-consentimento')
    <form action="{{ route('pessoas.consentimentos.store', $person) }}" method="POST"
          enctype="multipart/form-data" data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
            <h3 class="modal__title">
                Registar consentimento
            </h3>

            <x-forms.file name="ficheiro" label="Ficheiro" required accept=".png,.jpg,.jpeg,.pdf"
                          rules="required|ext:png,jpg,jpeg,pdf" :scope="$scope" />

            <pessoa-input v-validate="'{{ ($person->idade < 16 ? 'required|' : '').'pes_exists' }}'" name="assinado_por" @if($person->idade < 16) required @endif
                data-vv-scope="{{ $scope }}" scope="{{ $scope }}"
                label="Assinado por @if($person->idade >= 16)(se não por {{ $person->nome_curto }}) @endif "
                data-vv-as="Assinado por"></pessoa-input>
        </div>

        <footer class="modal__footer">
            <button type="submit" class="button button--primary">Registar</button>
            <button type="button" class="button" @click.prevent="closeModal('addConsent')">Cancelar</button>
        </footer>
    </form>

</modal>