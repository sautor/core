@props(['current' => 1, 'stepLabel'])
<div {{ $attributes->merge(['class' => 'flex gap-4']) }}>
    {{ $slot }}
</div>
