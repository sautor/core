@props(['idx' => 1, 'icon'])
@aware(['current' => 1, 'stepLabel'])

<div class="flex flex-col flex-1 border-t-2 transition-colors {{ $current >= $idx ? 'border-primary-500' : 'border-gray-200'}} pt-2">
    <p class="font-medium text-xs text-primary-600">
        @if(empty($stepLabel))
            Passo {{ $idx }}
        @else
            {{ $stepLabel($idx) }}
        @endif

        @unless(empty($icon))
            <span class="transition-colors float-right fad fa-{{ $icon }} h-4 {{ $current >= $idx ? 'text-primary-500' : 'text-gray-300'}}"></span>
        @endunless
    </p>
    <p class="font-semibold text-xs md:text-sm">{{ $slot }}</p>
</div>
