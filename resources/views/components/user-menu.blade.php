<dropdown position="right">
    <template v-slot:default="{ toggle }">
        {{ $slot }}
    </template>
    <template v-slot:dropdown>
        <a class="dropdown__item" href="{{ route('me.account') }}">
            <span class="dropdown__item__icon far fa-fw fa-user-circle"></span>
            A minha conta
        </a>
        @if(Auth::user()->grupos()->exists())
            <a class="dropdown__item" href="{{ route('me.groups') }}">
                <span class="dropdown__item__icon far fa-fw fa-users"></span>
                Os meus grupos
            </a>
        @endif
        @canany(['ver pessoas', 'gerir pessoas', 'gerir grupos', 'gerir permissões', 'gerir notícias', 'gerir páginas', 'gerir opções', 'gerir eventos'])
            <a class="dropdown__item" href="{{ route('admin.dashboard') }}">
                <span class="dropdown__item__icon far fa-fw fa-user-tie"></span>
                Administração
            </a>
        @endcanany
        @php($restrictAddons = AddonService::forMenu(true))
        @if($restrictAddons->isNotEmpty())
            <div class="dropdown__divider dropdown__divider--with-icon"></div>
            @foreach($restrictAddons as $addon)
                <a class="dropdown__item" href="{{ route($addon->getEntryRoute()) }}">
                    <span class="dropdown__item__icon far fa-fw fa-{{ $addon->icon }}"></span>
                    {{ $addon->label }}
                </a>
            @endforeach
        @endif
        <div class="dropdown__divider dropdown__divider--with-icon"></div>
        <a class="dropdown__item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
            <span class="dropdown__item__icon far fa-fw fa-sign-out"></span>
            Sair
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST"
              style="display: none;">
            @csrf
        </form>
    </template>
</dropdown>
