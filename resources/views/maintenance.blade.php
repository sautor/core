<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    @vite(['resources/sass/maintenance.scss'])

    @include('layouts.partials.custom-styles')

</head>
<body class="maintenance">
    <div class="maintenance__logo">
        @if(Setting::get('site-logo'))
            <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
        @else
            {{ config('app.name') }}
        @endif
    </div>
    <section class="maintenance__content">
        <h1>{{ Setting::get('man-title') }}</h1>
        <h5>{{ Setting::get('man-subtitle') }}</h5>
        <div class="prose">
            {!! Setting::get('man-content') !!}
        </div>
    </section>
    <a href="{{ route('login') }}" class="maintenance__login button button--icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width: 1rem;height: 1rem; fill: currentColor;"><path d="M416 448h-84c-6.6 0-12-5.4-12-12v-24c0-6.6 5.4-12 12-12h84c26.5 0 48-21.5 48-48V160c0-26.5-21.5-48-48-48h-84c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h84c53 0 96 43 96 96v192c0 53-43 96-96 96zM167.1 83.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L260.8 230H12c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L147.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z"/></svg>
    </a>
</body>
</html>
