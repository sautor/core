@extends('layouts.app')

@section('content')
    <div class="people-create__container">
        <h1 class="title mb-12">Registar pessoa</h1>

        @php($scope = 'new-pessoa')
        <form action="{{ route('pessoas.store') }}" method="POST" data-vv-scope="{{ $scope }}"
              @submit.prevent="validateForm">
            @csrf

            @unless(empty($relacao))
                <input type="hidden" name="relacao" value="{{ $relacao }}">
                <input type="hidden" name="relate_with" value="{{ $related_person->id }}">
                <div class="alert alert--info mb-6">
                    Esta pessoa será registada com <strong>{{ $related_person->nome }}</strong> como
                    <strong>{{ Sautor\Core\Models\Relacao::DESC_NAMES[$relacao][$related_person->genero] }}</strong>.
                </div>
            @endunless

            @unless(empty($grupo) and empty($anoletivo) and empty($cargo) and empty($responsavel))
                <input type="hidden" name="anoletivo" value="{{ $anoletivo }}">
                <input type="hidden" name="cargo" value="{{ $cargo }}">
                <input type="hidden" name="responsavel" value="{{ $responsavel }}">
                <input type="hidden" name="register_in" value="{{ $grupo->id }}">
                <div class="alert alert--info mb-6">
                    Esta pessoa será inscrita
                    @if($anoletivo == 1)
                        durante este ano letivo
                    @endif
                    @unless(empty($cargo))
                        com o cargo <strong>{{ $cargo }}</strong>
                    @endunless
                    @if($responsavel == 1)
                        como responsável de
                    @else
                        em
                    @endif
                    <strong>{{ $grupo->nome }}</strong>.
                </div>
            @endunless

            @include('people.form')

            <div class="people-create__actions">
                <button type="submit" class="button button--primary">Registar</button>
            </div>
        </form>
    </div>
@endsection