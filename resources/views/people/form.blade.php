<div class="people-form__section">
    <header class="people-form__section__header">
        <h3 class="people-form__section__title">
            Informação pessoal
        </h3>
    </header>
    <div class="group-settings__section__content">
        <div class="group-settings__section__body">

            <x-forms.input name="nome" label="Nome completo" required rules="required" :value="isset($pessoa) ? $pessoa->nome : null" :scope="$scope" />
            <x-forms.input name="data_nascimento" label="Data de nascimento" :value="isset($pessoa->data_nascimento) ? $pessoa->data_nascimento->format('Y-m-d') : null" rules="date_format:yyyy-MM-dd" type="date" :options="['placeholder' => 'AAAA-MM-DD']" :scope="$scope" />
            <x-forms.radio name="genero" label="Género" required rules="required" :value="isset($pessoa) ? $pessoa->genero : null" :list="['M' => 'Masculino', 'F' => 'Feminino']" />
            <x-forms.input name="profissao" label="Profissão" :value="isset($pessoa) ? $pessoa->profissao : null" :scope="$scope" />
            <x-forms.input name="nic" label="Número de Identificação Civil" required :rules="'required|unique_pes:nic'.(isset($pessoa)?','.$pessoa->id:'')" help="N.º de Cartão de Cidadão" :value="isset($pessoa) ? $pessoa->nic : null" :scope="$scope" />
            <x-forms.input name="nus" label="Número de Utente de Saúde" :value="isset($pessoa) ? $pessoa->nus : null" :scope="$scope" />
            <x-forms.input name="nif" label="Número de Identificação Fiscal" :value="isset($pessoa) ? $pessoa->nif : null" :scope="$scope" />


        </div>
    </div>
</div>

@if(Auth::user()->can('gerir pessoas'))

<div class="people-form__section">
    <header class="people-form__section__header">
        <h3 class="people-form__section__title">
            Informação eclesial
        </h3>
    </header>
    <div class="group-settings__section__content">
        <div class="group-settings__section__body">

            <x-forms.radio name="ordem" label="Ordem" required :list="[null => 'Nenhuma', 'diacono' => 'Diaconado', 'sacerdote' => 'Sacerdócio']" :value="isset($pessoa) ? $pessoa->ordem : null" :scope="$scope" />

        </div>
    </div>
</div>

@endif

<div class="people-form__section">
    <header class="people-form__section__header">
        <h3 class="people-form__section__title">
            Informação de contacto
        </h3>
    </header>
    <div class="group-settings__section__content">
        <div class="group-settings__section__body">

            <x-forms.textarea name="morada" label="Morada" :value="isset($pessoa) ? $pessoa->morada : null" :scope="$scope" :options="['rows' => 2]" />
            <x-forms.input name="email" label="Endereço de e-mail" type="email" :value="isset($pessoa) ? $pessoa->email : null" :scope="$scope" :rules="'email|unique_pes:email'.(isset($pessoa)?','.$pessoa->id:'')" />
            <x-forms.input name="telefone" label="Número de telefone" type="tel" :value="isset($pessoa) ? $pessoa->telefone : null" :scope="$scope" />


        </div>
    </div>
</div>