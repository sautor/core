@extends('layouts.app')

@section('content')
  <div class="people-show__container">
    <header class="people-show__header">
      <div class="people-show__photo">
        <img src="{{ $pessoa->foto }}">
        @can('update', $pessoa)
          <button type="button" class="button button--small button--icon" @click.prevent="openModal('changePhoto')">
            <span class="fas fa-camera fa-fw"></span>
          </button>
        @endcan
      </div>

      <h1 class="people-show__name">{{ $pessoa->nome_exibicao }}</h1>
      <div class="people-show__header__actions">

        <dropdown position="right">
          <template v-slot:default="{ toggle }">
            <button class="button" type="button" @click="toggle">
              Contactar
              <span class="far fa-angle-down ml-2 -mr-2"></span>
            </button>
          </template>
          <template v-slot:dropdown>
            @unless(empty($pessoa->email))
              <a href="mailto:{{ $pessoa->email }}" class="dropdown__item">
                <span class="dropdown__item__icon fa-fw far fa-at"></span>
                Enviar e-mail
              </a>
            @endunless
            @unless(empty($pessoa->email) && empty($pessoa->telefone))
                <hr class="dropdown__divider dropdown__divider--with-icon">
            @endunless
            @unless(empty($pessoa->telefone))
              <a href="tel:{{ $pessoa->telefone->formatForMobileDialingInCountry('PT') }}" class="dropdown__item">
                <span class="dropdown__item__icon fa-fw far fa-phone"></span>
                Telefonar
              </a>
              <a href="sms:{{ $pessoa->telefone->formatForMobileDialingInCountry('PT') }}" class="dropdown__item">
                <span class="dropdown__item__icon fa-fw far fa-sms"></span>
                Enviar SMS
              </a>
              <a href="https://api.whatsapp.com/send?phone={{ $pessoa->telefone->formatE164() }}" class="dropdown__item">
                <span class="dropdown__item__icon fa-fw fab fa-whatsapp"></span>
                WhatsApp
              </a>
            @endunless
          </template>
        </dropdown>

        @can('update', $pessoa)
          <a href="{{ $pessoa->is(Auth::user()) ? route('me.editProfile') : route('pessoas.edit', $pessoa) }}"
             class="button button--primary">
            <span class="far fa-user-edit mr-1"></span>
            Editar
          </a>
        @endcan
      </div>
    </header>

    @unless($pessoa->hasActiveConsent())
      <div class="alert alert--danger mb-8 flex flex-col md:flex-row" role="alert">
        <div class="mb-2 md:mb-0 md:mr-6 text-xl text-red-800">
          <span class="fas fa-exclamation-triangle"></span>
        </div>
        <div class="md:flex-1">
          <h4 class="font-bold text-red-800 mb-3">Esta pessoa não tem nenhum consentimento ativo!</h4>
          <p class="text-sm mb-2">
            O tratamento destes dados pode constituir uma violação do
            <abbr title="Regulamento Geral da Proteção de Dados">RGPD</abbr>.
          </p>
          <p class="text-sm">
            Pode adicionar um consentimento físico na secção <strong>Consentimentos</strong> abaixo
            <strong>ou</strong> a pessoa poderá dá-lo digitalmente entrando com os seus dados.
          </p>
        </div>
      </div>
    @endunless

    <side-tabs>
      <tab title="Perfil" icon="fa-user">
        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Informação pessoal</h2>
        </header>
        <div class="people-show__tab__card">
          <div class="flex gap-6">
            <p class="people-show__item flex-1">
              <span class="people-show__item__label">Nome</span>
              @empty($pessoa->nome)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nome }}
              @endempty
            </p>
            <p class="people-show__item">
              <span class="people-show__item__label">Género</span>
              @empty($pessoa->genero)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->genero === 'M' ? 'Masculino' : 'Feminino' }}
              @endempty
            </p>
            @unless(empty($pessoa->ordem))
            <p class="people-show__item">
              <span class="people-show__item__label">Ordem</span>
              {{ $pessoa->ordem === 'sacerdote' ? 'Sacerdócio' : 'Diaconado' }}
            </p>
              @endunless
          </div>


          <div class="grid grid-cols-2 gap-6">
            <p class="people-show__item">
              <span class="people-show__item__label">Data de nascimento</span>
              @empty($pessoa->data_nascimento)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->data_nascimento->isoFormat('LL') }}
                <em>({{ $pessoa->idade }})</em>
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Profissão</span>
              @empty($pessoa->profissao)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->profissao }}
              @endempty
            </p>
          </div>

          <div class="grid grid-cols-3 gap-6 -mb-6">

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Identificação Civil</span>
              @empty($pessoa->nic)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nic }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Utente de Saúde</span>
              @empty($pessoa->nus)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nus }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Identificação Fiscal</span>
              @empty($pessoa->nif)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nif }}
              @endempty
            </p>

          </div>
        </div>

        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Informação de contacto</h2>
        </header>

        <div class="people-show__tab__card">

          <p class="people-show__item">
            <span class="people-show__item__label">Morada</span>
            @empty($pessoa->morada)
              <span class="people-show__item__empty">(não especificado)</span>
            @else
              <span class="whitespace-pre-line">{{ $pessoa->morada }}</span>
            @endempty
          </p>

          <div class="grid grid-cols-2 gap-6 -mb-6">
            <p class="people-show__item">
              <span class="people-show__item__label">Endereço de e-mail</span>
              @empty($pessoa->email)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->email }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de telefone</span>
              @empty($pessoa->telefone)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->telefone->formatForCountry('PT') }}
              @endempty
            </p>
          </div>

        </div>
      </tab>

      <tab title="Familiares" icon="fa-people-arrows">
        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Familiares</h2>

          @can('update', $pessoa)
            <button class="button button--small" type="button" @click.prevent="openModal('addRelation')">
              <span class="far fa-plus mr-1"></span>
              Adicionar
            </button>
          @endcan
        </header>

        <x-people.list full :pessoas="$familiares" empty_message="Não estão registados familiares." :meta="function($p) use ($pessoa) {
                      if($p->relacao) {
                          return ucfirst($p->relacao->nomeFrom($pessoa));
                      }
                      return $p->genero === 'M' ? 'Irmão' : 'Irmã';
                  }" :actions="function($p) use ($pessoa) {
                      if(!Auth::user()->can('update', $p)) return null;
                      if(empty($p->relacao)) return null;
                      return [
                          ['icon' => 'far fa-fw fa-unlink', 'label' => 'Remover relação', 'modal' => 'removeRelation'.$p->id]
                      ];
                  }" />
      </tab>

      <tab title="Passos" icon="fa-bookmark">
        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Passos</h2>
          <button class="button button--small" type="button" @click.prevent="openModal('addStep')">
            <span class="far fa-plus mr-1"></span>
            Registar
          </button>
        </header>

        <x-steps.list full :steps="$pessoa->passos"
                         :actions="function($p) use ($pessoa) {
                      if(!Auth::user()->can('update', $pessoa)) return null;
                      return [
                          ['icon' => 'fas fa-fw fa-trash', 'label' => 'Eliminar', 'modal' => 'removeStep'.$p->id]
                      ];
                  }"/>
      </tab>

      <tab title="Consentimentos" icon="fa-file-contract">
        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Consentimentos</h2>
          <button class="button button--small" type="button" @click.prevent="openModal('addConsent')">
            <span class="far fa-plus mr-1"></span>
            Registar
          </button>
        </header>

        <x-consents.list full :person="$pessoa" :consents="$pessoa->allConsentimentos()"
          :actions="function($c) use ($pessoa) {
                          if(!Auth::user()->can('update', $pessoa)) return null;
                          return [
                              $c->revoked_at ? null : ['icon' => 'far fa-fw fa-ban', 'label' => 'Revogar', 'modal' => 'revokeConsent'.$c->id],
                              ['icon' => 'far fa-fw fa-eye', 'label' => 'Ver', 'modal' => 'consent'.$c->id]
                          ];
                      }"/>
      </tab>

      @if($pessoa->grupos()->exists() or $pessoa->historial()->exists())
        <tab title="Grupos" icon="fa-users">
          <div class="flex flex-col lg:flex-row gap-6">
            @if($pessoa->grupos()->exists())
              <div class="lg:flex-1">
                <header class="people-show__tab__header">
                  <h2 class="people-show__tab__title">Grupos a que pertence</h2>
                </header>

                <x-groups.list full :grupos="$pessoa->grupos" :meta="function($g) { return $g->inscricao->cargo; }" :badge="function($g) { if($g->inscricao->responsavel) { return '<span class=\'text-yellow-500 fas fa-badge\'></span>'; } return null; }" />
              </div>
            @endif
            @if($pessoa->historial()->exists())
              <div class="lg:flex-1">
                <header class="people-show__tab__header">
                  <h2 class="people-show__tab__title">Grupos a que pertenceu</h2>
                </header>

                @foreach($pessoa->historial->groupBy('inscricao.ano_letivo') as $ano => $grupos)
                  <h6 class="font-headline text-gray-500 mt-4 mb-2">{{ $ano ? Sautor\formatAnoLetivo($ano) : 'Sem ano' }}</h6>

                  <x-groups.list full :grupos="$grupos" :meta="function($g) { return $g->inscricao->cargo . ($g->inscricao->deleted_at ? ' até '.$g->inscricao->deleted_at->isoFormat('ll') : ''); }" :badge="function($g) { if($g->inscricao->responsavel) { return '<span class=\'text-yellow-500 fas fa-badge\'></span>'; } return null; }" />
                @endforeach
              </div>
            @endif
          </div>
        </tab>
      @endif

      @can('gerir pessoas')
        <tab title="Conta" icon="fa-key">
          <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Conta</h2>
          </header>

          <div class="prose">
            <ul>

              <li>
                {{ $pessoa->nome_curto }}
                @empty($pessoa->email) <span class="font-bold text-red-600">não tem</span> @else <span class="font-bold text-green-600">tem</span> @endempty
                um endereço de e-mail
              </li>

              <li>
                <p>
                  {{ $pessoa->nome_curto }}
                  @empty($pessoa->password) <span class="font-bold text-red-600">não tem</span> @else <span class="font-bold text-green-600">tem</span> @endempty
                  uma password definida
                </p>
                <a href="{{ route('pessoas.sendResetLink', $pessoa) }}" class="button button--small">
                  <span class="far fa-lock fa-fw mr-1"></span>
                  Enviar {{ $pessoa->password ? 'redefinição' : 'definição' }}
                </a>
              </li>

              <li>
                {{ $pessoa->nome_curto }}
                @unless($pessoa->hasTwoFactorEnabled()) <span class="font-bold text-red-600">não tem</span> @else <span class="font-bold text-green-600">tem</span> @endunless
                a autenticação em dois passos ativa
              </li>

            </ul>
          </div>
        </tab>
      @endcan

      @can('gerir permissões')
        @php($otherRoles = \Spatie\Permission\Models\Role::whereNotIn('id', $pessoa->roles->pluck('id'))->get())
        @php($otherPermissions = \Spatie\Permission\Models\Permission::whereNotIn('id', $pessoa->permissions->pluck('id'))->get())
        <tab title="Permissões" icon="fa-shield-alt" id="permissoes">

          <div class="flex flex-col lg:flex-row gap-6">

            <div class="lg:flex-1">
              <header class="people-show__tab__header">
                <h2 class="people-show__tab__title">Funções</h2>

                @if($otherRoles->isNotEmpty())
                  <button class="button button--small" type="button" @click.prevent="openModal('addRole')">
                    <span class="far fa-plus mr-1"></span>
                    Atribuir
                  </button>
                @endif
              </header>

              <x-permissions.roles.list full :roles="$pessoa->roles" :actions="function($r) use ($pessoa) {
                          return [
                              ['icon' => 'fas fa-fw fa-trash', 'label' => 'Remover', 'modal' => 'removeRole'.$r->id]
                          ];
                      }" />
            </div>

            <div class="lg:flex-1">
              <header class="people-show__tab__header">
                <h2 class="people-show__tab__title">Permissões</h2>

                @if($otherPermissions->isNotEmpty())
                  <button class="button button--small" type="button" @click.prevent="openModal('addPermission')">
                    <span class="far fa-plus mr-1"></span>
                    Conceder
                  </button>
                @endif
              </header>

              <x-permissions.list full :permissions="$pessoa->getAllPermissions()" :actions="function($p) use ($pessoa) {
                          if(!$pessoa->permissions->contains($p)) return[];
                          return [
                              ['icon' => 'fas fa-fw fa-trash', 'label' => 'Remover', 'modal' => 'removePermission'.$p->id]
                          ];
                      }" />
            </div>

          </div>

        </tab>
      @endcan
    </side-tabs>

  </div>

  @can('update', $pessoa)
    <modal id="changePhoto" with-close>
      <form method="POST" action="{{ route('pessoas.updateFoto', $pessoa) }}">
        @csrf
        @method('PATCH')
        <div class="modal__body">
          <h3 class="modal__title">Alterar fotografia</h3>

          <photo-picker name="foto"></photo-picker>
        </div>
        <footer class="modal__footer">
          <button type="submit" class="button button--primary">Alterar</button>
          <button type="button" class="button" @click.prevent="closeModal('changePhoto')">Cancelar</button>
        </footer>
      </form>
    </modal>

    <modal id="addRelation" with-close>
      @php($scope = 'adicionar-familiar')
      <form class="modal-content" method="POST" action="{{ route('pessoas.relacoes.store', $pessoa) }}"
            data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
          <h3 class="modal__title">Adicionar familiar</h3>

          <pessoa-input v-validate="'required|excluded:{{ $pessoa->id }}'" name="nic"
                        data-vv-scope="{{ $scope }}" required
                        scope="{{ $scope }}" data-vv-as="Número de Identificação Civil"></pessoa-input>

          <x-forms.radio name="relacao" label="Relação" required rules="required|included:pai,avo,padrinho,padrasto" :scope="$scope"
                         :list="['pai' => 'Pai/Mãe', 'avo' => 'Avô/Avó', 'padrinho' => 'Padrinho/Madrinha', 'padrasto' => 'Padrasto/Madrasta']" />

        </div>
        <footer class="modal__footer">
          <button type="submit" class="button button--primary">Adicionar</button>
          <button type="button" class="button" @click.prevent="closeModal('addRelation')">Cancelar</button>
        </footer>
      </form>
    </modal>

    @foreach($familiares as $familiar)
      @if($familiar->relacao)
        <modal id="removeRelation{{ $familiar->id }}">
          <div class="modal__body modal__confirmation">
            <div class="modal__confirmation__icon">
              <span class="fas fa-exclamation"></span>
            </div>
            <div class="modal__confirmation__content">
              <h3 class="modal__confirmation__title">Remover relação</h3>
              <p class="modal__confirmation__text">
                Tem a certeza que quer remover a relação entre <strong>{{ $pessoa->nome_exibicao }}</strong> e
                <strong>{{ $familiar->nome_exibicao }}</strong>?
              </p>
            </div>
          </div>
          <form class="modal__footer" method="POST" action="{{ route('pessoas.relacoes.destroy', [$pessoa, $familiar]) }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="button button--danger">Remover</button>
            <button type="button" class="button" @click.prevent="closeModal('removeRelation{{ $familiar->id }}')">Cancelar</button>
          </form>
        </modal>
      @endif
    @endforeach


    <x-steps.add-modal :person="$pessoa" />

    @foreach($pessoa->passos as $step)
      <x-steps.remove-modal :step="$step" />
    @endforeach

    <x-consents.add-modal :person="$pessoa" />
  @endcan

  @foreach($pessoa->allConsentimentos() as $consentimento)
    <x-consents.show-modal :consent="$consentimento" :person="$pessoa" />
    @can('update', $pessoa)
      <x-consents.revoke-modal :consent="$consentimento" />
    @endcan
  @endforeach

  @can('gerir permissões')

    @if($otherRoles->isNotEmpty())
      <x-permissions.roles.add-modal :otherRoles="$otherRoles" :person="$pessoa" />
    @endif

    @foreach($pessoa->roles as $role)
      <x-permissions.roles.remove-modal :role="$role" :person="$pessoa" />
    @endforeach

    @if($otherPermissions->isNotEmpty())
      <x-permissions.add-modal :otherPermissions="$otherPermissions" :person="$pessoa" />
    @endif

    @foreach($pessoa->permissions as $permission)
      <x-permissions.remove-modal :permission="$permission" :person="$pessoa" />
    @endforeach

  @endcan

@endsection
