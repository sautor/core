@extends('layouts.app')

@section('content')
    <div class="people-edit__container">
        <h1 class="title mb-12">Editar pessoa</h1>

        @php($scope = 'edit-pessoa')
        <form action="{{ route('pessoas.update', $pessoa) }}" method="POST" data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
            @csrf
            @method('PUT')

            @include('people.form')

            <div class="people-edit__actions">
                <button type="submit" class="button button--primary">Editar</button>
            </div>
        </form>
    </div>
@endsection