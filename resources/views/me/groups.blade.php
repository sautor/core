@extends('layouts.app')

@section('content')
  <div class="me-groups__container">
    <h1 class="me-groups__title">Os meus grupos</h1>
    <x-groups.grid :groups="Auth::user()->grupos" />
  </div>
@endsection
