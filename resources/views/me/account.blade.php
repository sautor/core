@extends('layouts.app')

@section('content')
  <div class="me-account">
    <side-tabs>
      <tab title="Perfil" icon="fa-user">
        @php($pessoa = Auth::user())

        <header class="people-show__header">
          <div class="people-show__photo">
            <img src="{{ $pessoa->foto }}">
          </div>

          <h1 class="people-show__name">{{ $pessoa->nome_exibicao }}</h1>
          <div class="people-show__header__actions">
              <a href="{{ route('me.editProfile') }}"
                 class="button button--primary">
                <span class="far fa-user-edit mr-1"></span>
                Editar
              </a>
          </div>
        </header>

        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Informação pessoal</h2>
        </header>
        <div class="people-show__tab__card">
          <div class="flex gap-6">
            <p class="people-show__item flex-1">
              <span class="people-show__item__label">Nome</span>
              @empty($pessoa->nome)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nome }}
              @endempty
            </p>
            <p class="people-show__item">
              <span class="people-show__item__label">Género</span>
              @empty($pessoa->genero)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->genero === 'M' ? 'Masculino' : 'Feminino' }}
              @endempty
            </p>
            @unless(empty($pessoa->ordem))
              <p class="people-show__item">
                <span class="people-show__item__label">Ordem</span>
                {{ $pessoa->ordem === 'sacerdote' ? 'Sacerdócio' : 'Diaconado' }}
              </p>
            @endunless
          </div>


          <div class="grid grid-cols-2 gap-6">
            <p class="people-show__item">
              <span class="people-show__item__label">Data de nascimento</span>
              @empty($pessoa->data_nascimento)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->data_nascimento->isoFormat('LL') }}
                <em>({{ $pessoa->idade }})</em>
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Profissão</span>
              @empty($pessoa->profissao)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->profissao }}
              @endempty
            </p>
          </div>

          <div class="grid grid-cols-3 gap-6 -mb-6">

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Identificação Civil</span>
              @empty($pessoa->nic)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nic }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Utente de Saúde</span>
              @empty($pessoa->nus)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nus }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de Identificação Fiscal</span>
              @empty($pessoa->nif)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->nif }}
              @endempty
            </p>

          </div>
        </div>

        <header class="people-show__tab__header">
          <h2 class="people-show__tab__title">Informação de contacto</h2>
        </header>

        <div class="people-show__tab__card">

          <p class="people-show__item">
            <span class="people-show__item__label">Morada</span>
            @empty($pessoa->morada)
              <span class="people-show__item__empty">(não especificado)</span>
            @else
              <span class="whitespace-pre-line">{{ $pessoa->morada }}</span>
            @endempty
          </p>

          <div class="grid grid-cols-2 gap-6 -mb-6">
            <p class="people-show__item">
              <span class="people-show__item__label">Endereço de e-mail</span>
              @empty($pessoa->email)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->email }}
              @endempty
            </p>

            <p class="people-show__item">
              <span class="people-show__item__label">Número de telefone</span>
              @empty($pessoa->telefone)
                <span class="people-show__item__empty">(não especificado)</span>
              @else
                {{ $pessoa->telefone->formatForCountry('PT') }}
              @endempty
            </p>
          </div>

        </div>

        <a href="{{ route('me.profile') }}" class="button">Ver perfil completo</a>

      </tab>

      <tab title="Segurança" id="seguranca" icon="fa-shield-alt">
        <h1 class="me-account__title">Segurança</h1>
        <section class="me-account__section">
          <header class="me-account__section__header">
            <h3 class="me-account__section__title">Alterar password</h3>
          </header>
          @php($scope = 'change-password')
          <form class="me-account__section__content" method="POST" action="{{ route('me.changePassword') }}" data-vv-scope="{{ $scope }}"
                @submit.prevent="validateForm">
            @csrf
            <div class="me-account__section__body">
              <x-forms.input :scope="$scope" name="password" label="Password atual" required rules="required" type="password" />
              <x-forms.input :scope="$scope" name="new_password" label="Nova password" required rules="required|confirmed:new_password_confirmation" type="password" />
              <x-forms.input :scope="$scope" name="new_password_confirmation" label="Confirmar nova password" required rules="required" type="password" :options="['ref' => 'new_password_confirmation']" />
            </div>
            <div class="me-account__section__footer">
              <button type="submit" class="button button--primary">Alterar</button>
            </div>
          </form>
        </section>
        <section class="me-account__section">
          <header class="me-account__section__header">
            <h3 class="me-account__section__title">Autenticação em dois passos</h3>
            <p class="me-account__section__desc">
              Esta funcionalidade é uma camada extra de segurança para a sua conta
              e pode ser necessária para algumas ações neste site.
            </p>
          </header>
          <div class="me-account__section__content">
            <two-factor
                @if(Auth::user()->hasTwoFactorEnabled()) enabled @endif
                enable-url="{{ route('api.2fa.enable') }}"
                confirm-url="{{ route('api.2fa.confirm') }}"
                disable-url="{{ route('api.2fa.disable') }}"
            />
          </div>
        </section>
      </tab>

      @if($providers->isNotEmpty())
        <tab title="Contas associadas" id="contas" icon="fa-link">
          <h1 class="me-account__title">Contas associadas</h1>
          <div class="sautor-list sautor-list--full">
            @foreach($providers as $provider)
              @php($hasProvider = Auth::user()->socialAccounts->contains('provider', $provider['key']))
              <div class="sautor-list__item">
                <div class="sautor-list__item__status {{ $hasProvider ? 'sautor-list__item__status--success sautor-list__item__status--glow' : 'sautor-list__item__status--danger' }}"></div>
                <div class="sautor-list__item__logo">
                  <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--{{ $provider['key'].($hasProvider ? '' : '--invert') }}">
                    <span class="{{ $provider['icon'] }}"></span>
                  </span>
                </div>
                <div class="sautor-list__item__data">
                  <p class="sautor-list__item__name">{{ $provider['name'] }}</p>
                  <p class="sautor-list__item__meta">
                    @if($hasProvider)
                      Conta associada
                    @else
                      Sem conta associada
                    @endif
                  </p>
                </div>
                <div class="sautor-list__item__actions">
                  @if($hasProvider)
                    <button type="button" @click.prevent="openModal('unlinkModal{{ ucfirst($provider['key']) }}')">
                      <span class="far fa-fw fa-unlink"></span>
                    </button>
                  @else
                    <a href="{{ route('social.redirect', $provider['key']) }}">
                      <span class="far fa-fw fa-plus"></span>
                    </a>
                  @endif
                </div>
              </div>
            @endforeach
          </div>

        </tab>
      @endif

      <tab title="Aplicações" id="aplicacoes" icon="fa-browser">
        <h1 class="me-account__title">Aplicações e sessões</h1>
        <passport-authorized-clients :scopes='{{ json_encode(\Laravel\Passport\Passport::$scopes) }}'></passport-authorized-clients>
      </tab>

      @can('programar com api')
        <tab title="API" icon="fa-code">
          <h1 class="me-account__title">API</h1>

          <passport-clients></passport-clients>

          <passport-personal-access-tokens class="mt-4"></passport-personal-access-tokens>
        </tab>
      @endcan
    </side-tabs>
  </div>

  @foreach(Auth::user()->socialAccounts as $socialAccount)
    <modal id="unlinkModal{{ ucfirst($socialAccount->provider) }}">
      <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
          <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
          <h3 class="modal__confirmation__title">Desassociar conta</h3>
          <p class="modal__confirmation__text">
            Tem a certeza que quer desassociar a sua conta de <strong>{{ $providers[$socialAccount->provider]['name'] }}</strong>?
            Deixará de poder entrar com esta conta.
          </p>
        </div>
      </div>
      <form class="modal__footer" method="POST" action="{{ route('social.unlink', $socialAccount->provider) }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Desassociar</button>
        <button type="button" class="button" @click.prevent="closeModal('unlinkModal{{ ucfirst($socialAccount->provider) }}')">Cancelar</button>
      </form>
    </modal>
  @endforeach
@endsection
