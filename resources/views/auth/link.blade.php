@extends('layouts.auth')

@section('content')
    <form method="POST" action="{{ route('social.linkConfirm', $provider['key']) }}" class="auth-link auth-link--{{ $provider['key'] }}">
        @csrf

        <div class="auth-link__images">
            <div class="auth-link__images__avatar auth-link__images__avatar--social auth-link__images__avatar--{{ $provider['key'] }}">
                <img src="{{ $socialUser->getAvatar() }}" alt="{{ $socialUser->getName() }}">
                @unless(empty($provider['icon']))
                    <div class="auth-link__images__avatar__badge">
                        <span class="{{ $provider['icon'] }}"></span>
                    </div>
                @endunless
            </div>
            <span class="auth-link__images__icon far fa-arrows-h"></span>
            <div class="auth-link__images__avatar auth-link__images__avatar--main">
                <img src="{{ Auth::user()->foto }}" alt="{{ Auth::user()->nome_curto }}">
                <div class="auth-link__images__avatar__badge">
                    <x-s-icon />
                </div>
            </div>
        </div>

        <h2 class="title title--xs mb-4">Associar contas</h2>

        <p class="text-sm text-gray-700 mb-6">
            Quer associar a sua conta de {{ $provider['name'] }} <strong>{{ $socialUser->getName() }}</strong>
            à sua conta {{ config('app.name') }} <strong>{{ Auth::user()->nome_curto }}</strong>?
        </p>

        <input type="hidden" name="provider_user_id" value="{{ $socialUser->getId() }}">

        <button class="button button--primary w-full" type="submit">
            Associar
        </button>
    </form>

@endsection
