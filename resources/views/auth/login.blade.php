@extends('layouts.auth')

@section('content')
  <form method="POST" action="{{ route('login') }}" @submit.prevent="validateForm">
  @csrf
    <x-forms.input name="email" label="Endereço de email" required rules="required|email" />
    <x-forms.input name="password" required rules="required" type="password" />
    <button type="submit" class="button button--primary w-full mb-6">
      Entrar
    </button>
    <div class="flex justify-between items-center">
      <x-forms.checkbox name="remember" label="Lembrar-me" class="mb-0" no-field />
      <a class="text-xs text-primary-500 hover:text-primary-600" href="{{ route('password.request') }}">
        Esqueceu-se da password?
      </a>
    </div>

    @if(empty($grupo) && $providers->isNotEmpty())
      <hr class="mt-6 mb-3 border-gray-200">

      <p class="text-center text-gray-400 text-xs mb-3">Ou entre com um dos seguintes serviços</p>

      <div class="flex gap-2 items-center justify-center flex-wrap">
        @foreach($providers as $provider)
          <a href="{{ route('social.redirect', $provider['key']) }}" class="button button--small button--{{ $provider['key'] }}-outline">
            <span class="{{ $provider['icon'] }} mr-1"></span>
            {{ $provider['name'] }}
          </a>
        @endforeach
      </div>
    @endif
  </form>
@endsection
