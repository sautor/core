@extends('layouts.auth')

@section('content')
    <form action="{{ $action }}" method="post">
        @csrf
        @foreach((array)$credentials as $name => $value)
            <input type="hidden" name="{{ $name }}" value="{{ $value }}">
        @endforeach
        @if($remember)
            <input type="hidden" name="remember" value="on">
        @endif
        <h2 class="font-headline text-xl text-gray-700 mb-6">
            Autenticação em dois passos
        </h2>
        <p class="text-sm text-gray-700 mb-4">
            Para continuar, abra a sua aplicação de Autenticador e insira o seu código de
            <strong>autenticação em dois passos</strong>.
        </p>
        <div class="field @if($error) field--invalid @endif">
            <div class="control">
                <input type="text" name="{{ $input }}" id="{{ $input }}"
                       class="input text-center tabular-nums tracking-wider"
                       minlength="6" autofocus autocomplete="off" required>
            </div>
            @if($error)
                <small class="field__help field__help--invalid">
                    O código é inválido ou expirou.
                </small>
            @endif
        </div>
        <button type="submit" class="button button--primary w-full mb-6">
            Confirmar
        </button>
        <p class="text-sm text-gray-600 text-center">
            <a href="javascript:history.back()" class="hover:underline">
                Voltar
            </a>
        </p>
    </form>
@endsection
