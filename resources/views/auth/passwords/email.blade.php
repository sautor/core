@extends('layouts.auth')

@section('content')
  <form method="POST" action="{{ route('password.email') }}">
    <h1 class="title title--xs mb-6">Redefinir password</h1>
    @if (session('status'))
      <div class="p-4 rounded bg-green-50 text-green-600 mb-4">
        {{ session('status') }}
      </div>
    @endif
    @csrf
    <x-forms.input name="email" label="Endereço de email" type="email" required />
    <button type="submit" class="button button--primary w-full">
      Enviar link de redefinição
    </button>
  </form>
@endsection
