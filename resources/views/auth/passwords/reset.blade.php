@extends('layouts.auth')

@section('content')
  <form method="POST" action="{{ route('password.request') }}">
    <h1 class="title title--xs mb-6">Redefinir password</h1>
    @if (session('status'))
      <div class="p-4 rounded bg-green-50 text-green-600 mb-4">
        {{ session('status') }}
      </div>
    @endif

    @csrf

    <input type="hidden" name="token" value="{{ $token }}">

    <x-forms.input name="email" label="Endereço de e-mail" type="email" required :value="$email" />
    <x-forms.input name="password" type="password" required />
    <x-forms.input name="password_confirmation" label="Confirmar password" type="password" required />

    <button type="submit" class="button button--primary w-full">
      Redefinir Password
    </button>
  </form>

@endsection
