@extends('layouts.app')

@section('content')
    <div class="max-w-prose container mx-auto pt-16 pb-8">
        <div class="alert alert--info mb-8">
            <h5 class="font-bold text-base mb-2">
                <span class="far fa-info-circle text-blue-600"></span>
                Precisamos do seu consentimento
            </h5>
            <p class="mb-1">
                Para cumprirmos com o Regulamento Geral da Proteção de Dados (RGPD), precisamos que nos autorize a tratar os
                seus dados.
            </p>
            <p>
                Como tal, precisamos do seu consentimento antes de poder entrar com a sua conta no site.
            </p>
        </div>

        @if(Setting::has('gdpr-info'))
            <div class="prose max-w-none mb-8">
                {!! Setting::get('gdpr-info') !!}
            </div>
        @endif

        <form action="{{ route('me.storeConsentRequest') }}" method="post" @submit.prevent="validateForm">
            @csrf
            <x-forms.checkbox name="consent" :label="Setting::get('gdpr-authorization')" required />
            <button class="button button--primary" type="submit">Enviar</button>
            <button class="button " type="button" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
                Não autorizo, sair
            </button>
        </form>
        <form id="logout-form" action="{{ route('logout') }}" method="POST"
              style="display: none;">
            @csrf
        </form>
    </div>
@endsection
