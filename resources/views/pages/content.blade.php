@if($pagina->template)
  @include($pagina->template->view, ['options' => $pagina->options])
@else
  <div class="page-content">
    {!! $pagina->conteudo !!}
  </div>
@endif
