@php
  $groups = Sautor\Core\Models\Grupo::where('grupo_pai', null)->where('visibilidade', 'visivel')->orderBy('ordem')->orderBy('nome')->get();
@endphp
@if(@$options['list'])
  <x-groups.large-list :groups="$groups" />
@else
  <x-groups.grid :groups="$groups" />
@endif
