@extends('layouts.app')

@php
  $site_group = isset($grupo) ? $grupo->site_group : null;
@endphp

@section('content')
  @if($pagina->imagem)
    <div class="hero @if($site_group) hero--group @endif ">
      <div class="hero__image" style="background-image: url('{{ $pagina->imagem }}')">
      </div>
    </div>
  @endif
  <div class="page-show__container">
    <h1 class="page-show__title">{{ $pagina->titulo }}</h1>
    @include('pages.content')
  </div>
@endsection
