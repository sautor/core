<div>

    <table class="w-full border-collapse mb-4 bg-white rounded-lg shadow overflow-hidden">
        <thead class="bg-gray-200 text-gray-700">
        <tr>
            <th class="uppercase font-bold text-sm py-3 px-4 text-left">Domínio</th>
            <th class="uppercase font-bold text-sm py-3 px-4 text-left">Grupo</th>
            <th class="uppercase font-bold text-sm py-3 px-4 text-left">Confirmado</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="border-t py-3 px-4 font-mono font-bold">{{ config('app.domain') }}</td>
            <td class="border-t py-3 px-4 text-gray-500 italic">(Domínio principal)</td>
            <td class="border-t py-3 px-4 text-sm text-gray-500">N/A</td>
        </tr>
        @foreach($grupos->whereNotNull('dominio')->sortBy('dominio') as $grupo)
            <tr>
                <td class="border-t py-3 px-4 font-mono font-bold">{{ $grupo->dominio }}</td>
                <td class="border-t py-3 px-4">{{ $grupo->nome }}</td>
                <td class="border-t py-3 px-4">
                    @if($grupo->dominio_confirmado)
                        <span class="fas fa-check-circle fa-fw text-green-500"></span>
                    @else
                        <span class="fas fa-times-circle fa-fw text-red-500"></span>
                    @endif
                </td>
                <td class="border-t py-2 px-4 text-right">
                    @unless($grupo->dominio_confirmado)
                        <button class="button button--icon button--outline button--small" type="button" wire:click="confirm({{$grupo->id}})">
                            <span class="far fa-check-circle fa-fw"></span>
                        </button>
                    @endunless
                    <button class="button button--icon button--danger-outline button--small" type="button" wire:click="delete({{$grupo->id}})" wire:confirm="Tem a certeza que quer remover este domínio?">
                        <span class="far fa-trash fa-fw"></span>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @php($scope = 'add-domain')
    <form class="md:flex gap-4 mt-8 items-start" wire:submit="submit">
        <div class="field flex-1">
            <label for="old_url" class="label">Domínio</label>
            <div class="control">
                <input type="text" class="input" name="domain" id="domain" wire:model="domain" />
            </div>
            @error('domain') <span class="field-help field-help--invalid">{{ $message }}</span> @enderror
        </div>
        <div class="field flex-1">
            <label for="new_url" class="label">Grupo</label>
            <div class="control">
                <select class="input" name="grupo_id" id="grupo_id" wire:model="grupo_id">
                    <option disabled>Escolher um grupo</option>
                    @foreach($grupos->whereNull('dominio')->whereNull('grupo_pai')->pluck('nome', 'id') as $id => $label)
                        <option value="{{ $id }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            @error('grupo_id') <span class="field__help field__help--invalid">{{ $message }}</span> @enderror
        </div>
        <button type="submit" class="button button--primary mt-7">Registar</button>
    </form>
</div>
