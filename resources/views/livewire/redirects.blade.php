<div>
    @if($redirects->isNotEmpty())
        <table class="w-full border-collapse mb-4 bg-white rounded-lg shadow overflow-hidden">
            <thead class="bg-gray-200 text-gray-700">
            <tr>
                <th class="uppercase font-bold text-sm py-3 px-2 text-left">URL Antigo</th>
                <th class="uppercase font-bold text-sm py-3 px-2 text-left">URL Novo</th>
                <th class="uppercase font-bold text-sm py-3 px-2 text-left">Código</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($redirects as $redirect)
                <tr>
                    <td class="border-t py-3 px-2">{{ $redirect->old_url }}</td>
                    <td class="border-t py-3 px-2">{{ $redirect->new_url }}</td>
                    <td class="border-t py-3 px-2">{{ $redirect->status_code }}</td>
                    <td class="border-t py-2 px-2 text-right">
                        <button class="button button--icon button--danger-outline button--small" type="button" wire:click="delete({{$redirect->id}})">
                            <span class="far fa-trash fa-fw"></span>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <section class="sautor-list--empty">
            Não existem redirecionamentos.
        </section>
    @endif

    @php($scope = 'add-redirect')
    <form class="md:flex gap-4 mt-8 items-start" wire:submit="submit">
        <div class="field flex-1">
            <label for="old_url" class="label">URL Antigo</label>
            <div class="control">
                <input type="text" class="input" name="old_url" id="old_url" wire:model="old_url" />
            </div>
            @error('old_url') <span class="field-help field-help--invalid">{{ $message }}</span> @enderror
        </div>
        <div class="field flex-1">
            <label for="new_url" class="label">URL Novo</label>
            <div class="control">
                <input type="text" class="input" name="new_url" id="new_url" wire:model="new_url" />
            </div>
            @error('new_url') <span class="field-help field-help--invalid">{{ $message }}</span> @enderror

        </div>
        <div class="field flex-1">
            <label for="new_url" class="label">Código</label>
            <div class="control">
                <select class="input" name="status_code" id="status_code" wire:model="status_code">
                    @foreach(\Sautor\Core\Models\Redirection::getStatuses() as $code => $label)
                        <option value="{{ $code }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            @error('status_code') <span class="field__help field__help--invalid">{{ $message }}</span> @enderror
        </div>
        <button type="submit" class="button button--primary mt-7">Criar</button>
    </form>
</div>
