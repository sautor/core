@extends('layouts.app', ['headerSize' => 'lg'])

@section('content')
  @include('home.hero')
  @include('home.addons')
  @include('home.news')
  @include('home.events')
  @include('home.groups')
@endsection
