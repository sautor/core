<section class="home__events">

    <h1 class="title mb-8">Horários</h1>

    <flickity :options="{cellAlign: 'left', contain: true, pageDots: false, groupCells: true}">
      @foreach($eventos as $dia)
        <div class="home__events__day">
          <h2 class="home__events__day__title">
            @if($dia['date']->isToday()) Hoje
            @elseif($dia['date']->isTomorrow()) Amanhã
            @else {{ ucfirst($dia['date']->isoFormat('dddd')) }} @endif
            <small>{{ $dia['date']->isoFormat('D MMMM') }}</small>
          </h2>
          @forelse($dia['events'] as $event)
            <article class="home__events__event">
              <h3 class="home__events__event__name">
                {{ $event['nome'] }}
                @if(!empty($event['grupo']) && $event['grupo']['nome_curto'] !== $event['nome'] )
                  <small>{{ $event['grupo']['nome_curto'] }}</small>
                @endunless
              </h3>
              <time class="home__events__event__meta">
                <span class="home__events__event__meta__icon">
                    <span class="fas fa-clock fa-fw" data-fa-transform="shrink-2"></span>
                </span>
                <span class="home__events__event__meta__value">
                  {{ $event['date']->format('H:i') }}
                </span>
              </time>
              @if($event['local'])
                <p class="home__events__event__meta">
                  <span class="home__events__event__meta__icon">
                      <span class="fas fa-map-marker-alt fa-fw" data-fa-transform="shrink-2"></span>
                  </span>
                  <span class="home__events__event__meta__value">
                  {{ $event['local'] }}
                  </span>
                </p>
              @endif
            </article>
          @empty
            <p class="home__events__day__empty">Sem eventos neste dia</p>
          @endforelse
        </div>
      @endforeach
    </flickity>
</section>
