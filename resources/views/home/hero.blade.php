@php($siteHero = Setting::get('site-hero'))
@if($hasHighlight or !empty($siteHero))
  @php($slides = empty($siteHero) ? [] : explode(';', $siteHero))
  <div class="home__hero">
    <flickity :options="{autoPlay: true, pageDots: true, prevNextButtons: false}">
      @include('home.hero-highlight')
      @foreach($slides as $slide)
        <div class="home__hero__content" style="background-image: url('{{ $slide }}')">
        </div>
      @endforeach
    </flickity>
  </div>
@endunless