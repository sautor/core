@if($grupos->isNotEmpty())
  <div class="home__groups">
    <h1 class="title mb-8 text-center">Grupos e Movimentos</h1>

    <x-groups.grid :groups="$grupos" small />
  </div>
@endif
