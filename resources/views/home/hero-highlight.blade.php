@if($hasHighlight)
    <div class="home__hero__content" style="background-image: url({{ Setting::get('highlight-bg') ?? Setting::get('site-hero') }})">
        <div class="home__hero__highlight">
            @unless(empty(Setting::get('highlight-logo')))
                <img src="{{ Setting::get('highlight-logo') }}" alt="{{ Setting::get('highlight-title') }}"
                     class="home__hero__highlight__logo">
            @else
                <h1 class="home__hero__highlight__title">
                    {{ Setting::get('highlight-title') }}
                </h1>
            @endunless
            @unless(empty(Setting::get('highlight-text')))
                <p class="home__hero__highlight__text">{{ Setting::get('highlight-text') }}</p>
            @endunless
            <a class="button button--primary home__hero__highlight__link" href="{{ Setting::get('highlight-link') }}">Saber mais</a>
        </div>
    </div>
@endif