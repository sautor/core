<div class="home__news">
  <h1 class="title mb-8">Últimas notícias</h1>

  <x-news.grid :news="$noticias" />

  <p class="text-center mt-8">
    <a href="{{ route('noticias.index') }}" class="button">
      Ver todas as notícias
    </a>
  </p>
</div>
