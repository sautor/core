@extends('layouts.app')

@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@section('content')
  <div class="news-index__container">
    <h1 class="news-index__title">
      Notícias
      @unless($isSiteGroup)
        <small class="news-index__title__group">
          {{ $grupo->nome_curto }}
        </small>
      @endunless
    </h1>


    <x-news.grid :news="$news" :grupo="$grupo" />

    @if($news->hasPages())
      <div class="mt-12">
        {{ $news->links() }}
      </div>
    @endif
  </div>
@endsection
