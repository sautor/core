@extends($grupo->showChildren ? 'layouts.app' : 'layouts.group-admin')

@section($grupo->showChildren ? 'content' : 'group-content')
    @if($grupo->showChildren)<div class="group-groups__container">@endif

    @if($grupo->showChildren)

      <h1 class="group-groups__title">
        {{ Sautor\pluralize($grupo->designacao_grupo) }}
      </h1>
  
      @manages($grupo)
      <div class="text-center -mt-6 mb-12">
        <button class="button button--small" @click.prevent="openModal('addGroup')">
          <span class="far fa-plus"></span>
          Adicionar {{$grupo->designacao_grupo}}
        </button>
      </div>
      @endmanages

    @else

      <div class="group-groups__header">
        <h1 class="group-groups__header__title">
          {{ Sautor\pluralize($grupo->designacao_grupo) }}
        </h1>
        @manages($grupo)
          <div class="group-groups__header__actions">
            <button class="button button--responsive" @click.prevent="openModal('addGroup')">
              <span class="far fa-plus"></span>
              Adicionar {{$grupo->designacao_grupo}}
            </button>
          </div>
        @endmanages
      </div>

    @endif

    <x-groups.large-grid :groups="$grupo->grupos" />

    @manages($grupo)
      <div class="md:flex gap-4">
        @if($grupo->gruposOcultos()->exists())
          <div class="flex-1 mt-6">
            <h3 class="title title--sm mb-4">{{ Sautor\pluralize($grupo->designacao_grupo) }} não visíveis</h3>
            <x-groups.list :grupos="$grupo->gruposOcultos" link />
          </div>
        @endif
          @if($grupo->gruposEliminados()->exists())
            <div class="flex-1 mt-6">
              <h3 class="title title--sm mb-4">{{ Sautor\pluralize($grupo->designacao_grupo) }} eliminados</h3>
              <x-groups.list full :grupos="$grupo->gruposEliminados" :actions="function($g) {
                          return [
                              ['icon' => 'far fa-fw fa-undo', 'label' => 'Restaurar', 'modal' => 'restoreModal'.$g->id]
                          ];
                      }" />
            </div>
            @foreach($grupo->gruposEliminados as $grp)
              <x-groups.restore-modal :group="$grp" :parent="$grupo" />
            @endforeach
          @endif
      </div>

    <x-groups.add-modal :parent="$grupo" :slugs="$slugs" />
    @endmanages
    @if($grupo->showChildren)</div>@endif
@endsection
