@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))
@php($showLogo = ($grupo->logo_horizontal ?? $grupo->logo) && !$isSiteGroup)
@php($showName = ($grupo->logo && $isSiteGroup) || !$isSiteGroup)

@if($showLogo)
    <img class="group-index__logo" src="{{ $grupo->logo_horizontal ?? $grupo->logo }}" alt="{{ $grupo->nome }}">
@endif
@if($showName)
    <div class="group-index__name @unless($isSiteGroup) group-index__name--title @endunless @if(empty($grupo->descricao)) group-index__name--no-about @endif ">
        {{ $grupo->nome }}
    </div>
@endif
@unless(empty($grupo->descricao))
    <div class="group-index__about prose lg:prose-lg">
        {!! $grupo->descricao !!}
    </div>
@endunless
