@php
    $site_group = isset($grupo) ? $grupo->site_group : null;
    $isSiteGroup = isset($grupo) && $grupo->site_group && $grupo->is($grupo->site_group);

    if(isset($grupo)) {
        $address = Setting::get('grupo-'.$grupo->id.'-contact-address');
        $times = Setting::get('grupo-'.$grupo->id.'-contact-times');
        $phone = Setting::get('grupo-'.$grupo->id.'-contact-phone');
        if (!empty($phone)) $phone = new \Propaganistas\LaravelPhone\PhoneNumber($phone, 'PT');
        $email = $grupo->email;
        $url = $grupo->url;

        $facebook = Setting::get('grupo-'.$grupo->id.'-social-facebook');
        $instagram = Setting::get('grupo-'.$grupo->id.'-social-instagram');
        $twitter = Setting::get('grupo-'.$grupo->id.'-social-twitter');
        $youtube = Setting::get('grupo-'.$grupo->id.'-social-youtube');
        $spotify = Setting::get('grupo-'.$grupo->id.'-social-spotify');
        $has_social = $facebook || $instagram || $twitter || $youtube || $spotify;
        $social = array_filter(compact('facebook', 'instagram', 'twitter', 'youtube', 'spotify'));

        $show_footer = !empty($address) || !empty($times) || !empty($phone) || !empty($email) || !empty($url) || $has_social;
    }
  @endphp

@if(@$show_footer)
    <footer class="group-footer @if($isSiteGroup) group-footer--colors @endif ">
        @unless(empty($address))
            <address class="group-footer__address">{{ $address }}</address>
        @endunless

        @unless(empty($times))
            <div class="group-footer__times">
                <h5>Horário</h5>

                <p>{{ $times }}</p>
            </div>
        @endunless

        @unless(empty($email) && empty($phone) && empty($url) && !$has_social)
            <div class="group-footer__contacts">
                @unless(empty($phone))
                    <a href="tel:{{ $phone->formatForMobileDialingInCountry('PT') }}" class="group-footer__contact">
                        {{ $phone->formatForCountry('PT') }}
                    </a>
                @endunless
                @unless(empty($email))
                    <a href="mailto:{{ $email }}" class="group-footer__contact">
                        {{ $email }}
                    </a>
                @endunless
                @unless(empty($url))
                    <a href="{{ $url }}" class="group-footer__contact">
                        {{ $url }}
                    </a>
                @endunless
                @if($has_social)
                    <div class="group-footer__social">
                        @foreach($social as $name => $link)
                            <a href="{{ $link  }}">
                                <span class="fab fa-{{ $name }}"></span>
                            </a>
                        @endforeach
                    </div>
                @endif
            </div>
        @endunless


    </footer>
@endif
