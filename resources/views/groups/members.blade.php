@php($manages = $grupo->isManagedBy(Auth::user()))
@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends($manages ? 'layouts.group-admin' : 'layouts.app')

@section($manages ? 'group-content' : 'content')
  <div class="group-members__container">

    @if($manages)
    <div class="group-members__header">
      <h1 class="group-members__header__title">
        Membros
      </h1>

      <div class="group-members__header__actions">
        <button class="button button--responsive" @click.prevent="openModal('addMember')">
          <span class="far fa-user-plus"></span>
          Inscrever
        </button>
      </div>
    </div>
    @else
      <h1 class="group-members__title">
        Membros
        @unless($isSiteGroup)
          <small class="group-members__title__group">
            {{ $grupo->nome_curto }}
          </small>
        @endunless
      </h1>
    @endif

    <x-people.list full
                   :pessoas="$grupo->inscritos"
                   :meta="function($p) { return $p->inscricao->cargo; }"
                   :badge="function($p) {
                                $output = '';
                                if($p->inscricao->responsavel) { $output .= ' <span class=\'ml-2 text-yellow-500 fas fa-badge\'></span>'; }
                    if($p->inscricao->ano_letivo) {
                    $output .= ' <span class=\'ml-2 badge\'>'.\Sautor\formatAnoLetivo($p->inscricao->ano_letivo).'</span>';
                    }
                    return $output !== '' ? $output : null;
                    }"
                  :actions="function($p) use ($grupo) {
                    return \Auth::user()->can('update', $grupo) ? [
                    ['icon' => 'far fa-fw fa-user-times', 'label' => 'Desinscrever', 'modal' => 'removeMember'.$p->id]
                    ] : [];
                  }"
                   empty-message="Este grupo não tem membros."
    />

    <modal id="addMember">
      @php($scope = 'adicionar-membro')
      <form method="POST" action="{{ $grupo->route('inscricoes.store') }}"
            data-vv-scope="{{ $scope }}" @submit.prevent="validateForm">
        @csrf
        <div class="modal__body">
          <h3 class="modal__title">Inscrever pessoa</h3>
          <pessoa-input v-validate="'required|excluded:{{ $grupo->inscritos->pluck('nic')->implode(',') }}'"
                        name="nic" data-vv-scope="{{ $scope }}" required
                        scope="{{ $scope }}" data-vv-as="Número de Identificação Civil"></pessoa-input>
          <x-forms.input name="cargo" :scope="$scope" />
          <x-forms.checkbox name="responsavel" :label="$grupo->designacao_responsavel" :scope="$scope" />
          <x-forms.radio name="anoletivo" label="Duração da inscrição" required rules="required|included:0,1"
                         :list="['1' => 'Ano letivo '.\Sautor\formatAnoLetivo(\Sautor\anoLetivo()), '0' => 'Permanente']" value="1" />
        </div>
        <div class="modal__footer">
          <button class="button button--primary">
            Inscrever
          </button>
          <button class="button" type="button" @click.prevent="closeModal('addMember')">
            Cancelar
          </button>
        </div>
      </form>
    </modal>

    @foreach($grupo->inscritos as $pessoa)
      <modal id="removeMember{{ $pessoa->id }}">
        <div class="modal__body modal__confirmation">
          <div class="modal__confirmation__icon">
            <span class="fas fa-exclamation"></span>
          </div>
          <div class="modal__confirmation__content">
            <h3 class="modal__confirmation__title">Desinscrever {{ $pessoa->nome_curto }}</h3>
            <p class="modal__confirmation__text">
              Tem a certeza que quer desinscrever <strong>{{ $pessoa->nome_exibicao }}</strong> de
              <strong>{{ $grupo->nome }}</strong>?
            </p>
          </div>
        </div>
        <form class="modal__footer" method="POST" action="{{ $grupo->route('inscricoes.destroy', $pessoa->inscricao) }}">
          @csrf
          @method('DELETE')
          <button type="submit" class="button button--danger">Desinscrever</button>
          <button type="button" class="button" @click.prevent="closeModal('removeMember{{ $pessoa->id }}')">Cancelar</button>
        </form>
      </modal>
    @endforeach

  </div>
@endsection
