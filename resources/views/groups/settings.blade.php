@extends('layouts.group-admin')

@section('group-content')
  <div class="group-settings__container">

    <div class="group-settings__header">
      <h1 class="group-settings__header__title">
        Definições
      </h1>
    </div>

    @php($scope='definicoes')
    <form method="post" @submit.prevent="validateForm" data-vv-scope="{{ $scope }}" enctype="multipart/form-data">
      @csrf

      <div class="group-settings__section">
        <header class="group-settings__section__header">
          <h3 class="group-settings__section__title">Informações do grupo</h3>
        </header>
        <div class="group-settings__section__content">
          <div class="group-settings__section__body">

            <x-forms.input name="nome" required rules="required" :scope="$scope" :value="$grupo->nome" />
            <x-forms.input name="nome_curto" label="Nome curto" :scope="$scope" :value="$grupo->nome_curto !== $grupo->nome ? $grupo->nome_curto : ''" rules="max:20" />

            <x-forms.input name="slug" label="Endereço público" required :value="$grupo->slug"
                           :rules="'required|excluded:'.$slugs->implode(',').'|regex:^[a-z0-9]+(?:-[a-z0-9]+)*$'"
                           help="O endereço público só pode ter letras não acentuadas, números e hífenes."
                           :before="url('grupos').'/'" :scope="$scope"
            />

            <x-forms.select name="visibilidade" label="Visibilidade" :scope="$scope" :value="$grupo->visibilidade" :list="['visivel' => 'Visível', 'nao_listado' => 'Não listado', 'oculto' => 'Oculto']" required />

            <div class="lg:grid grid-cols-2 gap-3">
              <x-forms.input name="designacao_grupo" label="Designação de grupo filho" :scope="$scope" :value="$grupo->designacao_grupo !== 'Grupo' ? $grupo->designacao_grupo : ''" />
              <x-forms.input name="designacao_responsavel" label="Designação de responsável" :scope="$scope" :value="$grupo->designacao_responsavel !== 'Responsável' ? $grupo->designacao_responsavel : ''" />
            </div>
          </div>
        </div>
      </div>

      <div class="group-settings__section">
        <header class="group-settings__section__header">
          <h3 class="group-settings__section__title">Permissões</h3>
        </header>
        <div class="group-settings__section__content">
          <div class="group-settings__section__body">
            <x-forms.checkbox :name="'setting[grupo-'.$grupo->id.'-view-members-details]'" label="Os membros do grupo podem ver os detalhes dos outros membros" :value="Setting::get('grupo-'.$grupo->id.'-view-members-details')" :scope="$scope" />
          </div>
        </div>
      </div>

      <div class="group-settings__section">
        <header class="group-settings__section__header">
          <h3 class="group-settings__section__title">Contactos</h3>
        </header>
        <div class="group-settings__section__content">
          <div class="group-settings__section__body">
            <x-forms.textarea :name="'setting[grupo-'.$grupo->id.'-contact-address]'" label="Morada" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-contact-address')" />
            <x-forms.textarea :name="'setting[grupo-'.$grupo->id.'-contact-times]'" label="Horário" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-contact-times')" />
            <x-forms.input name="url" label="Website" :scope="$scope" :value="$grupo->url" type="url" rules="url" />
            <x-forms.input name="email" label="Endereço de email" :scope="$scope" :value="$grupo->email" type="email" rules="email" before="<span class='far fa-at fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-contact-phone]'" label="Telefone" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-contact-phone')" type="tel" before="<span class='fas fa-phone-alt fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-social-facebook]'" label="Facebook" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-social-facebook')" type="url" before="<span class='fab fa-facebook fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-social-instagram]'" label="Instagram" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-social-instagram')" type="url" before="<span class='fab fa-instagram fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-social-twitter]'" label="Twitter" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-social-twitter')" type="url" before="<span class='fab fa-twitter fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-social-youtube]'" label="YouTube" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-social-youtube')" type="url" before="<span class='fab fa-youtube fa-fw'></span>" />
            <x-forms.input :name="'setting[grupo-'.$grupo->id.'-social-spotify]'" label="Spotify" :scope="$scope" :value="Setting::get('grupo-'.$grupo->id.'-social-spotify')" type="url" before="<span class='fab fa-spotify fa-fw'></span>" />

          </div>
        </div>
      </div>

      @php($addons = AddonService::togglablePerGroup())
      @if($addons->isNotEmpty())
      <div class="group-settings__section">
        <header class="group-settings__section__header">
          <h3 class="group-settings__section__title">Extensões</h3>
        </header>
        <div class="group-settings__section__content">
          <div class="sautor-list">
            @foreach($addons as $addon)
              <label class="sautor-list__item" for="addon[{{ $addon->key }}]">
                <input type="checkbox" class="checkbox-input -ml-1 mr-3" id="addon[{{ $addon->key }}]" name="addon[{{ $addon->key }}]" value="1" @if($addon->isEnabledFor($grupo)) checked @endif />
                <div class="sautor-list__item__logo">
                  <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                    <span class="fad fa-{{ $addon->icon }}"></span>
                  </span>
                </div>
                <div class="sautor-list__item__data">
                  <p class="sautor-list__item__name">
                    {{ $addon->label }}
                  </p>
                  @unless(empty($addon->description))
                    <p class="sautor-list__item__meta">{{ $addon->description }}</p>
                  @endunless
                </div>
              </label>
            @endforeach
          </div>
        </div>
      </div>
      @endif

      @php($addonsWithSettings = AddonService::enabledFor($grupo)->filter(function($a) { return !empty($a->getGroupSettings()); }))
      @if($addonsWithSettings->isNotEmpty())
      <div class="group-settings__section">
        <header class="group-settings__section__header">
          <h3 class="group-settings__section__title">Opções de extensões</h3>
        </header>
        <div class="group-settings__section__content group-settings__section__content--empty">
            @foreach($addonsWithSettings as $addon)
              <h6 class="font-headline mb-2 mt-3 first:mt-0 text-gray-600">{{ $addon->label }}</h6>
              <div class="group-settings__section__content group-settings__section__body">
                @foreach($addon->getGroupSettings() as $gs)
                  @php($settingName = implode('-', ['addon', $addon->key, $grupo->id, 'setting', $gs['name']]))
                  @if($gs['type'] === 'richtext')
                    <div class="field">
                      <label class="label">
                        {{ $gs['label'] }} <span class="label__optional">(opcional)</span>
                      </label>
                      <div class="control">
                        <small-content-editor name="setting[{{ $settingName }}]" value="{{ Setting::get($settingName) }}"></small-content-editor>
                      </div>
                    </div>
                  @elseif($gs['type'] === 'textarea')
                    <x-forms.textarea :name="'setting['.$settingName.']'" :label="$gs['label']" :value="Setting::get($settingName)" :scope="$scope" />
                  @elseif($gs['type'] === 'input')
                    <x-forms.input :name="'setting['.$settingName.']'" :label="$gs['label']" :value="Setting::get($settingName)" :scope="$scope" />
                  @elseif($gs['type'] === 'select')
                    <x-forms.select :name="'setting['.$settingName.']'" :label="$gs['label']" :value="Setting::get($settingName)" :scope="$scope" :list="$gs['options']" />
                  @elseif($gs['type'] === 'image')
                    <div class="field">
                      <label class="label">
                        {{ $gs['label'] }}
                        <span class="label__optional">(opcional)</span>
                      </label>
                      <div class="control">
                        <setting-file-picker name="{{ $settingName }}" path="addon-{{ $addon->key }}" accept="image/*" title="Escolher imagem…" value="{{ Setting::get($settingName) }}"></setting-file-picker>
                      </div>
                    </div>
                  @endif
                @endforeach
              </div>
            @endforeach
        </div>
      </div>
      @endif

      <div class="group-settings__actions">
        <button type="submit" class="button button--primary">Guardar</button>
        @can('delete', $grupo)
          <button type="button" class="button button--danger-light" @click.prevent="openModal('deleteModal')">
            <span class="far fa-trash mr-2"></span>
            Eliminar
          </button>
        @endcan
      </div>

    </form>

  </div>

  @can('delete', $grupo)
    <modal id="deleteModal">
      <div class="modal__body modal__confirmation">
        <div class="modal__confirmation__icon">
          <span class="fas fa-exclamation"></span>
        </div>
        <div class="modal__confirmation__content">
          <h3 class="modal__confirmation__title">Eliminar grupo</h3>
          <p class="modal__confirmation__text">
            Tem a certeza que quer eliminar o grupo <strong>{{ $grupo->nome }}</strong>?
          </p>
        </div>
      </div>
      <form class="modal__footer" method="POST" action="{{ $grupo->route('delete') }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="button button--danger">Eliminar</button>
        <button type="button" class="button" @click.prevent="closeModal('deleteModal')">Cancelar</button>
      </form>
    </modal>
  @endcan
@endsection
