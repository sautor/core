@extends('layouts.app', ['headerSize' => 'md'])

@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@section('content')
  @php($hero = Setting::get('grupo-'.$grupo->id.'-hero'))
  @if($hero)
    <div class="hero @if($grupo->site_group) hero--group @endif ">
      <div class="hero__image" style="background-image: url('{{ $hero }}')">
      </div>
    </div>
  @endif

  @include('groups.partials.presentation')

  @if(!$isSiteGroup && $grupo->isManagedBy(Auth::user()))
  <div class="container mx-auto text-center -mt-4 mb-12">
    <a href="{{ $grupo->route('membros') }}" class="button button--small button--primary">
      <span class="far fa-user-tie"></span>
      Abrir administração
    </a>
  </div>
  @endif

  @unless($noticias->isEmpty())
    <div class="group-index__news">
      <h1 class="group-index__news__title @if($isSiteGroup) group-index__news__title--lg @endif">Últimas notícias</h1>

      <x-news.grid :news="$noticias" :grupo="$grupo" />

      <p class="text-center mt-8">
        <a href="{{ $grupo->route('noticias') }}" class="button">
          Ver todas as notícias
        </a>
      </p>
    </div>
  @endunless

  @if(!$isSiteGroup && $grupo->showChildren)
    <div class="group-index__groups">
      <div class="group-index__groups__container">
        <h1 class="title mb-8 text-center">{{ Sautor\pluralize($grupo->designacao_grupo) }}</h1>

        <x-groups.grid :groups="$grupo->grupos" small />

        @manages($grupo)
        <div class="text-center mt-6">
          <a href="{{ $grupo->route('grupos') }}" class="button button--small button--primary">
            Gerir {{ strtolower(Sautor\pluralize($grupo->designacao_grupo)) }}
          </a>
        </div>
        @endmanages
      </div>
    </div>
  @endif

@endsection
