@extends('layouts.group-admin')

@section('group-content')
    <div class="group-settings__container">

        <div class="group-settings__header">
            <h1 class="group-settings__header__title">
                Personalizar
            </h1>
        </div>

        @php($scope='personalizar')
        <form method="post" @submit.prevent="validateForm" data-vv-scope="{{ $scope }}" enctype="multipart/form-data">
            @csrf

            <section class="group-settings__section">
                <header class="group-settings__section__header">
                    <h3 class="group-settings__section__title">Identidade visual</h3>
                </header>
                <div class="group-settings__section__content group-settings__section__content--empty">
                    <h6 class="font-headline mb-2 text-gray-600">Logótipo</h6>
                    <div class="group-settings__section__content group-settings__section__body">

                        <div class="field">
                            <label class="label">
                                Logótipo
                                <span class="label__optional">(opcional)</span>
                            </label>
                            <div class="control">
                                <setting-file-picker name="grupo-{{ $grupo->id }}-logo" path="logos" accept="image/*" title="Escolher logótipo…" value="{{ Setting::get('grupo-'.$grupo->id.'-logo') }}"></setting-file-picker>
                            </div>
                            <small class="field__help">
                                É recomendada uma imagem em formato PNG ou SVG, com fundo transparente, e com uma proporção próxima de 1:1 (quadrada).
                            </small>
                        </div>

                        <div class="field">
                            <label class="label">
                                Logótipo horizontal
                                <span class="label__optional">(opcional)</span>
                            </label>
                            <div class="control">
                                <setting-file-picker name="grupo-{{ $grupo->id }}-logo-horizontal" path="logos" accept="image/*" title="Escolher logótipo horizontal…" value="{{ Setting::get('grupo-'.$grupo->id.'-logo-horizontal') }}"></setting-file-picker>
                            </div>
                            <small class="field__help">
                                É recomendada uma imagem em formato PNG ou SVG, com fundo transparente, e com uma proporção horizontal. Será usada principalmente no cabeçalho do microsite e de alguns documentos. Se não existir, será usado o logótipo acima.
                            </small>
                        </div>

                        <div class="field">
                            <label class="label">
                                Ícone
                                <span class="label__optional">(opcional)</span>
                            </label>
                            <div class="control">
                                <setting-file-picker name="grupo-{{ $grupo->id }}-icon" path="logos" accept="image/*" title="Escolher ícone…" value="{{ Setting::get('grupo-'.$grupo->id.'-icon') }}"></setting-file-picker>
                            </div>
                            <small class="field__help">
                                É recomendada uma imagem em formato PNG ou SVG, com fundo transparente, e com uma proporção 1:1 (quadrada). Deve ser uma versão simplificada do logótipo e será usada em sítios com pouco espaço. Se não existir, será usado o logótipo.
                            </small>
                        </div>

                        <div class="field">
                            <label class="label">Cor</label>
                            <setting-color-picker class="mt-2" name="grupo-{{ $grupo->id }}-color" value="{{ $grupo->cor }}" {{ Setting::has('grupo-'.$grupo->id.'-color') ? 'is-setting' : '' }} />
                        </div>

                        <div class="field">
                            <label class="label">
                                Imagem do cabeçalho
                                <span class="label__optional">(opcional)</span>
                            </label>
                            <div class="control">
                                <setting-file-picker name="grupo-{{ $grupo->id }}-hero" path="imagens" accept="image/*" title="Escolher imagem do cabeçalho…" value="{{ Setting::get('grupo-'.$grupo->id.'-hero') }}"></setting-file-picker>
                            </div>
                        </div>

                    </div>

                    <h6 class="font-headline mb-2 mt-3 text-gray-600">Tipografia</h6>
                    <div class="group-settings__section__content group-settings__section__body">
                        <typography-options name="setting[grupo-{{ $grupo->id }}-typography]" value="{{ Setting::get('grupo-'.$grupo->id.'-typography') }}" />
                    </div>
                </div>
            </section>

            <section class="group-settings__section">
                <header class="group-settings__section__header">
                    <h3 class="group-settings__section__title">Opções de página pública</h3>
                </header>
                <div class="group-settings__section__content">
                    <div class="group-settings__section__body">
                        <div class="field">
                            <label class="label">Sobre</label>
                            <div class="control flex-col">
                                <small-content-editor name="descricao" value="{{ $grupo->descricao }}" simple></small-content-editor>
                            </div>
                        </div>

                        <div class="grid lg:grid-cols-2 -mb-2 gap-x-3">
                            <x-forms.checkbox :name="'setting[grupo-'.$grupo->id.'-show-leaders]'" label="Mostrar responsáveis" :value="Setting::get('grupo-'.$grupo->id.'-show-leaders')" :scope="$scope" />
                            <x-forms.checkbox :name="'setting[grupo-'.$grupo->id.'-show-children]'" label="Mostrar grupos filho" :value="Setting::get('grupo-'.$grupo->id.'-show-children')" :scope="$scope" />
                        </div>
                    </div>
                </div>
            </section>

            <section class="group-settings__section">
                <header class="group-settings__section__header">
                    <h3 class="group-settings__section__title">Microsite</h3>
                    <p class="group-settings__section__desc">
                        Esta opção transformará a página de <strong>{{ $grupo->nome_curto }}</strong> num micro site
                        para o grupo, incluindo personalização a nível de marca (logos, cores, fontes, etc.).
                    </p>
                </header>
                <div class="group-settings__section__content">
                    <div class="group-settings__section__body">
                        <x-forms.checkbox :name="'setting[grupo-'.$grupo->id.'-microsite]'" label="Ativar microsite" :value="Setting::get('grupo-'.$grupo->id.'-microsite')" :scope="$scope" />
                        <x-forms.checkbox :name="'setting[grupo-'.$grupo->id.'-short-footer]'" label="Rodapé reduzido" :value="Setting::get('grupo-'.$grupo->id.'-short-footer')" :scope="$scope" />

                        <div class="field">
                            <label class="label">Menu</label>
                            <menu-editor grupo="{{ $grupo->id }}" name="{{ 'setting[grupo-'.$grupo->id.'-menu]' }}" value="{{ old('setting[grupo-'.$grupo->id.'-menu]', Setting::get('grupo-'.$grupo->id.'-menu', '')) }}"></menu-editor>
                        </div>
                    </div>
                </div>
            </section>

            <div class="group-settings__actions">
                <button type="submit" class="button button--primary">Guardar</button>
            </div>
        </form>

    </div>
@endsection
