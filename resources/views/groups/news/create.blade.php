@extends('layouts.basic')

@section('content')

    <content-editor
            controller-url="/api/noticias"
            edit-url="/grupos/{{ $grupo->slug }}/noticias/{id}/edit"
            delete-url="/grupos/{{ $grupo->slug }}/noticias/{id}"
            base-url="{{ url('/') }}"
            :grupo='{!! $grupo->toJson() !!}'
            news
    ></content-editor>

@endsection