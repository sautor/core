@extends('layouts.group-admin')

@section('group-content')
    <div class="group-members__header">
        <h1 class="group-members__header__title">
            Notícias
        </h1>
        <div class="group-members__header__actions">
            <a href="{{ $grupo->route('noticias.create') }}" class="button button--responsive">
                <span class="far fa-plus mr-2"></span>
                Criar
            </a>
        </div>
    </div>

    <x-news.list :news="$news" :meta="function ($n) {
    $output = $n->created_at->isoFormat('LL');
    return $output;
 } " :actions="function ($n) use ($grupo) {
    return [
        ['label' => 'Editar', 'icon' => 'fas fa-pencil', 'link' => $grupo->route('noticias.edit', $n)]
    ];
 }" />

    @if(method_exists($news, 'links'))
        <div class="mt-4">
            {{ $news->links() }}
        </div>
    @endif
@endsection
