@extends('layouts.basic')

@section('content')

    <content-editor
            :value='{!! $noticia->toJson() !!}'
            controller-url="/api/noticias"
            edit-url="/admin/noticias/{id}/edit"
            delete-url="/admin/noticias/{id}"
            base-url="{{ url('/') }}"
            news style="min-height: 100%"></content-editor>

@endsection