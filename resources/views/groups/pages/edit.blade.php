@extends('layouts.basic')

@section('content')

    <content-editor
            :value='{!! $pagina->toJson() !!}'
            controller-url="/api/paginas"
            edit-url="/grupos/{{ $grupo->slug }}/paginas/{id}/edit"
            delete-url="/grupos/{{ $grupo->slug }}/paginas/{id}"
            templates-url="/api/paginas/modelos/{{ $grupo->slug }}"
            base-url="{{ url('/') }}"
            :grupo='{!! $grupo->toJson() !!}'
            has-slug parent-key="pagina_pai"
            :children='{!! $pagina->filhos()->select(['id', 'titulo', 'slug'])->get()->toJson() !!}'
    ></content-editor>

@endsection
