@extends('layouts.group-admin')

@section('group-content')
    <div class="group-members__header">
        <h1 class="group-members__header__title">
            Páginas
        </h1>
        <div class="group-members__header__actions">
            <a href="{{ $grupo->route('paginas.create') }}" class="button button--responsive">
                <span class="far fa-plus mr-2"></span>
                Criar
            </a>
        </div>
    </div>

    <x-pages.list :pages="$pages" :grupo="$grupo" full />

    @if(method_exists($pages, 'links'))
        <div class="mt-4">
            {{ $pages->links() }}
        </div>
    @endif
@endsection
