@extends('layouts.basic')

@section('content')

    <content-editor
            controller-url="/api/paginas"
            edit-url="/grupos/{{ $grupo->slug }}/paginas/{id}/edit"
            delete-url="/grupos/{{ $grupo->slug }}/paginas/{id}"
            templates-url="/api/paginas/modelos/{{ $grupo->slug }}"
            base-url="{{ url('/') }}"
            :grupo='{!! $grupo->toJson() !!}'
            has-slug
            parent-key="pagina_pai"
    ></content-editor>

@endsection
