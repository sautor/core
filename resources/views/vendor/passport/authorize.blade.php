@extends('layouts.auth')

@section('content')
    <h2 class="title title--xs mb-6">Pedido de autorização</h2>

    <!-- Introduction -->
    <p><strong>{{ $client->name }}</strong> está a pedir permissão para aceder à sua conta.</p>

    <!-- Scope List -->
    @if (count($scopes) > 0)
      <div class="mt-4">
        <p class="font-bold text-sm">Esta aplicação poderá:</p>
        <div class="prose prose-sm mb-4">
          <ul>
            @foreach ($scopes as $scope)
              <li>{{ $scope->description }}</li>
            @endforeach
          </ul>
        </div>
      </div>
    @endif

    <div class="flex gap-2 mt-4">
      <!-- Cancel Button -->
      <form class="flex-1" method="post" action="{{ url('/oauth/authorize') }}">
        @csrf
        @method('DELETE')

        <input type="hidden" name="state" value="{{ $request->state }}">
        <input type="hidden" name="client_id" value="{{ $client->id }}">
        <button class="button w-full">Cancelar</button>
      </form>

      <!-- Authorize Button -->
      <form class="flex-1" method="post" action="{{ url('/oauth/authorize') }}">
        @csrf

        <input type="hidden" name="state" value="{{ $request->state }}">
        <input type="hidden" name="client_id" value="{{ $client->id }}">
        <button type="submit" class="button button--primary w-full">Autorizar</button>
      </form>
    </div>
@endsection
