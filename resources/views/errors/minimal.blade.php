<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Styles -->
        @vite(['resources/sass/errors.scss'])

        @include('layouts.partials.custom-styles')
    </head>
    <body class="error">
        <div class="error__logo">
            @if(Setting::get('site-logo'))
                <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}">
            @else
                {{ config('app.name') }}
            @endif
        </div>
        <section class="error__content">
            <h1>@yield('code')</h1>
            <h5>@yield('message')</h5>
        </section>
        <a href="/" class="button button--small">
            Voltar ao início
        </a>
    </body>
</html>
