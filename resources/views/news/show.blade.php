@extends('layouts.app')

@php
  $site_group = isset($grupo) ? $grupo->site_group : null;
  $isSiteGroup = isset($grupo) && $grupo->site_group && $grupo->is($grupo->site_group);
@endphp

@section('content')
  @if($noticia->imagem)
    <div class="hero @if($site_group) hero--group @endif ">
      <div class="hero__image" style="background-image: url('{{ $noticia->imagem }}')">
      </div>
    </div>
  @endif
  <div class="news-show__container">
    <header class="news-show__header">
      <h1 class="news-show__title">{{ $noticia->titulo }}</h1>
      <p class="news-show__meta">
        Publicado a <strong>{{ $noticia->created_at->isoFormat('LL') }}</strong>
        por <strong>{{ $noticia->autor->nome_exibicao }}</strong>
        @if(!$isSiteGroup && $noticia->grupo)
        &middot; <a href="{{ $noticia->grupo->route('index') }}">{{ $noticia->grupo->nome }}</a>
        @endif
        @if($noticia->visibilidade === 'privado')
          &middot; <span class="fas fa-lock fa-sm"></span> Privado
        @endif
      </p>
    </header>

    <div class="news-show__content">
      {!! $noticia->conteudo !!}
    </div>
  </div>
@endsection
