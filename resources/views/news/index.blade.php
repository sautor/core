@extends('layouts.app')

@section('content')
  <div class="news-index__container">
    <h1 class="news-index__title">
      Notícias
    </h1>

    <x-news.grid :news="$news" />

    @if($news->hasPages())
      <div class="mt-12">
        {{ $news->links() }}
      </div>
    @endif
  </div>
@endsection
