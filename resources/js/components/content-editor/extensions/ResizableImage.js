import { Node } from '@tiptap/core'
import VueComponent from './ResizableImage.vue'
import { VueNodeViewRenderer } from '@tiptap/vue-2'

const ResizableImage = Node.create({
  name: 'resizableImage',
  group: 'block',
  content: 'inline*',
  draggable: true,
  addAttributes () {
    const imgElement = element => element.tagName === 'IMG' ? element : element.querySelector('img')
    return {
      src: {
        default: null,
        parseHTML: element => imgElement(element).getAttribute('src')
      },
      width: {
        default: '10em',
        parseHTML: element => imgElement(element).style.width,
        renderHTML: attributes => {
          const isFigure = !attributes.rounded && !attributes.float
          return attributes.width && !isFigure
            ? {
                style: `width: ${attributes.width}`
              }
            : {}
        }
      },
      alt: {
        default: null,
        parseHTML: element => imgElement(element).getAttribute('alt')
      },
      rounded: {
        default: false,
        parseHTML: element => imgElement(element).classList.contains('rounded-full'),
        renderHTML: attributes => attributes.rounded
          ? {
              class: 'rounded-full'
            }
          : {}
      },
      float: {
        default: null,
        parseHTML: element => imgElement(element).classList.contains('float-left') ? 'left' : element.classList.contains('float-right') ? 'right' : null,
        renderHTML: attributes => attributes.float
          ? {
              class: `float-${attributes.float}`
            }
          : {}
      }
    }
  },
  parseHTML () {
    return [
      {
        tag: 'figure',
        contentElement: 'figcaption'
      },
      {
        tag: 'img[src]'
      }
    ]
  },
  renderHTML ({ HTMLAttributes, node }) {
    if (node.content.size > 0 || Object.keys(HTMLAttributes).length) {
      return ['figure', {}, ['img', HTMLAttributes], ['figcaption', 0]]
    } else {
      return ['img', HTMLAttributes]
    }
  },
  addCommands () {
    return {
      resizableImage: (options) => ({ commands }) => commands.insertContent({
        type: this.name,
        attrs: options
      })
    }
  },
  addNodeView () {
    return VueNodeViewRenderer(VueComponent)
  }
})

export default ResizableImage
