import VeeValidate, { Validator } from 'vee-validate'
import cronParser from 'cron-parser'

import validatePt from 'vee-validate/dist/locale/pt_PT'

export default (Vue) => {
  /**
   * Load Vee-Validate, including localizations and custom rules
   */

  Vue.use(VeeValidate, {
    classes: true,
    classNames: {
      valid: null,
      invalid: 'is-invalid'
    }
  })

  Validator.localize('pt_PT', validatePt)

  const uniquePessoaRule = (value, args) => {
    return new Promise((resolve, reject) => {
      const data = { value: value }
      if (args[1]) data.id = args[1]
      Vue.prototype.$http.post(`/api/pessoas/unique/${args[0]}`, data)
        .then(() => {
          resolve(true)
        })
        .catch(err => {
          if (err.response && err.response.status === 409) {
            resolve(false)
          } else {
            reject(err)
          }
        })
    })
  }
  Validator.extend('unique_pes', uniquePessoaRule)

  const nicExistsRule = (value, args) => {
    return new Promise((resolve, reject) => {
      Vue.prototype.$http.get(`/api/pessoas/nic/${value}`)
        .then(() => {
          resolve(true)
        })
        .catch(err => {
          if (err.response && err.response.status === 404) {
            resolve(false)
          } else {
            reject(err)
          }
        })
    })
  }
  Validator.extend('pes_exists', nicExistsRule)

  const cronValidator = (value, args) => {
    try {
      cronParser.parseExpression(value)
      return true
    } catch (e) {
      return false
    }
  }
  Validator.extend('cron', cronValidator)

  Validator.localize({
    pt_PT: {
      messages: {
        unique_pes: (name) => `O valor no campo ${name} já está em uso.`,
        pes_exists: (name) => 'Não existe nenhuma pessoa com este NIC.',
        cron: (name) => `O campo ${name} não contém uma expressão cron válida.`
      }
    }
  })
}
