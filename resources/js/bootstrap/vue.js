import veeValidate from './vee-validate'
import VueCroppie from 'vue-croppie'
import vClickOutside from 'v-click-outside'
import axios from 'axios'
import moment from 'moment'
import 'moment/locale/pt'

import Dropdown from '../components/Dropdown.vue'
import Modal from '../components/Modal.vue'

import PhotoPicker from '../components/PhotoPicker.vue'
import GrupoPicker from '../components/GrupoPicker.vue'
import PessoaPicker from '../components/PessoaPicker.vue'
import PessoaInput from '../components/PessoaInput.vue'
import EventsList from '../components/events/EventsList.vue'
import Flickity from '../components/Flickity.vue'
import MenuInput from '../components/MenuInput.vue'
import MenuEditor from '../components/menu-editor/MenuEditor.vue'
import TypographyOptions from '../components/TypographyOptions.vue'
import FileManager from '../components/FileManager.vue'
import SettingFilePicker from '../components/SettingFilePicker.vue'

import SearchOverlay from '../components/search/SearchOverlay.vue'

import ContentEditor from '../components/content-editor/ContentEditor.vue'
import SmallContentEditor from '../components/content-editor/SmallContentEditor.vue'

import PassportClients from '../components/passport/Clients.vue'
import PassportAuthorizedClients from '../components/passport/AuthorizedClients.vue'
import PassportPersonalAccessTokens from '../components/passport/PersonalAccessTokens.vue'

import CookieBanner from '../components/CookieBanner.vue'

import TwoFactor from '../components/TwoFactor.vue'

import SideTabs from '../components/tabs/SideTabs.vue'
import Tab from '../components/tabs/Tab.vue'

import ColorPicker from '../components/ColorPicker.vue'
import SettingColorPicker from '../components/SettingColorPicker.vue'

import modals from '../mixins/modals'

export default (Vue) => {
  // Set up axios
  axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
  const token = document.head.querySelector('meta[name="csrf-token"]')
  if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
  } else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token')
  }
  Vue.prototype.$http = axios

  veeValidate(Vue)

  // Set up moment
  moment.locale('pt')
  Vue.prototype.$moment = moment

  // Other Vue plugins
  Vue.use(VueCroppie)
  Vue.use(vClickOutside)

  Vue.component('Modal', Modal)
  Vue.component('MenuEditor', MenuEditor)

  window.Vue = Vue

  // eslint-disable-next-line
  const app = new Vue({
    el: '#app',
    mixins: [modals],
    components: {
      Dropdown,
      PhotoPicker,
      GrupoPicker,
      PessoaPicker,
      PessoaInput,
      EventsList,
      Flickity,
      TypographyOptions,
      SearchOverlay,
      FileManager,
      SettingFilePicker,
      MenuInput,
      ContentEditor,
      SmallContentEditor,
      CookieBanner,
      TwoFactor,
      SideTabs,
      Tab,
      ColorPicker,
      SettingColorPicker,
      // Passport components
      PassportClients,
      PassportAuthorizedClients,
      PassportPersonalAccessTokens
    },
    data () {
      return {
        isMenuOpen: false,
        isSearchOpen: false
      }
    },
    methods: {
      // Common methods
      validateForm ($event) {
        const form = $event.target
        this.$validator.validateAll(form.getAttribute('data-vv-scope')).then(result => {
          if (!result) return
          form.submit()
        })
      },
      windowWidth () {
        return window.innerWidth
      },
      toggleMenu () {
        this.isMenuOpen = !this.isMenuOpen
      },
      toggleSearch () {
        this.isSearchOpen = !this.isSearchOpen
      }
    }
  })
}
