/**
 * Loading Sentry to handle JS errors
 */

import * as Sentry from '@sentry/vue'

export default (Vue) => {
  if (process.env.NODE_ENV !== 'development' && window.sautor.environment !== 'local') {
    Sentry.init({
      Vue,
      dsn: window.sautor.dsn,
      environment: window.sautor.environment
    })

    Sentry.configureScope((scope) => {
      if (window.sautor.user !== null) scope.setUser(window.sautor.user)
    })
  }
}
