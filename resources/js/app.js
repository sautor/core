import Vue from 'vue'
import loadSentry from './bootstrap/sentry'
import initVue from './bootstrap/vue'

loadSentry(Vue)

initVue(Vue)
