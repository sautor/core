const pad = (num, size) => {
  let s = String(num)
  while (s.length < (size || 2)) { s = '0' + s }
  return s
}

export default {
  methods: {
    cronToMoment (cron, year) {
      if (!this.isCronUnique(cron, year)) return null
      const parts = cron.split(' ')
      return this.$moment(`${year}-${pad(parts[3], 2)}-${pad(parts[2], 2)}T${pad(parts[1], 2)}:${pad(parts[0], 2)}`)
    },
    isCronUnique (cron, year) {
      if (!year || year === '') return false
      const parts = cron.split(' ')
      return !cron.includes(',') && parts.length === 5 && year && year !== '' && parts[0] !== '*' && parts[1] !== '*' && parts[2] !== '*' &&
        parts[3] !== '*'
    }
  }
}
