import cronstrue from 'cronstrue'
import 'cronstrue/locales/pt_PT'
import cronHelpers from './cronHelpers'

export default {
  mixins: [cronHelpers],
  methods: {
    mf (dateTime, format = 'LLL') {
      return this.$moment(dateTime).format(format)
    },
    cronf (cron, year = '', format = 'LLL') {
      const moment = this.cronToMoment(cron, year)
      if (moment) return moment.format(format)
      try {
        return cronstrue.toString(cron, { locale: 'pt_PT' }) + (year && year !== '' ? `, apenas em ${year}` : '')
      } catch (e) {
        return null
      }
    }
  }
}
