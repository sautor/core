export default {
  data () {
    return {
      openModals: []
    }
  },
  methods: {
    isModalOpen (modalId) {
      return this.openModals.includes(modalId)
    },
    openModal (modalId) {
      if (!this.isModalOpen(modalId)) this.openModals.push(modalId)
    },
    closeModal (modalId) {
      if (this.isModalOpen(modalId)) this.openModals.splice(this.openModals.indexOf(modalId), 1)
    }
  }
}
