import colors from 'tailwindcss/colors'
import defaultTheme from 'tailwindcss/defaultTheme'

function withOpacityValue (variable) {
  return ({ opacityValue }) => {
    if (opacityValue === undefined) {
      return `rgb(var(${variable}))`
    }
    return `rgb(var(${variable}) / ${opacityValue})`
  }
}

export default {
  content: [
    './app/Filament/**/*.php',
    './vendor/filament/**/*.blade.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue'
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          50: withOpacityValue('--primary-50'),
          100: withOpacityValue('--primary-100'),
          200: withOpacityValue('--primary-200'),
          300: withOpacityValue('--primary-300'),
          400: withOpacityValue('--primary-400'),
          500: withOpacityValue('--primary-500'),
          600: withOpacityValue('--primary-600'),
          700: withOpacityValue('--primary-700'),
          800: withOpacityValue('--primary-800'),
          900: withOpacityValue('--primary-900')
        },
        gray: colors.zinc,
        blue: colors.sky,
        green: colors.emerald,
        yellow: colors.amber
      },
      fontFamily: {
        sans: `var(--sans-family, ${defaultTheme.fontFamily.sans})`,
        serif: `var(--serif-family, ${defaultTheme.fontFamily.serif})`,
        headline: `var(--headline-family, var(--sans-family, ${defaultTheme.fontFamily.sans}))`
      },
      fontWeight: {
        headline: `var(--headline-weight, ${defaultTheme.fontWeight.bold})`,
        accent: `var(--accent-weight, ${defaultTheme.fontWeight.medium})`
      },
      borderWidth: {
        DEFAULT: 'var(--border-width, 1px)'
      },
      cursor: {
        grab: 'grab'
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            h1: {
              fontFamily: theme('fontFamily.headline'),
              fontWeight: theme('fontWeight.headline')
            },
            h2: {
              fontFamily: theme('fontFamily.headline'),
              fontWeight: theme('fontWeight.headline')
            },
            h3: {
              fontFamily: theme('fontFamily.headline'),
              fontWeight: theme('fontWeight.headline')
            },
            h4: {
              fontFamily: theme('fontFamily.headline'),
              fontWeight: theme('fontWeight.headline')
            }
          }
        }
      })
    }
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled']
    }
  },
  corePlugins: {
    preflight: false
  }
}
