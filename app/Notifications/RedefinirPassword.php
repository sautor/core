<?php

namespace Sautor\Core\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Sautor\Core\Models\Pessoa;

class RedefinirPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->line('Está a receber este e-mail porque recebemos um pedido para redefinir a password para a sua conta.')
            ->action('Redefinir Password', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Se não fez um pedido de redefinição de password não precisa fazer qualquer ação.');

        if ($notifiable instanceof Pessoa) {
            $name = explode(' ', $notifiable->nome)[0];
            $message->greeting('Olá '.$name.'!');
        }

        return $message;
    }
}
