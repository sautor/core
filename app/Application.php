<?php

namespace Sautor\Core;

use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Mix;
use Illuminate\Foundation\PackageManifest;

class Application extends \Illuminate\Foundation\Application
{
    protected $mainAppPath;

    /**
     * Application constructor.
     */
    public function __construct($mainAppPath, $basePath = null)
    {
        $this->mainAppPath = $mainAppPath;
        parent::__construct($basePath);
    }

    public function storagePath($path = '')
    {
        return ($this->storagePath ?: $this->mainAppPath.DIRECTORY_SEPARATOR.'storage').($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }

    public function environmentPath()
    {
        return $this->environmentPath ?: $this->mainAppPath;
    }

    public function resourcePath($path = '')
    {
        return $this->mainAppPath.DIRECTORY_SEPARATOR.'resources'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    public function langPath($path = '')
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'lang'.($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }

    public function publicPath($path = '')
    {
        return $this->mainAppPath.DIRECTORY_SEPARATOR.'public'.($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }

    public function bootstrapPath($path = '')
    {
        return $this->mainAppPath.DIRECTORY_SEPARATOR.'bootstrap'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Register the basic bindings into the container.
     *
     * @return void
     */
    protected function registerBaseBindings()
    {
        static::setInstance($this);

        $this->instance('app', $this);

        $this->instance(Container::class, $this);
        $this->singleton(Mix::class);

        $this->singleton(PackageManifest::class, function () {
            return new PackageManifest(
                new Filesystem, $this->mainAppPath, $this->getCachedPackagesPath()
            );
        });
    }
}
