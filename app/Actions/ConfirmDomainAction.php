<?php

namespace Sautor\Core\Actions;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Sautor\Core\Models\Grupo;

class ConfirmDomainAction
{
    protected Grupo $grupo;

    public function __construct(Grupo $grupo)
    {
        $this->grupo = $grupo;
    }

    public function execute()
    {
        if (! $this->grupo->dominio) {
            return null;
        }

        $endpoint = route('api.domain-check', $this->grupo);

        $check_response = Http::get($endpoint);

        $matches = $check_response->successful() && Hash::check($this->grupo->id.$this->grupo->dominio, $check_response->body());

        $this->grupo->dominio_confirmado = $matches;
        $this->grupo->save();

        return $matches;
    }
}
