<?php

namespace Sautor\Core\Support;

use Spatie\MediaLibrary\Support\UrlGenerator\DefaultUrlGenerator;

class MediaUrlGenerator extends DefaultUrlGenerator
{
    public function getUrl(): string
    {
        return route('media', $this->media->uuid);
    }
}
