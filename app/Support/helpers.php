<?php

namespace Sautor;

use Carbon\Carbon;
use ICanBoogie\Inflector;
use Illuminate\Support\Facades\Route;

function anoLetivo(?Carbon $data = null)
{
    if (is_null($data)) {
        $data = Carbon::now();
    }
    if ($data->month < 9) {
        return $data->year - 1;
    }

    return $data->year;
}

function formatAnoLetivo($anoLetivo)
{
    return $anoLetivo.'/'.($anoLetivo + 1);
}

function pluralize($word)
{
    $inflector = Inflector::get('pt');

    return $inflector->pluralize($word);
}

function hexToHsl($hex)
{
    $hex = [$hex[1].$hex[2], $hex[3].$hex[4], $hex[5].$hex[6]];
    $rgb = array_map(function ($part) {
        return hexdec($part) / 255;
    }, $hex);

    $max = max($rgb);
    $min = min($rgb);

    $l = ($max + $min) / 2;

    if ($max == $min) {
        $h = $s = 0;
    } else {
        $diff = $max - $min;
        $s = $l > 0.5 ? $diff / (2 - $max - $min) : $diff / ($max + $min);

        switch ($max) {
            case $rgb[0]:
                $h = ($rgb[1] - $rgb[2]) / $diff + ($rgb[1] < $rgb[2] ? 6 : 0);
                break;
            case $rgb[1]:
                $h = ($rgb[2] - $rgb[0]) / $diff + 2;
                break;
            case $rgb[2]:
                $h = ($rgb[0] - $rgb[1]) / $diff + 4;
                break;
        }

        $h /= 6;
    }

    return [$h, $s, $l];
}

function hslToHex($hsl)
{
    [$h, $s, $l] = $hsl;

    if ($s == 0) {
        $r = $g = $b = 1;
    } else {
        $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
        $p = 2 * $l - $q;

        $r = hue2rgb($p, $q, $h + 1 / 3);
        $g = hue2rgb($p, $q, $h);
        $b = hue2rgb($p, $q, $h - 1 / 3);
    }

    return '#'.rgb2hex($r).rgb2hex($g).rgb2hex($b);
}

function hslToRgbTriplet($hsl)
{
    [$h, $s, $l] = $hsl;

    if ($s == 0) {
        $r = $g = $b = 1;
    } else {
        $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
        $p = 2 * $l - $q;

        $r = hue2rgb($p, $q, $h + 1 / 3);
        $g = hue2rgb($p, $q, $h);
        $b = hue2rgb($p, $q, $h - 1 / 3);
    }

    return round($r * 255).' '.round($g * 255).' '.round($b * 255);
}

function hue2rgb($p, $q, $t)
{
    if ($t < 0) {
        $t += 1;
    }
    if ($t > 1) {
        $t -= 1;
    }
    if ($t < 1 / 6) {
        return $p + ($q - $p) * 6 * $t;
    }
    if ($t < 1 / 2) {
        return $q;
    }
    if ($t < 2 / 3) {
        return $p + ($q - $p) * (2 / 3 - $t) * 6;
    }

    return $p;
}

function rgb2hex($rgb)
{
    return str_pad(dechex($rgb * 255), 2, '0', STR_PAD_LEFT);
}

function generate_palette($hex_color)
{
    $hsl = hexToHsl($hex_color);

    $max_light = min(0.95, $hsl[2] + 0.35);
    $max_dark = $hsl[2] < 0.35 ? 0.1 : 0.2;

    $light_step = ($max_light - $hsl[2]) / 5;
    $dark_step = ($hsl[2] - $max_dark) / 4;

    $palette = [
        hslToRgbTriplet([$hsl[0], $hsl[1], $max_light]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] + 4 * $light_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] + 3 * $light_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] + 2 * $light_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] + 1 * $light_step]),
        hslToRgbTriplet($hsl),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] - 1 * $dark_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] - 2 * $dark_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $hsl[2] - 3 * $dark_step]),
        hslToRgbTriplet([$hsl[0], $hsl[1], $max_dark]),
    ];

    return $palette;
}

function groupRoute(callable $routes)
{
    Route::domain(config('app.domain'))->prefix('grupos/{grupo}')->middleware(['web', 'maintenance'])->name('grupo.')->group($routes);
    Route::domain('{grupo:dominio}')->where(['grupo' => '[a-z0-9.]+'])->name('grupo-dominio.')->middleware(['web', 'maintenance'])->group($routes);
}

function groupApiRoute (callable $routes) {
    Route::domain(config('app.domain'))->prefix('api/grupos/{grupo}')->middleware(['api', 'auth:api'])->name('grupo.api.')->group($routes);
    Route::domain('{grupo:dominio}')->prefix('api')->where(['grupo' => '[a-z0-9.]+'])->name('grupo-dominio.api.')->middleware(['api','auth:api'])->group($routes);
}
