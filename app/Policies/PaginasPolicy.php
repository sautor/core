<?php

namespace Sautor\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Pagina;
use Sautor\Core\Models\Pessoa;

class PaginasPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability)
    {
        if ($user->can('gerir páginas')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the page.
     *
     * @return mixed
     */
    public function view(?Pessoa $user, Pagina $pagina)
    {
        if ($pagina->grupo) {
            if ($pagina->publico) {
                return true;
            }

            return $user && $user->can('details', $pagina->grupo);
        }

        return true;
    }

    /**
     * Determine whether the user can create pages.
     *
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists();
    }

    /**
     * Determine whether the user can update the page.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Pagina $pagina)
    {
        if ($pagina->grupo) {
            return $pagina->grupo->isManagedBy($user);
        }

        return false;
    }

    /**
     * Determine whether the user can delete the page.
     *
     * @return mixed
     */
    public function delete(Pessoa $user, Pagina $pagina)
    {
        if ($pagina->grupo) {
            return $pagina->grupo->isManagedBy($user);
        }

        return false;
    }
}
