<?php

namespace Sautor\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Evento;
use Sautor\Core\Models\Pessoa;

class EventosPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability)
    {
        if ($user->can('gerir eventos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the evento.
     *
     * @return mixed
     */
    public function view(Pessoa $user, Evento $evento)
    {
        return true;
    }

    /**
     * Determine whether the user can create eventos.
     *
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the evento.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Evento $evento)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the evento.
     *
     * @return mixed
     */
    public function delete(Pessoa $user, Evento $evento)
    {
        return false;
    }
}
