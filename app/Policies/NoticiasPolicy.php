<?php

namespace Sautor\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Noticia;
use Sautor\Core\Models\Pessoa;

class NoticiasPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability)
    {
        if ($user->can('gerir notícias')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the noticia.
     *
     * @return mixed
     */
    public function view(?Pessoa $user, Noticia $noticia)
    {
        if ($noticia->grupo) {
            if ($noticia->visibilidade !== 'privado') {
                return true;
            }

            return $user && $user->can('details', $noticia->grupo);
        }

        return true;
    }

    /**
     * Determine whether the user can create noticias.
     *
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists();
    }

    /**
     * Determine whether the user can update the noticia.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Noticia $noticia)
    {
        if ($noticia->grupo) {
            return $noticia->grupo->isManagedBy($user);
        }

        return false;
    }

    /**
     * Determine whether the user can delete the noticia.
     *
     * @return mixed
     */
    public function delete(Pessoa $user, Noticia $noticia)
    {
        if ($noticia->grupo) {
            return $noticia->grupo->isManagedBy($user);
        }

        return false;
    }
}
