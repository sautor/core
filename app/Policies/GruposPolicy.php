<?php

namespace Sautor\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pessoa;

class GruposPolicy
{
    use HandlesAuthorization;

    public function before(?Pessoa $user, $ability)
    {
        if ($user and $user->can('gerir grupos')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the grupo.
     *
     * @return mixed
     */
    public function view(?Pessoa $user, Grupo $grupo)
    {
        return $grupo->visibilidade !== 'oculto' or ($user and $this->details($user, $grupo));
    }

    /**
     * Determine whether the user can view the grupo private details.
     *
     * @return mixed
     */
    public function details(Pessoa $user, Grupo $grupo)
    {
        return $grupo->isManagedBy($user) or $user->isInGrupo($grupo);
    }

    /**
     * Determine whether the user can create grupos.
     *
     * @return mixed
     */
    public function create(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists();
    }

    /**
     * Determine whether the user can update the grupo.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Grupo $grupo)
    {
        return $grupo->isManagedBy($user);
    }

    /**
     * Determine whether the user can delete the grupo.
     *
     * @return mixed
     */
    public function delete(Pessoa $user, Grupo $grupo)
    {
        //
    }
}
