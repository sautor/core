<?php

namespace Sautor\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Pessoa;

class PessoasPolicy
{
    use HandlesAuthorization;

    public function before(Pessoa $user, $ability)
    {
        if ($user->can('gerir pessoas') && $ability !== 'destroy') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the pessoa.
     *
     * @return mixed
     */
    public function view(Pessoa $user, Pessoa $pessoa)
    {
        if ($user->can('ver pessoas')) {
            return true;
        }

        if ($this->update($user, $pessoa)) {
            return true;
        }

        foreach ($pessoa->grupos as $grp) {
            if ($user->isInGrupo($grp) && \Setting::get('grupo-'.$grp->id.'-view-members-details')) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can update the pessoa.
     *
     * @return mixed
     */
    public function update(Pessoa $user, Pessoa $pessoa)
    {
        if ($user->is($pessoa)) {
            return true;
        }

        // Allow if manager of any of their groups
        foreach ($pessoa->grupos as $grupo) {
            if ($grupo->isManagedBy($user)) {
                return true;
            }
        }

        // Allow if ancestor or manager of a group ancestor belongs to
        foreach ($pessoa->ascendentes as $asc) {
            if ($asc->is($user)) {
                return true;
            }
            foreach ($asc->grupos as $grp) {
                if ($grp->isManagedBy($user)) {
                    return true;
                }
            }
        }

        // Allow in same conditions for descendants
        foreach ($pessoa->descendentes as $desc) {
            // if($desc->is($user)) return true;
            foreach ($desc->grupos as $grp) {
                if ($grp->isManagedBy($user)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function store(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists() || $user->can('gerir grupos');
    }

    public function destroy(Pessoa $user, Pessoa $pessoa)
    {
        return $user->can('gerir pessoas') && ! $user->is($pessoa);
    }
}
