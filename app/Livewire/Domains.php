<?php

namespace Sautor\Core\Livewire;

use Filament\Notifications\Notification;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Sautor\Core\Actions\ConfirmDomainAction;
use Sautor\Core\Models\Grupo;

class Domains extends Component
{
    public $domain = '';

    public $grupo_id = null;

    public function rules()
    {
        return [
            'domain' => 'required|unique:grupos,dominio',
            'grupo_id' => [
                'required',
                Rule::exists('grupos', 'id')->where(fn (Builder $query) => $query->whereNull('dominio')),
            ],
        ];
    }

    public function submit()
    {
        $this->authorize('gerir domínios');

        $this->validate();

        $grupo = Grupo::find($this->grupo_id);
        $grupo->dominio = $this->domain;
        $grupo->dominio_confirmado = null;
        $grupo->save();

        // TODO: Confirm

        Notification::make()
            ->title('Domínio adicionado com sucesso.')
            ->success()
            ->send();

        $this->domain = '';
        $this->grupo_id = null;
    }

    public function confirm($grupo_id)
    {
        $this->authorize('gerir domínios');

        $grupo = Grupo::find($grupo_id);
        if (! $grupo) {
            return;
        }

        $confirmed = (new ConfirmDomainAction($grupo))->execute();

        if ($confirmed) {
            Notification::make()
                ->title('Domínio confirmado com sucesso.')
                ->success()
                ->send();
        } else {
            Notification::make()
                ->title('Não foi possível confirmar o domínio.')
                ->danger()
                ->send();
        }
    }

    public function delete($grupo_id)
    {
        $this->authorize('gerir domínios');

        $grupo = Grupo::find($grupo_id);
        if (! $grupo) {
            return;
        }

        $grupo->dominio = null;
        $grupo->dominio_confirmado = null;
        $grupo->save();

        Notification::make()
            ->title('Domínio eliminado com sucesso.')
            ->success()
            ->send();
    }

    public function render()
    {
        return view('livewire.domains', ['grupos' => Grupo::all()]);
    }
}
