<?php

namespace Sautor\Core\Livewire;

use Filament\Notifications\Notification;
use Livewire\Component;
use SiroDiaz\Redirection\Models\Redirection;

class Redirects extends Component
{
    public $old_url = '';

    public $new_url = '';

    public $status_code = '302';

    protected $rules = [
        'old_url' => 'required',
        'new_url' => 'required',
        'status_code' => 'required',
    ];

    public function delete($redirect_id)
    {
        Redirection::where('id', $redirect_id)->delete();
    }

    public function submit()
    {
        $this->validate();

        Redirection::create([
            'old_url' => $this->old_url,
            'new_url' => $this->new_url,
            'status_code' => $this->status_code,
        ]);

        Notification::make()
            ->title('Redirecionamento criado com sucesso.')
            ->success()
            ->send();

        $this->old_url = '';
        $this->new_url = '';
        $this->status_code = '302';
    }

    public function render()
    {
        return view('livewire.redirects', [
            'redirects' => Redirection::all(),
        ]);
    }
}
