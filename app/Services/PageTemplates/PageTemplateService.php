<?php

namespace Sautor\Core\Services\PageTemplates;

use Illuminate\Support\Collection;

class PageTemplateService
{
    private Collection $templates;

    /**
     * AddonService constructor.
     */
    public function __construct()
    {
        $this->templates = new Collection;
    }

    public function all(): Collection
    {
        return $this->templates->sortBy(function ($addon) {
            return strtolower($addon->label);
        });
    }

    public function get(string $key): ?PageTemplate
    {
        return $this->templates->firstWhere('key', $key);
    }

    public function forMain(): Collection
    {
        return $this->templates->filter(function ($template) {
            return $template->type === PageTemplate::ALL || $template->type === PageTemplate::MAIN;
        });
    }

    public function forGroup(): Collection
    {
        return $this->templates->filter(function ($template) {
            return $template->type === PageTemplate::ALL || $template->type === PageTemplate::GROUP;
        });
    }

    public function register(PageTemplate $template)
    {
        $this->templates->push($template);
    }
}
