<?php

namespace Sautor\Core\Services\PageTemplates;

use Illuminate\Support\Facades\Facade;

class PageTemplateServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'PageTemplateService';
    }
}
