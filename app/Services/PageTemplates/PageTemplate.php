<?php

namespace Sautor\Core\Services\PageTemplates;

class PageTemplate
{
    const ALL = 'ALL';

    const MAIN = 'MAIN';

    const GROUP = 'GROUP';

    public string $key;

    public string $label;

    public string $type;

    public string $view;

    public ?string $description;

    private bool $noContent = false;

    private array $options = [];

    /**
     * PageTemplate constructor.
     */
    private function __construct(string $key, string $label, string $type, string $view, ?string $description)
    {
        $this->key = $key;
        $this->label = $label;
        $this->type = $type;
        $this->view = $view;
        $this->description = $description;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): PageTemplate
    {
        $this->options = $options;

        return $this;
    }

    public function isNoContent(): bool
    {
        return $this->noContent;
    }

    public function setNoContent(bool $noContent): PageTemplate
    {
        $this->noContent = $noContent;

        return $this;
    }

    public static function create(string $key, string $label, string $type, string $view, ?string $description)
    {
        return new PageTemplate($key, $label, $type, $view, $description);
    }

    public function toArray()
    {
        return [
            'key' => $this->key,
            'label' => $this->label,
            'description' => $this->description,
            'noContent' => $this->noContent,
            'options' => $this->options,
        ];
    }
}
