<?php

namespace Sautor\Core\Services\Addons;

use Illuminate\Support\Collection;
use Sautor\Core\Models\Grupo;

class AddonService
{
    private Collection $registeredAddons;

    /**
     * AddonService constructor.
     */
    public function __construct()
    {
        $this->registeredAddons = new Collection;
    }

    public function all(): Collection
    {
        return $this->registeredAddons->sortBy(function ($addon) {
            return strtolower($addon->label);
        });
    }

    public function get(string $key): ?Addon
    {
        return $this->registeredAddons->firstWhere('key', $key);
    }

    public function enabled(): Collection
    {
        return $this->all()->filter(function (Addon $addon) {
            return $addon->isEnabled();
        });
    }

    public function enabledGlobal(): Collection
    {
        return $this->enabled()->filter(function (Addon $addon) {
            return ! $addon->isGroupAddon();
        });
    }

    public function enabledFor(Grupo $grupo): Collection
    {
        return $this->all()->filter(function (Addon $addon) use ($grupo) {
            return $addon->isEnabled() && (! $addon->isTogglablePerGroup() || $addon->isEnabledFor($grupo));
        });
    }

    public function togglablePerGroup(): Collection
    {
        return $this->enabled()->filter(function (Addon $addon) {
            return $addon->isTogglablePerGroup();
        });
    }

    public function forMenu(bool $restrict = false): Collection
    {
        return $this->enabled()->filter(function (Addon $addon) use ($restrict) {
            return $addon->getEntryRoute() && $addon->isRestrict() === $restrict;
        });
    }

    public function forGroupMenu(Grupo $grupo, bool $restrict = false): Collection
    {
        return $this->enabledFor($grupo)->filter(function (Addon $addon) use ($grupo, $restrict) {
            return $addon->getEntryRouteForGroup($grupo) && ($addon->isRestrict($grupo) === $restrict || ($restrict && $addon->getEntryRouteForGroupAdmin()));
        });
    }

    public function withAssets(): Collection
    {
        return $this->all()->filter(function (Addon $addon) {
            return $addon->hasAssets();
        });
    }

    public function allPermissions()
    {
        return $this->all()->map(function (Addon $addon) {
            return $addon->getPermissions();
        })->flatten();
    }

    public function register(Addon $addon)
    {
        $this->registeredAddons->push($addon);
    }
}
