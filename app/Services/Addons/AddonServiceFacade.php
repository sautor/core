<?php

namespace Sautor\Core\Services\Addons;

class AddonServiceFacade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AddonService';
    }
}
