<?php

namespace Sautor\Core\Services\Addons;

use Sautor\Core\Models\Grupo;

class Addon
{
    public string $key;

    public string $label;

    public string $icon;

    public ?string $description;

    private bool $groupAddon = true;

    private bool $togglablePerGroup = true;

    private bool $managersOnly = true;

    private array $groupSettings = [];

    private array $permissions = [];

    private bool $hasAssets = false;

    private $restrict = true;

    private ?string $entryRoute = null;

    private ?string $entryRouteForGroup = null;

    private ?string $entryRouteForGroupAdmin = null;

    /**
     * Addon constructor.
     */
    private function __construct(string $key, string $label, string $icon, ?string $description = null)
    {
        $this->key = $key;
        $this->label = $label;
        $this->icon = $icon;
        $this->description = $description;
    }

    public function isGroupAddon(): bool
    {
        return $this->groupAddon;
    }

    public function isTogglablePerGroup(): bool
    {
        return $this->togglablePerGroup;
    }

    public function isManagersOnly(): bool
    {
        return $this->managersOnly;
    }

    public function hasAssets(): bool
    {
        return $this->hasAssets;
    }

    public function getEntryRoute(): ?string
    {
        return $this->entryRoute ?? null;
    }

    public function getEntryRouteForGroup(): ?string
    {
        return $this->entryRouteForGroup ?? null;
    }

    public function getEntryRouteForGroupAdmin(): ?string
    {
        return $this->entryRouteForGroupAdmin ?? null;
    }

    public function getGroupSettings(): array
    {
        return $this->groupSettings;
    }

    public function getPermissions(): array
    {
        return $this->permissions;
    }

    public function isRestrict(?Grupo $grupo = null): bool
    {
        if (is_callable($this->restrict)) {
            return call_user_func($this->restrict, $grupo);
        }

        return $this->restrict;
    }

    public function isEnabled(): bool
    {
        return \Setting::get(implode('-', ['addon', $this->key, 'enabled']), true);
    }

    public function isEnabledFor(Grupo $grupo): bool
    {
        return \Setting::get(implode('-', ['addon', $this->key, $grupo->id, 'enabled']), false);
    }

    public function setEnabled(bool $enabled)
    {
        \Setting::set(implode('-', ['addon', $this->key, 'enabled']), $enabled);
        \Setting::save();
    }

    public function setEnabledFor(Grupo $grupo, bool $enabled)
    {
        \Setting::set(implode('-', ['addon', $this->key, $grupo->id, 'enabled']), $enabled);
        \Setting::save();
    }

    public static function create(string $key, string $label, string $icon, ?string $description = null): Addon
    {
        return new Addon($key, $label, $icon, $description);
    }

    public function setGroupAddon(bool $groupAddon): Addon
    {
        $this->groupAddon = $groupAddon;

        return $this;
    }

    public function setTogglablePerGroup(bool $togglablePerGroup): Addon
    {
        $this->togglablePerGroup = $togglablePerGroup;

        return $this;
    }

    public function setManagersOnly(bool $managersOnly): Addon
    {
        $this->managersOnly = $managersOnly;

        return $this;
    }

    public function setEntryRoute(?string $entryRoute): Addon
    {
        $this->entryRoute = $entryRoute;

        return $this;
    }

    public function setEntryRouteForGroup(?string $entryRouteForGroup): Addon
    {
        $this->entryRouteForGroup = $entryRouteForGroup;

        return $this;
    }

    public function setEntryRouteForGroupAdmin(?string $entryRouteForGroupAdmin): Addon
    {
        $this->entryRouteForGroupAdmin = $entryRouteForGroupAdmin;

        return $this;
    }

    public function setGroupSettings(array $groupSettings): Addon
    {
        $this->groupSettings = $groupSettings;

        return $this;
    }

    public function setPermissions(array $permissions): Addon
    {
        $this->permissions = $permissions;

        return $this;
    }

    public function withAssets(): Addon
    {
        $this->hasAssets = true;

        return $this;
    }

    /**
     * @param  bool  $restrict
     */
    public function setRestrict(bool|callable $restrict): Addon
    {
        $this->restrict = $restrict;

        return $this;
    }
}
