<?php

namespace Sautor\Core\Http\ViewComposers;

use Illuminate\View\View;
use Sautor\Core\Models\Grupo;

class GrupoComposer
{
    /**
     * Bind data to the view.
     *
     * @return void
     */
    public function compose(View $view)
    {
        /** @var Grupo $grupo */
        $grupo = $view->getData()['grupo'];
        $isManagedByUser = \Auth::user() ? $grupo->isManagedBy(\Auth::user()) : null;

        $canMembersSeeMembers = \Setting::get('grupo-'.$grupo->id.'-view-members-details');

        if (\Auth::user() && ((\Auth::user()->isInGrupo($grupo) && $canMembersSeeMembers) or $isManagedByUser)) {
            $privateMenu = [
                [
                    'label' => 'Membros',
                    'route' => 'membros',
                    'icon' => 'far fa-user-friends',
                ],
            ];

            if (! $grupo->showChildren && $isManagedByUser) {
                $privateMenu[] = [
                    'label' => \Sautor\pluralize($grupo->designacao_grupo),
                    'badge' => $grupo->grupos()->count(),
                    'route' => 'grupos',
                    'icon' => 'far fa-users',
                ];
            }

            // Add addon links
            $restrict_addons = \AddonService::forGroupMenu($grupo, true);
            foreach ($restrict_addons as $addon) {
                if (! $addon->isManagersOnly() or ($addon->isManagersOnly() && $isManagedByUser)) {
                    $privateMenu[] = [
                        'label' => $addon->label,
                        'route' => $addon->getEntryRouteForGroupAdmin() ?? $addon->getEntryRouteForGroup(),
                        'icon' => 'far fa-'.$addon->icon,
                    ];
                }
            }

            if ($isManagedByUser) {
                $privateMenu[] = [
                    'label' => 'Notícias',
                    'route' => 'noticias.admin',
                    'icon' => 'far fa-newspaper',
                ];
                $privateMenu[] = [
                    'label' => 'Páginas',
                    'route' => 'paginas.admin',
                    'icon' => 'far fa-file-alt',
                ];
                $privateMenu[] = [
                    'icon' => 'far fa-pencil-paintbrush',
                    'label' => 'Personalizar',
                    'route' => 'personalizar',
                ];
                $privateMenu[] = [
                    'icon' => 'far fa-cogs',
                    'label' => 'Definições',
                    'route' => 'definicoes',
                ];
            }

            $view->with('privateMenu', $privateMenu);
        }
    }
}
