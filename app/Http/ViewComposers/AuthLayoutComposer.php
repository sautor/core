<?php

namespace Sautor\Core\Http\ViewComposers;

use Illuminate\View\View;
use Sautor\Core\Models\Grupo;

class AuthLayoutComposer
{
    public function compose(View $view)
    {
        $host = request()->host();

        if ($host !== config('app.domain')) {
            $grupo = Grupo::where('dominio', $host)->first();

            if ($grupo) {
                $view->with('grupo', $grupo);
            }
        }
    }
}
