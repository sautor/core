<?php

namespace Sautor\Core\Http\Middleware;

use Closure;

class AddonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next, $addonKey)
    {
        $addon = \AddonService::get($addonKey);
        if (! $addon) {
            abort(500, 'A extensão pedida não existe.');

            return null;
        }

        if (! $addon->isEnabled()) {
            abort(404);

            return null;
        }

        if ($request->route()->hasParameter('grupo')) {
            if (! $addon->isEnabledFor($request->route()->parameter('grupo'))) {
                abort(404);

                return null;
            }
        }

        return $next($request);
    }
}
