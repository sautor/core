<?php

namespace Sautor\Core\Http\Middleware;

use Closure;

class Consent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Setting::get('gdpr-requireForLogin') && ! $request->routeIs('me.showConsentRequest', 'me.storeConsentRequest', 'logout') && \Auth::user() && ! \Auth::user()->hasActiveConsent()) {
            return redirect(route('me.showConsentRequest'));
        }

        return $next($request);
    }
}
