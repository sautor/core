<?php

namespace Sautor\Core\Http\Middleware;

use Closure;
use Sentry\State\Scope;

use function Sentry\configureScope;

class SentryContextMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && app()->bound('sentry')) {
            configureScope(function (Scope $scope): void {
                $scope->setUser([
                    'id' => auth()->id(),
                    'email' => auth()->user()->email,
                    'username' => auth()->user()->nome,
                ]);
            });
        }

        return $next($request);
    }
}
