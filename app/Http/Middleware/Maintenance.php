<?php

namespace Sautor\Core\Http\Middleware;

use Closure;

class Maintenance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Setting::get('man') && (\Auth::guest() || ! \Auth::user()->can('entrar em manutenção'))) {
            return response(view('maintenance'));
        }

        return $next($request);
    }
}
