<?php

namespace Sautor\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Sautor\Core\Models\Redirection;

class RedirectRequests
{
    public function handle(Request $request, Closure $next)
    {
        $redirection = Redirection::findValidOrNull($request->path());

        if (! $redirection && $request->getQueryString()) {
            $path = sprintf('%s?%s', $request->path(), $request->getQueryString());
            $redirection = Redirection::findValidOrNull($path);
        }

        if ($redirection) {
            return redirect($redirection->newUrl, $redirection->statusCode);
        }

        return $next($request);
    }
}
