<?php

namespace Sautor\Core\Http\Controllers;

use Sautor\Core\Models\Evento;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;
use Sautor\Core\Models\Pagina;
use Sautor\Core\Models\Pessoa;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    public function index()
    {
        $cards = [];
        if (\Gate::any(['ver pessoas', 'gerir pessoas'])) {
            $cards[] = [
                'label' => 'Pessoas',
                'value' => Pessoa::count(),
                'icon' => 'fa-user-friends',
                'url' => route('admin.people'),
            ];
        }
        if (\Gate::any('gerir grupos')) {
            $cards[] = [
                'label' => 'Grupos',
                'value' => Grupo::count(),
                'icon' => 'fa-users',
                'url' => route('admin.groups'),
            ];
        }
        if (\Gate::any('gerir notícias')) {
            $cards[] = [
                'label' => 'Notícias',
                'value' => Noticia::count(),
                'icon' => 'fa-newspaper fa-swap-opacity',
                'url' => route('admin.noticias.index'),
            ];
        }
        if (\Gate::any('gerir páginas')) {
            $cards[] = [
                'label' => 'Páginas',
                'value' => Pagina::count(),
                'icon' => 'fa-file-alt',
                'url' => route('admin.paginas.index'),
            ];
        }
        if (\Gate::any('gerir eventos')) {
            $cards[] = [
                'label' => 'Eventos',
                'value' => Evento::count(),
                'icon' => 'fa-calendar-alt',
                'url' => route('admin.eventos'),
            ];
        }
        if (\Gate::any('gerir opções')) {
            if (\AddonService::all()->isNotEmpty()) {
                $cards[] = [
                    'label' => 'Extensões ativas',
                    'value' => \AddonService::enabled()->count(),
                    'icon' => 'fa-puzzle-piece',
                    'url' => route('admin.extensoes'),
                ];
            }

            $cards[] = [
                'label' => 'Manutenção',
                'value' => \Setting::get('man') ? 'Sim' : 'Não',
                'icon' => 'fa-tools',
                'url' => route('admin.definicoes'),
            ];
        }

        return view('admin.dashboard', compact('cards'));
    }

    public function pessoas()
    {
        $people = Pessoa::orderBy('nome')->paginate();

        return view('admin.people', compact('people'));
    }

    public function permissoes()
    {
        $permissions = Permission::all();
        $roles = Role::all();

        return view('admin.permissions', compact('permissions', 'roles'));
    }

    public function redirects()
    {
        return view('admin.redirects');
    }

    public function domains()
    {
        return view('admin.domains');
    }
}
