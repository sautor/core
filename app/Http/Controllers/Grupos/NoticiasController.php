<?php

namespace Sautor\Core\Http\Controllers\Grupos;

use Filament\Notifications\Notification;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $news = Noticia::where('grupo_id', $grupo->id)->latest()->paginate(20);

        return view('groups.news.index', compact('grupo', 'news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        return view('groups.news.create', compact('grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Grupo $grupo, Noticia $noticia)
    {
        $this->authorize('update', $grupo);

        return view('groups.news.edit', compact('grupo', 'noticia'))->with('formModel', $noticia);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Grupo $grupo, Noticia $noticia)
    {
        $this->authorize('update', $grupo);

        $noticia->delete();

        Notification::make()
            ->title('Notícia eliminada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('noticias.index'));
    }
}
