<?php

namespace Sautor\Core\Http\Controllers\Grupos;

use Filament\Notifications\Notification;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pagina;

class PaginasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $pages = Pagina::where('grupo_id', $grupo->id)->orderBy('titulo')->paginate(20);

        return view('groups.pages.index', compact('grupo', 'pages'));
    }

    public function show(Grupo $grupo, $slug)
    {
        $pagina = Pagina::where('slug', $slug)->where('grupo_id', $grupo->id)->firstOrFail();
        $this->authorize('view', $pagina);

        return view('pages.show', compact('grupo', 'pagina'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        return view('groups.pages.create', compact('grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Grupo $grupo, Pagina $pagina)
    {
        $this->authorize('update', $grupo);

        return view('groups.pages.edit', compact('grupo', 'pagina'))->with('formModel', $pagina);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Grupo $grupo, Pagina $pagina)
    {
        $this->authorize('delete', $pagina);

        $pagina->delete();

        Notification::make()
            ->title('Página eliminada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('paginas.admin'));
    }
}
