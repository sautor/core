<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Auth\SocialController;

class EuController extends Controller
{
    private $pessoasController;

    /**
     * EuController constructor.
     */
    public function __construct()
    {
        $this->pessoasController = new PessoasController;
    }

    public function conta()
    {
        $providers = SocialController::enabledProviders();

        return view('me.account', compact('providers'));
    }

    /**
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function perfil()
    {
        return $this->pessoasController->show(\Auth::user());
    }

    public function editPerfil()
    {
        return $this->pessoasController->edit(\Auth::user());
    }

    public function grupos()
    {
        return view('me.groups');
    }

    public function changePassword(Request $request)
    {
        if (! (\Hash::check($request->password, \Auth::user()->password))) {
            Notification::make()
                ->title('A password atual não está correta.')
                ->danger()
                ->send();

            return back();
        }

        if (strcmp($request->password, $request->new_password) == 0) {
            Notification::make()
                ->title('A password nova não pode ser igual à password atual.')
                ->danger()
                ->send();

            return back();
        }

        $this->validate($request, [
            'new_password' => 'required|confirmed',
            'new_password_confirmation' => 'required',
        ]);

        $user = \Auth::user();
        $user->password = bcrypt($request->new_password);
        $user->save();

        Notification::make()
            ->title('Password alterada com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function enable2FA(Request $request)
    {
        $secret = $request->user()->createTwoFactorAuth();

        return [
            'uri' => $secret->toUri(),
            'string' => $secret->toString(),
        ];
    }

    public function confirm2FA(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
        ]);
        $activated = $request->user()->confirmTwoFactorAuth($request->code);

        if (! $activated) {
            return response('Código inválido', 400);
        }

        return [
            'codes' => $request->user()->getRecoveryCodes(),
        ];
    }

    public function disable2FA(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|totp',
        ]);

        $request->user()->disableTwoFactorAuth();

        return response(null);
    }

    public function apiGet(Request $request)
    {
        return $request->user();
    }
}
