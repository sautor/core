<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;

class GruposController extends Controller
{
    public function index(Grupo $grupo)
    {
        $this->authorize('view', $grupo);

        $noticias = Noticia::latest()
            ->where(function (Builder $query) use ($grupo) {
                $query->where('grupo_id', $grupo->id);

                if (! Gate::allows('details', $grupo)) {
                    $query->where('visibilidade', '!=', 'privado');
                }
            })
            ->orWhere(function (Builder $query) use ($grupo) {
                $query->whereHas('grupo', function ($q) use ($grupo) {
                    $q->where('grupo_pai', $grupo->id);
                })->where('visibilidade', 'publico');
            })
            ->with('grupo')
            ->limit(6)
            ->get();

        return view('groups.index', compact('grupo', 'noticias'));
    }

    public function noticias(Grupo $grupo)
    {
        $this->authorize('view', $grupo);

        $news = Noticia::latest()
            ->where(function (Builder $query) use ($grupo) {
                $query->where('grupo_id', $grupo->id);

                if (! Gate::allows('details', $grupo)) {
                    $query->where('visibilidade', '!=', 'privado');
                }
            })
            ->orWhere(function (Builder $query) use ($grupo) {
                $query->whereHas('grupo', function ($q) use ($grupo) {
                    $q->where('grupo_pai', $grupo->id);
                })->where('visibilidade', 'publico');
            })
            ->with('grupo')
            ->paginate(12);

        return view('groups.news', compact('grupo', 'news'));
    }

    public function noticia(Grupo $grupo, Noticia $noticia)
    {
        if (! $noticia->grupo->is($grupo)) {
            abort(404);
        } else {
            $this->authorize('view', $noticia);

            return view('news.show', ['noticia' => $noticia, 'grupo' => $noticia->grupo]);
        }
    }

    public function grupos(Grupo $grupo)
    {
        $isManagedByUser = \Auth::user() && $grupo->isManagedBy(\Auth::user());
        if (! $grupo->showChildren && ! $isManagedByUser) {
            abort(404);

            return;
        }
        $this->authorize('view', $grupo);

        $view = view('groups.groups', compact('grupo'));
        if ($isManagedByUser) {
            $view->with('slugs', Grupo::withTrashed()->select('slug')->get()->pluck('slug'));
        }

        return $view;
    }

    public function store(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->validate($request, [
            'nome' => 'required',
            'slug' => 'unique:grupos',
        ]);
        Grupo::create([
            'nome' => $request->nome,
            'slug' => $request->slug,
            'grupo_pai' => $grupo->id,
        ]);

        Notification::make()
            ->title('Grupo criado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function restore(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $filho = Grupo::withTrashed()->find($request->get('id'));
        if (empty($filho) || ! $filho->pai->is($grupo) || ! $filho->trashed()) {
            Notification::make()
                ->title('Não pode restaurar este grupo.')
                ->danger()
                ->send();

            return back();
        }
        $filho->restore();
        Notification::make()
            ->title('Grupo restaurado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function delete(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        if ($grupo->trashed()) {
            Notification::make()
                ->title('Não pode eliminar este grupo.')
                ->danger()
                ->send();

            return back();
        }
        // TODO: Delete children
        $grupo->delete();
        Notification::make()
            ->title('Grupo eliminado com sucesso.')
            ->success()
            ->send();
        // TODO: Improve redirects
        if ($grupo->pai && \Gate::allows('view', $grupo)) {
            return redirect($grupo->pai->route('index'));
        }

        return redirect('/');
    }

    public function membros(Grupo $grupo)
    {
        $this->authorize('details', $grupo);

        return view('groups.members', compact('grupo'));
    }

    public function personalizar(Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        return view('groups.customize', compact('grupo'));
    }

    public function postPersonalizar(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        try {
            \DB::transaction(function () use ($grupo, $request) {
                $grupo->update($request->only(['descricao']));

                if ($request->has('setting')) {
                    $settings = $request->setting;
                    foreach ($settings as $label => $value) {
                        if (empty($value)) {
                            \Setting::forget($label);
                        } else {
                            \Setting::set($label, $value);
                        }
                    }
                }

                $settingsToUpdate = ['microsite', 'show-leaders', 'show-children'];
                foreach ($settingsToUpdate as $setting) {
                    $settingName = 'grupo-'.$grupo->id.'-'.$setting;
                    \Setting::set($settingName, $request->has('setting.'.$settingName));
                }

                foreach ($request->allFiles() as $name => $file) {
                    if ($file->isValid()) {
                        $path = $file->store(Str::contains($name, ['logo', 'wordmark']) ? 'logos' : 'imagens', 'public');
                        \Setting::set($name, \Storage::disk('public')->url($path));
                    }
                }

                if (! empty($request->remove)) {
                    foreach (array_keys($request->remove) as $key) {
                        \Setting::forget($key);
                    }
                }

                \Setting::save();

                Notification::make()
                    ->title('Personalizações guardadas com sucesso.')
                    ->success()
                    ->send();
            });
        } catch (\Throwable $e) {
            Notification::make()
                ->title('Ocorreu um erro ao guardar as personalizações.')
                ->danger()
                ->send();
        }

        return back();
    }

    public function definicoes(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $slugs = Grupo::withTrashed()->where('id', '!=', $grupo->id)->select('slug')->get()->pluck('slug');

        return view('groups.settings', compact('grupo', 'slugs'))->with('formModel', $grupo);
    }

    public function postDefinicoes(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->validate($request, [
            'nome' => 'required',
            'nome_curto' => 'max:20|nullable',
            'slug' => [
                'required',
                Rule::unique('grupos')->ignore($grupo->id),
            ],
            'url' => 'url|nullable',
            'email' => 'email|nullable',
        ]);

        try {
            \DB::transaction(function () use ($grupo, $request) {
                $update = $request->except('setting', 'remove', 'grupo-'.$grupo->id.'-hero', 'addon');

                $update['designacao_responsavel'] = $update['designacao_responsavel'] === 'Responsável' ? null : $update['designacao_responsavel'];
                $update['designacao_grupo'] = $update['designacao_grupo'] === 'Grupo' ? null : $update['designacao_grupo'];

                $grupo->update($update);

                if ($request->has('setting')) {
                    $settings = $request->setting;
                    foreach ($settings as $label => $value) {
                        if (empty($value)) {
                            \Setting::forget($label);
                        } else {
                            \Setting::set($label, $value);
                        }
                    }
                }

                $settingsToUpdate = ['view-members-details'];
                foreach ($settingsToUpdate as $setting) {
                    $settingName = 'grupo-'.$grupo->id.'-'.$setting;
                    \Setting::set($settingName, $request->has('setting.'.$settingName));
                }

                foreach ($request->allFiles() as $name => $file) {
                    if ($file->isValid()) {
                        $path = $file->store(Str::contains($name, ['logo', 'wordmark']) ? 'logos' : 'imagens', 'public');
                        \Setting::set($name, \Storage::disk('public')->url($path));
                    }
                }

                if (! empty($request->remove)) {
                    foreach (array_keys($request->remove) as $key) {
                        \Setting::forget($key);
                    }
                }

                \Setting::save();

                $addons = \AddonService::togglablePerGroup();
                foreach ($addons as $addon) {
                    $addon->setEnabledFor($grupo, $request->has('addon.'.$addon->key));
                }

                Notification::make()
                    ->title('Definições alteradas com sucesso.')
                    ->success()
                    ->send();
            });
        } catch (\Throwable $e) {
            throw $e;
            Notification::make()
                ->title('Ocorreu um erro ao guardar as alterações.')
                ->danger()
                ->send();
        }

        return back();
    }
}
