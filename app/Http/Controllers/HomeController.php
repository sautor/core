<?php

namespace Sautor\Core\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Sautor\Core\Models\Evento;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $hasHighlight = ! empty(\Setting::get('highlight-title')) && ! empty(\Setting::get('highlight-link'));

        $sliderPath = public_path('img/slider');
        if (is_dir($sliderPath)) {
            $sliderImages = array_diff(scandir($sliderPath), ['..', '.']);
        } else {
            $sliderImages = null;
        }

        $noticias = Noticia::latest()
            ->whereNull('grupo_id')
            ->orWhere(function (Builder $query) {
                $query->whereHas('grupo', function ($q) {
                    $q->where('grupo_pai', null);
                })->where('visibilidade', 'publico');
            })
            ->with('grupo')
            ->limit(6)
            ->get();

        $allEventos = Evento::where('grupo_id', null)->orWhere('visivel_no_inicio', true)->with('grupo')->get();
        $eventos = [];
        $today = Carbon::now()->startOfDay();
        $endOfWeek = Carbon::instance($today)->addDays(6)->endOfDay();
        $day = Carbon::instance($today);
        while ($day->lt($endOfWeek)) {
            $eventos[$day->format('Y-m-d')] = [
                'date' => Carbon::instance($day),
                'events' => [],
            ];
            $day->addDay();
        }
        foreach ($allEventos as $evento) {
            $dates = $evento->runDatesBetween($today, $endOfWeek);
            foreach ($dates as $date) {
                $e = $evento->toArray();
                $e['date'] = $date;
                $eventos[$date->format('Y-m-d')]['events'][] = $e;
            }
        }
        $eventos = collect($eventos)->map(function ($item) {
            $item['events'] = collect($item['events'])->sortBy('date');

            return $item;
        });

        $grupos = Grupo::where('visibilidade', 'visivel')->where('grupo_pai', null)->orderBy('ordem')->orderBy('nome')->get();

        return view('home.index', compact('hasHighlight', 'sliderImages', 'noticias', 'eventos', 'grupos'));
    }
}
