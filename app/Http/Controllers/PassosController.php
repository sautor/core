<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Models\Passo;
use Sautor\Core\Models\Pessoa;

class PassosController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request, Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);

        $this->validate($request, [
            'nome' => 'required',
            'data' => 'date',
        ]);

        Passo::create([
            'nome' => $request->nome,
            'data' => $request->data,
            'pessoa_id' => $pessoa->id,
        ]);

        Notification::make()
            ->title('Passo registado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Pessoa $pessoa, Passo $passo)
    {
        $this->authorize('update', $pessoa);

        $passo->delete();

        Notification::make()
            ->title('Passo removido com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
