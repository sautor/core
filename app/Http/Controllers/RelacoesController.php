<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Models\Pessoa;
use Sautor\Core\Models\Relacao;

class RelacoesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);

        $this->validate($request, [
            'nic' => 'required|not_in:'.$pessoa->nic,
            'relacao' => 'required|in:pai,avo,padrinho,padrasto',
        ]);

        $asc = Pessoa::where('nic', $request->nic)->first();

        if (! $asc) {
            return redirect(route('pessoas.create').'?nic='.$request->nic.'&relate_with='.$pessoa->id.'&relation='.$request->relacao);
        }

        Relacao::create([
            'pessoa1_id' => $asc->id,
            'pessoa2_id' => $pessoa->id,
            'relacao' => $request->relacao,
        ]);

        Notification::make()
            ->title('Relação adicionada com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Pessoa $pessoa, Pessoa $pessoa2)
    {
        $rows = $pessoa->descendentes()->detach($pessoa2->id);
        if ($rows === 0) {
            $rows = $pessoa->ascendentes()->detach($pessoa2->id);
        }
        if ($rows > 0) {
            Notification::make()
                ->title('Relação removida com sucesso.')
                ->success()
                ->send();
        } else {
            Notification::make()
                ->title('A relação não foi encontrada.')
                ->danger()
                ->send();
        }

        return back();
    }
}
