<?php

namespace Sautor\Core\Http\Controllers\Admin;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;

class OpcoesController extends Controller
{
    public function definicoes()
    {
        return view('admin.settings');
    }

    public function updateDefinicoes(Request $request)
    {
        $settings = $request->setting;
        foreach ($settings as $label => $value) {
            if (empty($value)) {
                \Setting::forget($label);
            } else {
                \Setting::set($label, $value);
            }
        }
        foreach (['man', 'gdpr-blockData', 'gdpr-requireForLogin', 'cookie-banner'] as $label) {
            \Setting::set($label, array_key_exists($label, $settings));
        }

        foreach ($request->allFiles() as $name => $file) {
            if ($file->isValid()) {
                $path = $file->store(Str::contains($name, ['logo', 'wordmark']) ? 'logos' : 'imagens', 'public');
                \Setting::set($name, \Storage::disk('public')->url($path));
            }
        }

        if (! empty($request->remove)) {
            foreach (array_keys($request->remove) as $key) {
                \Setting::forget($key);
            }
        }

        \Setting::save();
        Notification::make()
            ->title('Definições guardadas com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function personalizar()
    {
        return view('admin.customize');
    }

    public function updatePersonalizar(Request $request)
    {
        $settings = $request->setting;
        foreach ($settings as $label => $value) {
            if (empty($value)) {
                \Setting::forget($label);
            } else {
                \Setting::set($label, $value);
            }
        }

        foreach ($request->allFiles() as $name => $file) {
            if ($file->isValid()) {
                $path = $file->store(Str::contains($name, ['logo', 'wordmark']) ? 'logos' : 'imagens', 'public');
                \Setting::set($name, \Storage::disk('public')->url($path));
            }
        }

        if (! empty($request->remove)) {
            foreach (array_keys($request->remove) as $key) {
                \Setting::forget($key);
            }
        }

        \Setting::save();
        Notification::make()
            ->title('Personalizações guardadas com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
