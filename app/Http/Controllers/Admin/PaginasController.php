<?php

namespace Sautor\Core\Http\Controllers\Admin;

use Filament\Notifications\Notification;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Pagina;

class PaginasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $pages = Pagina::whereNull('grupo_id')->orderBy('titulo')->paginate(20);

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Pagina::class);

        return view('admin.pages.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Pagina $pagina)
    {
        $this->authorize('update', $pagina);

        return view('admin.pages.edit', compact('pagina'))->with('formModel', $pagina);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Pagina $pagina)
    {
        $this->authorize('delete', $pagina);

        $pagina->delete();

        Notification::make()
            ->title('Página eliminada com sucesso.')
            ->success()
            ->send();

        return redirect(route('admin.paginas.index'));
    }
}
