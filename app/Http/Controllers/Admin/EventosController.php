<?php

namespace Sautor\Core\Http\Controllers\Admin;

use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Evento;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $events = Evento::with('grupo')->get();

        return view('admin.events', compact('events'));
    }
}
