<?php

namespace Sautor\Core\Http\Controllers\Admin;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class GruposController extends Controller
{
    public function index()
    {
        $grupos = Grupo::whereNull('grupo_pai')->orderBy('nome')->get();
        $gruposEliminados = Grupo::onlyTrashed()->whereNull('grupo_pai')->orderBy('nome')->get();
        $slugs = Grupo::withTrashed()->select('slug')->get()->pluck('slug');

        return view('admin.groups', compact('grupos', 'gruposEliminados', 'slugs'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'slug' => 'unique:grupos',
        ]);
        Grupo::create([
            'nome' => $request->nome,
            'slug' => $request->slug,
        ]);

        Notification::make()
            ->title('Grupo criado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function restore(Request $request)
    {
        $filho = Grupo::withTrashed()->find($request->get('id'));
        if (empty($filho) || ! $filho->trashed()) {
            Notification::make()
                ->title('Não é possível restaurar este grupo.')
                ->danger()
                ->send();

            return back();
        }
        $filho->restore();
        Notification::make()
            ->title('Grupo restaurado com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
