<?php

namespace Sautor\Core\Http\Controllers\Admin;

use AddonService;
use Filament\Notifications\Notification;
use Sautor\Core\Http\Controllers\Controller;

class AddonController extends Controller
{
    public function index()
    {
        $addons = AddonService::all();

        return view('admin.extensions', compact('addons'));
    }

    public function enable(string $key)
    {
        $addon = AddonService::get($key);
        if (! $addon) {
            abort(404);
        }
        if ($addon->isEnabled()) {
            return abort(400);
        }
        $addon->setEnabled(true);

        Notification::make()
            ->title('Extensão ativada com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function disable(string $key)
    {
        $addon = AddonService::get($key);
        if (! $addon) {
            abort(404);
        }
        if (! $addon->isEnabled()) {
            return abort(400);
        }

        Notification::make()
            ->title('Extensão desativada com sucesso.')
            ->success()
            ->send();
        $addon->setEnabled(false);

        return back();
    }
}
