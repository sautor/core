<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;
use Sautor\Core\Models\Pagina;
use Sautor\Core\Models\Pessoa;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {
        Auth::shouldUse('api');
        $term = $request->query('term');
        $people = Pessoa::search($term)->get()->filter(function ($pessoa) {
            return \Gate::allows('view', $pessoa);
        })->values();
        $groups = Grupo::search($term)->get()->filter(function ($grupo) {
            return \Gate::allows('view', $grupo);
        })->values();
        $news = Noticia::search($term)->get()->filter(function ($noticia) {
            return \Gate::allows('view', $noticia);
        })->values();
        $pages = Pagina::search($term)->get()->filter(function ($pagina) {
            return \Gate::allows('view', $pagina);
        })->values();

        return compact('people', 'groups', 'news', 'pages');
    }
}
