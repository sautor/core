<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Evento;

class EventosController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response|string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Evento::class);

        $this->validate($request, [
            'nome' => 'required',
            'ano' => 'numeric|nullable',
            'visivel_no_inicio' => 'boolean',
            'grupo_id' => 'exists:grupos,id|nullable',
            'cron' => 'required|cron',
        ]);

        $evento = Evento::create($request->all());

        $evento->loadMissing('grupo');

        return $evento->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response|string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Evento $evento)
    {
        $this->authorize('update', $evento);

        $this->validate($request, [
            'nome' => 'required',
            'ano' => 'numeric|nullable',
            'visivel_no_inicio' => 'boolean',
            'grupo_id' => 'exists:grupos,id|nullable',
            'cron' => 'required|cron',
        ]);

        $evento->update($request->all());

        $evento->loadMissing('grupo');

        return $evento->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     */
    public function destroy(Evento $evento)
    {
        $this->authorize('delete', $evento);

        $evento->delete();

        return response('', 200);
    }
}
