<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pagina;

class PaginasController extends Controller
{
    /**
     * @return string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Pagina::class);

        $this->validate($request, [
            'titulo' => 'required',
            'slug' => ['required', 'regex:/^[a-z0-9]+(?:(-|\/)[a-z0-9]+)*$/', Rule::unique('paginas')],
            'grupo_id' => 'exists:grupos,id|nullable',
            'page_template_id' => [
                'nullable',
                Rule::in(($request->grupo_id ? \PageTemplateService::forGroup() : \PageTemplateService::forMain())->pluck('key')),
            ],
            'options' => 'json|nullable',
            'pagina_pai' => 'exists:paginas,id|nullable',
        ]);

        $pagina = new Pagina;

        $pagina->titulo = $request->titulo;
        $pagina->conteudo = $request->conteudo;
        $pagina->slug = $request->slug;
        $pagina->grupo_id = $request->grupo_id;
        $pagina->publico = $request->publico;
        $pagina->page_template_id = $request->page_template_id;
        $pagina->options = json_decode($request->options);
        $pagina->pagina_pai = $request->pagina_pai;

        $pagina->imagem = $request->imagem;

        $pagina->save();

        $pagina->load('grupo');

        return $pagina->toJson();
    }

    /**
     * @return string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Pagina $pagina)
    {
        $this->authorize('update', $pagina);

        $this->validate($request, [
            'titulo' => 'required',
            'slug' => [
                'required',
                'regex:/^[a-z0-9]+(?:(-|\/)[a-z0-9]+)*$/',
                Rule::unique('paginas')->ignore($pagina->id),
            ],
            'grupo_id' => 'exists:grupos,id|nullable',
            'page_template_id' => [
                'nullable',
                Rule::in(($request->grupo_id ? \PageTemplateService::forGroup() : \PageTemplateService::forMain())->pluck('key')),
            ],
            'options' => 'json|nullable',
            'pagina_pai' => 'exists:paginas,id|nullable',
        ]);

        $pagina->titulo = $request->titulo;
        $pagina->conteudo = $request->conteudo;
        $pagina->slug = $request->slug;
        $pagina->grupo_id = $request->grupo_id;
        $pagina->publico = $request->publico;
        $pagina->page_template_id = $request->page_template_id;
        $pagina->options = json_decode($request->options);
        $pagina->pagina_pai = $request->pagina_pai;

        $pagina->imagem = $request->imagem;

        $pagina->save();

        $pagina->load('grupo');

        return $pagina->toJson();
    }

    public function validSlug(Request $request)
    {
        $uniqueRule = Rule::unique('paginas');
        if ($request->has('id')) {
            $uniqueRule->ignore($request->id);
        }
        $this->validate($request, [
            'slug' => ['required', 'regex:/^[a-z0-9]+(?:(-|\/)[a-z0-9]+)*$/', $uniqueRule],
        ]);

        return response('', 200);
    }

    public function templates(?Grupo $grupo = null)
    {
        if ($grupo) {
            $templates = \PageTemplateService::forGroup();
        } else {
            $templates = \PageTemplateService::forMain();
        }

        return $templates->map(function ($item) {
            return $item->toArray();
        });
    }
}
