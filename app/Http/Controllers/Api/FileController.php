<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;

class FileController extends Controller
{
    public function show($path)
    {
        return array_map(function ($name) {
            $info = pathinfo($name);

            return [
                'key' => $name,
                'name' => $info['basename'],
                'extension' => $info['extension'],
                'url' => \Storage::disk('public')->url($name),
                'size' => \Storage::disk('public')->size($name),
                'mime' => \Storage::disk('public')->mimeType($name),
                'lastModified' => \Storage::disk('public')->lastModified($name),
            ];
        }, \Storage::disk('public')->files($path));
    }

    public function upload(Request $request, $path)
    {
        $this->validate($request, [
            'file' => 'file',
        ]);
        $file = $request->file('file');
        if (! $file->isValid()) {
            abort(400);

            return;
        }
        $path = $file->store($path, 'public');

        return \Storage::disk('public')->url($path);
    }
}
