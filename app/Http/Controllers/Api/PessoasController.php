<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Pessoa;

class PessoasController extends Controller
{
    public function unique(Request $request, $column)
    {
        $query = Pessoa::where($column, $request->value);
        if ($request->has('id')) {
            $query = $query->where('id', '!=', $request->id);
        }
        $exists = $query->exists();
        if ($exists) {
            abort(409);

            return;
        }

        return response('', 200);
    }

    /**
     * @return array
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function fromNic($nic)
    {
        $this->authorize('store', Pessoa::class);
        $pessoa = Pessoa::where('nic', $nic)->firstOrFail();

        return [
            'nome' => $pessoa->nome,
            'nic' => $pessoa->nic,
            'foto' => $pessoa->foto,
        ];
    }
}
