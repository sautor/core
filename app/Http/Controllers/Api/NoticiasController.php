<?php

namespace Sautor\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Noticia;

class NoticiasController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response|string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Noticia::class);

        $this->validate($request, [
            'titulo' => 'required',
            'conteudo' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
            'autor_id' => 'exists:pessoas,id|nullable',
            'visibilidade' => 'in:publico,nao_listado,privado',
        ]);

        $noticia = new Noticia;

        $noticia->titulo = $request->titulo;
        $noticia->conteudo = $request->conteudo;
        $noticia->grupo_id = $request->grupo_id;
        $noticia->autor_id = $request->autor_id ?? \Auth::id();
        $noticia->visibilidade = $request->visibilidade;

        $noticia->imagem = $request->imagem;

        $noticia->save();

        return $noticia->toJson();
    }

    /**
     * @return string
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Noticia $noticia)
    {
        $this->authorize('update', $noticia);

        $this->validate($request, [
            'titulo' => 'required',
            'conteudo' => 'required',
            'grupo_id' => 'exists:grupos,id|nullable',
            'autor_id' => 'exists:pessoas,id|nullable',
            'visibilidade' => 'in:publico,nao_listado,privado',
        ]);

        $noticia->titulo = $request->titulo;
        $noticia->conteudo = $request->conteudo;
        $noticia->grupo_id = $request->grupo_id;
        $noticia->autor_id = $request->autor_id ?? \Auth::id();
        $noticia->visibilidade = $request->visibilidade;

        $noticia->imagem = $request->imagem;

        $noticia->save();

        return $noticia->toJson();
    }
}
