<?php

namespace Sautor\Core\Http\Controllers\Api;

use Auth;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class GruposController extends Controller
{
    public function managed()
    {
        $user = Auth::user();
        $grupos = Grupo::all()->filter(function ($g) use ($user) {
            return $g->isManagedBy($user);
        });

        return $grupos;
    }

    public function show(Grupo $grupo)
    {
        $this->authorize('view', $grupo);

        return $grupo;
    }
}
