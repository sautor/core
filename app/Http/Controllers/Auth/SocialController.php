<?php

namespace Sautor\Core\Http\Controllers\Auth;

use Auth;
use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\SocialAccount;

class SocialController extends Controller
{
    const PROVIDERS = [
        'facebook' => [
            'key' => 'facebook',
            'name' => 'Facebook',
            'icon' => 'fab fa-facebook',
        ],
        'google' => [
            'key' => 'google',
            'name' => 'Google',
            'icon' => 'fab fa-google',
        ],
        'microsoft' => [
            'key' => 'microsoft',
            'name' => 'Microsoft',
            'icon' => 'fab fa-microsoft',
        ],
        'twitter' => [
            'key' => 'twitter',
            'name' => 'Twitter',
            'icon' => 'fab fa-twitter',
        ],
    ];

    public static function enabledProviders()
    {
        return collect(SocialController::PROVIDERS)->filter(function ($provider) {
            return ! empty(config('services.'.$provider['key'].'.client_id')) && ! empty(config('services.'.$provider['key'].'.client_secret'));
        });
    }

    public function link(Request $request, $provider)
    {
        $socialUser = $request->session()->get('socialUser');
        if (empty($socialUser)) {
            Notification::make()
                ->title('Ocorreu um erro ao ligar a conta de '.SocialController::PROVIDERS[$provider]['name'].'.')
                ->danger()
                ->send();

            return redirect(route('me.account').'#contas');
        }

        return view('auth.link', compact('socialUser'))->with('provider', SocialController::PROVIDERS[$provider]);
    }

    public function linkConfirm(Request $request, $provider)
    {
        if (empty($provider) || ! $request->has('provider_user_id')) {
            Notification::make()
                ->title('Ocorreu um erro ao associar a conta de '.SocialController::PROVIDERS[$provider]['name'].'.')
                ->danger()
                ->send();

            return redirect(route('me.account').'#contas');
        }

        SocialAccount::create([
            'provider' => $provider,
            'provider_user_id' => $request->get('provider_user_id'),
            'pessoa_id' => Auth::id(),
        ]);

        Notification::make()
            ->title('Conta de '.SocialController::PROVIDERS[$provider]['name'].' associada com sucesso.')
            ->success()
            ->send();

        return redirect(route('me.account').'#contas');
    }

    public function unlink(Request $request, $provider)
    {
        $socialAccount = Auth::user()->socialAccounts()->where('provider', $provider)->firstOrFail();
        $socialAccount->delete();

        Notification::make()
            ->title('Conta de '.SocialController::PROVIDERS[$provider]['name'].' desassociada com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
