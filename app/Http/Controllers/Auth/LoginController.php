<?php

namespace Sautor\Core\Http\Controllers\Auth;

use Auth;
use Filament\Notifications\Notification;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Auth\Traits\AuthenticatesUsers;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\SocialAccount;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('auth.login')->with('providers', SocialController::enabledProviders());
    }

    protected function attemptLogin(Request $request)
    {
        try {
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        } catch (HttpResponseException $exception) {
            $this->incrementLoginAttempts($request);
            throw $exception;
        }
    }

    public function socialRedirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function socialCallback(Request $request, $provider)
    {
        try {
            $socialUser = Socialite::driver($provider)->user();
        } catch (\GuzzleHttp\Exception\ClientException $ex) {
            \Log::debug('error');
            \Log::debug((string) $ex->getResponse()->getBody());
            throw $ex;
        }
        // $socialUser = Socialite::driver($provider)->user();
        // If is logged in we connect, otherwise we try to login
        if (Auth::check()) {
            $request->session()->put('socialUser', $socialUser);

            return redirect(route('social.link', $provider));
        } else {
            return $this->socialLogin($request, $provider, $socialUser);
        }
    }

    private function socialLogin(Request $request, $provider, $socialUser)
    {
        $socialAccount = SocialAccount::where('provider', $provider)->where('provider_user_id', $socialUser->id)->first();
        if (empty($socialAccount)) {
            Notification::make()
                ->title('Esta conta não está ligada a nenhuma conta '.config('app.name').'.')
                ->danger()
                ->send();

            return redirect(route('login'));
        } else {
            Auth::login($socialAccount->pessoa);

            return $this->sendLoginResponse($request);
        }
    }
}
