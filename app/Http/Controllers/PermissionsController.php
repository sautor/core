<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Models\Pessoa;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsController extends Controller
{
    /**
     * PermissionsController constructor.
     */
    public function __construct()
    {
        $this->middleware('can:gerir permissões');
    }

    /**
     * Add a permission to the user.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function addPermission(Request $request, Pessoa $pessoa)
    {
        $this->validate($request, [
            'permission' => 'exists:permissions,name',
        ]);

        $pessoa->givePermissionTo($request->permission);

        Notification::make()
            ->title('Permissão concedida com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Removes a permission from the user.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function removePermission(Request $request, Pessoa $pessoa, Permission $permission)
    {
        if (! $pessoa->hasPermissionTo($permission)) {
            Notification::make()
                ->title($pessoa->nome_curto.' não tem permissão para '.$permission->name.'.')
                ->info()
                ->send();
        }

        $pessoa->revokePermissionTo($permission);

        Notification::make()
            ->title('Permissão revogada com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Add a role to the user.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function addRole(Request $request, Pessoa $pessoa)
    {
        $this->validate($request, [
            'role' => 'exists:roles,name',
        ]);

        $pessoa->assignRole($request->role);

        Notification::make()
            ->title('Função atribuída com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Removes a role from the user.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function removeRole(Request $request, Pessoa $pessoa, Role $role)
    {
        if (! $pessoa->hasRole($role)) {
            Notification::make()
                ->title($pessoa->nome_curto.' não tem a função '.ucfirst($role->name).'.')
                ->info()
                ->send();
        }

        $pessoa->removeRole($role);

        Notification::make()
            ->title('Função removida com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
