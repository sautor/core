<?php

namespace Sautor\Core\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Sautor\Core\Models\Noticia;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $news = Noticia::latest()
            ->whereNull('grupo_id')
            ->orWhere(function (Builder $query) {
                $query->whereHas('grupo', function ($q) {
                    $q->where('grupo_pai', null);
                })->where('visibilidade', 'publico');
            })
            ->with(['grupo', 'autor'])
            ->paginate(12);

        return view('news.index', compact('news'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function show(Noticia $noticia)
    {
        $this->authorize('view', $noticia);

        return view('news.show', ['noticia' => $noticia, 'grupo' => $noticia->grupo]);
    }
}
