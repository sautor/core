<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Inscricao;
use Sautor\Core\Models\Pessoa;
use Sautor\Core\Models\Relacao;

class PessoasController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('store', Pessoa::class);
        $view = view('people.create');
        if ($request->has(['nic'])) {
            $view->with('nic', $request->nic);
        }
        if ($request->has(['relate_with', 'relation'])) {
            $pessoa = Pessoa::findOrFail($request->relate_with);
            $view->with('related_person', $pessoa)->with('relacao', $request->relation);
        }
        if ($request->has(['register_in', 'cargo', 'responsavel', 'anoletivo'])) {
            $grupo = Grupo::findOrFail($request->register_in);
            $view->with('grupo', $grupo)->with('cargo', $request->cargo)->with('responsavel', $request->responsavel)
                ->with('anoletivo', $request->anoletivo);
        }

        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('store', Pessoa::class);

        $rules = [
            'nome' => 'required',
            'data_nascimento' => 'date|nullable',
            'genero' => 'required|in:M,F',
            'nic' => 'required|unique:pessoas',
            'email' => 'email|nullable|unique:pessoas',
            'telefone' => 'phone:INTERNATIONAL,PT|nullable',
            'relacao' => 'nullable|in:pai,avo,padrinho,padrasto',
            'relate_with' => 'nullable|exists:pessoas,id',
        ];

        if (\Auth::user()->can('gerir pessoas')) {
            $rules['ordem'] = 'nullable|in:diacono,sacerdote';
        }

        $this->validate($request, $rules);

        // TODO: All this in transaction

        $pessoa = Pessoa::create($request->except(['relacao', 'relate_with', 'register_in', 'cargo', 'responsavel', 'anoletivo']));

        if ($request->has(['relacao', 'relate_with'])) {
            Relacao::create([
                'pessoa1_id' => $pessoa->id,
                'pessoa2_id' => $request->relate_with,
                'relacao' => $request->relacao,
            ]);

            Notification::make()
                ->title('Familiar registado com sucesso.')
                ->success()
                ->send();

            return redirect(route('pessoas.show', $request->relate_with));
        }

        if ($request->has(['register_in', 'cargo', 'responsavel', 'anoletivo'])) {
            Inscricao::create([
                'grupo_id' => $request->register_in,
                'pessoa_id' => $pessoa->id,
                'cargo' => $request->cargo,
                'responsavel' => $request->responsavel == 1,
                'ano_letivo' => $request->anoletivo == 1 ? \Sautor\anoLetivo() : null,
            ]);
            Notification::make()
                ->title('Pessoa registada e inscrita com sucesso.')
                ->success()
                ->send();

            return redirect(route('pessoas.show', $pessoa));
        }

        Notification::make()
            ->title('Pessoa registada com sucesso.')
            ->success()
            ->send();

        return redirect(route('pessoas.show', $pessoa));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Pessoa $pessoa)
    {
        $this->authorize('view', $pessoa);
        if (\Setting::get('gdpr-blockData') && ! $pessoa->hasActiveConsent()) {
            return view('pessoas.show-blocked', compact('pessoa'));
        }
        $familiares = $pessoa->familiares();

        return view('people.show', compact('pessoa', 'familiares'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);

        return view('people.edit', compact('pessoa'))->with('formModel', $pessoa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);

        $rules = [
            'nome' => 'required',
            'data_nascimento' => 'date|nullable',
            'genero' => 'required|in:M,F',
            'nic' => [
                'required',
                Rule::unique('pessoas')->ignore($pessoa->id),
            ],
            'email' => [
                'email',
                'nullable',
                Rule::unique('pessoas')->ignore($pessoa->id),
            ],
            'telefone' => 'phone:INTERNATIONAL,PT|nullable',
        ];

        if (\Auth::user()->can('gerir pessoas')) {
            $rules['ordem'] = 'nullable|in:diacono,sacerdote';
        }

        $this->validate($request, $rules);

        $pessoa->update($request->all());

        if ($pessoa->is(\Auth::user())) {
            Notification::make()
                ->title('Perfil editado com sucesso.')
                ->success()
                ->send();

            return redirect(route('me.profile'));
        }

        Notification::make()
            ->title('Pessoa editada com sucesso.')
            ->success()
            ->send();

        return redirect(route('pessoas.show', $pessoa));
    }

    public function updatePhoto(Request $request, Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);
        $this->validate($request, [
            'foto' => 'required|base64image',
        ]);
        $fileName = 'pessoas/'.$pessoa->id.'.jpg';
        \Storage::put('public/'.$fileName, base64_decode(explode(',', $request->foto)[1]));
        $pessoa->update([
            'foto' => '/storage/'.$fileName,
        ]);

        Notification::make()
            ->title('Foto alterada com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function sendPasswordResetLink(Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);
        $linkSent = \Password::broker()->sendResetLink(['email' => $pessoa->email]);
        if ($linkSent === PasswordBroker::RESET_LINK_SENT) {
            Notification::make()
                ->title('Link de redefinição enviado com sucesso.')
                ->success()
                ->send();
        } else {
            Notification::make()
                ->title('Ocorreu um erro ao enviar o link de redefinição.')
                ->danger()
                ->send();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Pessoa $pessoa)
    {
        // TODO: Make this a soft delete

        $this->authorize('destroy', $pessoa);
        if ($pessoa->is(\Auth::user())) {
            abort(403);
        }

        $pessoa->delete();

        Notification::make()
            ->title('Pessoa eliminada com sucesso.')
            ->success()
            ->send();

        return redirect('/');
    }
}
