<?php

namespace Sautor\Core\Http\Controllers;

use Sautor\Core\Models\Pagina;

class PaginasController extends Controller
{
    public function show($slug)
    {
        $pagina = Pagina::where('slug', $slug)->whereNull('grupo_id')->firstOrFail();
        $this->authorize('view', $pagina);

        return view('pages.show', compact('pagina'));
    }
}
