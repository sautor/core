<?php

namespace Sautor\Core\Http\Controllers;

use Carbon\Carbon;
use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Models\Consentimento;
use Sautor\Core\Models\Pessoa;

class ConsentimentosController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Pessoa $pessoa)
    {
        $this->authorize('update', $pessoa);

        $this->validate($request, [
            'ficheiro' => 'required|file|mimes:pdf,jpeg,png',
            'assinado_por' => ($pessoa->idade < 16 ? 'required' : 'nullable').'|exists:pessoas,nic',
        ]);

        $filename = $request->ficheiro->store('consentimentos');

        $consentimento = Consentimento::create([
            'digital' => false,
            'pessoa_id' => $pessoa->id,
            'ficheiro' => $filename,
            'assinado_por_id' => $request->assinado_por ? Pessoa::whereNic($request->assinado_por)->first()->id : null,
            'added_by_id' => \Auth::id(),
        ]);

        Notification::make()
            ->title('Consentimento registado com sucesso.')
            ->success()
            ->send();

        return redirect(route('pessoas.show', $pessoa));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Pessoa $pessoa, Consentimento $consentimento)
    {
        $this->authorize('view', $pessoa);

        if ($consentimento->digital) {
            abort(400);
        }

        return response()->file(storage_path('app/'.$consentimento->ficheiro));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Pessoa $pessoa, Consentimento $consentimento)
    {
        $this->authorize('update', $pessoa);

        $consentimento->revoked_at = Carbon::now();
        $consentimento->save();

        Notification::make()
            ->title('Consentimento revogado com sucesso.')
            ->success()
            ->send();

        return back();
    }

    public function showRequest()
    {
        return view('consents.show-request');
    }

    public function storeRequest(Request $request)
    {
        $this->validate($request, [
            'consent' => 'required',
        ]);

        $consentimento = Consentimento::create([
            'digital' => true,
            'pessoa_id' => \Auth::id(),
            'ip' => $request->ip(),
        ]);

        Notification::make()
            ->title('Consentimento registado com sucesso.')
            ->success()
            ->send();

        return redirect(route('me.profile'));
    }
}
