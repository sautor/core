<?php

namespace Sautor\Core\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, string $uuid)
    {
        $media = Media::where('uuid', $uuid)->firstOrFail();
        $this->authorize('view', $media->model);

        return $media->toInlineResponse($request);
    }
}
