<?php

namespace Sautor\Core\Http\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Inscricao;
use Sautor\Core\Models\Pessoa;

class InscricoesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        $this->validate($request, [
            'nic' => 'required|not_in:'.$grupo->inscritos->pluck('nic')->implode(','),
            'anoletivo' => 'required|boolean',
        ]);

        $pessoa = Pessoa::where('nic', $request->nic)->first();

        if (! $pessoa) {
            return redirect(route('pessoas.create').'?nic='.$request->nic.'&register_in='.$grupo->id.
                '&anoletivo='.$request->anoletivo.'&responsavel='.($request->has('responsavel') ? '1' : '0').
                '&cargo='.$request->cargo);
        }

        $grupo->inscritos()->attach($pessoa->id, ['responsavel' => $request->has('responsavel'),
            'cargo' => $request->cargo, 'ano_letivo' => ($request->anoletivo ? \Sautor\anoLetivo() : null), ]);

        Notification::make()
            ->title('Pessoa inscrita com sucesso.')
            ->success()
            ->send();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Grupo $grupo, $inscricao)
    {
        $insc = Inscricao::find($inscricao);
        if (! $insc) {
            Notification::make()
                ->title('Inscrição inexistente.')
                ->danger()
                ->send();

            return back();
        }

        $insc->delete();

        Notification::make()
            ->title('Inscrição removida com sucesso.')
            ->success()
            ->send();

        return back();
    }
}
