<?php

namespace Sautor\Core\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Sautor\Core\Console\Commands\CheckDomainsCommand;
use Sautor\Core\Console\Commands\InitPermissions;
use Sautor\Core\Console\Commands\StorageLinkCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckDomainsCommand::class,
        InitPermissions::class,
        StorageLinkCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('domains:check')->hourly();
    }
}
