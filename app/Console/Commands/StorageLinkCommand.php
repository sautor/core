<?php

namespace Sautor\Core\Console\Commands;

use Illuminate\Foundation\Console\StorageLinkCommand as LaravelStorageLinkCommand;
use Sautor\Core\Services\Addons\Addon;

class StorageLinkCommand extends LaravelStorageLinkCommand
{
    protected function links()
    {
        $base_links = parent::links();

        $addon_links = \AddonService::withAssets()->mapWithKeys(function (Addon $addon) {
            return [public_path('addons/'.$addon->key) => __DIR__.'/../../../../'.$addon->key.'/public'];
        })->all();

        return array_merge($base_links, $addon_links);
    }
}
