<?php

namespace Sautor\Core\Console\Commands;

use Illuminate\Console\Command;
use Sautor\Core\Actions\ConfirmDomainAction;
use Sautor\Core\Models\Grupo;

class CheckDomainsCommand extends Command
{
    protected $signature = 'domains:check';

    protected $description = 'Check all ';

    public function handle(): int
    {
        $grupos = Grupo::whereNotNull('dominio')->get();

        if ($grupos->isEmpty()) {
            $this->info('Sem domínios para validar.');

            return self::SUCCESS;
        }

        $this->info('A validar '.$grupos->count().' domínios.');
        $this->newLine();

        foreach ($grupos as $grupo) {
            $result = (new ConfirmDomainAction($grupo))->execute();

            if ($result) {
                $this->info('Domínio '.$grupo->dominio.' confirmado.');
            } else {
                $this->error('Domínio '.$grupo->dominio.' não confirmado.');
            }
        }

        return self::SUCCESS;
    }
}
