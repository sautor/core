<?php

namespace Sautor\Core\Console\Commands;

use Illuminate\Console\Command;
use Sautor\Core\Models\Pessoa;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InitPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:init {--assign-admin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $assign_admin = $this->option('assign-admin');

        $default_permissions = [
            'ver pessoas',
            'gerir pessoas',
            'gerir grupos',
            'gerir permissões',
            'gerir notícias',
            'gerir páginas',
            'gerir opções',
            'gerir eventos',
            'gerir domínios',
            'programar com api',
            'entrar em manutenção',
            'ver logs',
        ];

        $all_permissions = array_merge($default_permissions, \AddonService::allPermissions()->all());

        // Create all permissions
        foreach ($all_permissions as $permission) {
            Permission::findOrCreate($permission);
        }

        $this->info('Permissões criadas');

        // Create roles
        $default_roles = [
            'administrador' => Permission::all(),
        ];

        foreach ($default_roles as $role_name => $role_perms) {
            $role = Role::findOrCreate($role_name);
            $role->givePermissionTo($role_perms);
        }

        $this->info('Papéis criados');

        if ($assign_admin) {
            while (true) {
                $id = $this->ask('Qual o ID do utilizador administrador?');
                $user = Pessoa::find($id);
                if (! $user) {
                    $this->error('Não existe utilizador com esse ID.');
                    $retry = $this->confirm('Procurar um novo utilizador?');
                    if ($retry) {
                        continue;
                    } else {
                        break;
                    }
                }
                $confirm = $this->confirm('Dar o papel de administrador ao utilizador '.$user->nome_exibicao.'?');
                if ($confirm) {
                    $user->assignRole('administrador');
                    break;
                } else {
                    $retry = $this->confirm('Procurar um novo utilizador?');
                    if ($retry) {
                        continue;
                    } else {
                        break;
                    }
                }
            }
        }

        return self::SUCCESS;
    }
}
