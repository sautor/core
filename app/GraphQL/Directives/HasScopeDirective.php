<?php

namespace Sautor\Core\GraphQL\Directives;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Exceptions\DefinitionException;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class HasScopeDirective extends BaseDirective implements FieldMiddleware
{
    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'GRAPHQL'
"""
Limit field access to users of a certain role.
"""
directive @hasScope(
  """
  The name of the role authorized users need to have.
  """
  scope: String!
) on FIELD_DEFINITION
GRAPHQL;
    }

    public function handleField(FieldValue $fieldValue): void
    {
        $fieldValue->wrapResolver(fn (callable $previousResolver) => function ($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
            $scope = $this->directiveArgValue('scope');
            // Throw in case of an invalid schema definition to remind the developer
            if ($scope === null) {
                throw new DefinitionException("Missing argument 'scope' for directive '@hasScope'.");
            }

            $user = $context->user();
            if (
                // Unauthenticated users don't get to see anything
                ! $user
                // The user's role has to match have the required role
                || ! $user->tokenCan($scope)
            ) {
                return null;
            }

            return $previousResolver($root, $args, $context, $resolveInfo);
        });
    }
}
