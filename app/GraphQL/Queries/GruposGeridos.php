<?php

namespace Sautor\Core\GraphQL\Queries;

use Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Sautor\Core\Models\Grupo;

class GruposGeridos
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue  Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args  The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context  Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo  Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $user = Auth::user();
        $grupos = Grupo::all()->filter(function ($g) use ($user) {
            return $g->isManagedBy($user);
        });
        if (@$args['topLevel']) {
            $ids = $grupos->pluck('id')->toArray();
            $grupos = $grupos->filter(function ($g) use ($ids) {
                return ! in_array($g->grupo_pai, $ids);
            });
        }

        return $grupos;
    }
}
