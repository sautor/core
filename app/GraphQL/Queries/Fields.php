<?php

namespace Sautor\Core\GraphQL\Queries;

use Sautor\Core\Models\Grupo;

class Fields
{
    public function resolveGrupoInscritos(Grupo $grupo, array $args)
    {
        $query = $grupo->inscritos();
        if (array_key_exists('term', $args)) {
            $query = $query->where('nome', 'LIKE', '%'.implode('%', explode(' ', $args['term'])).'%');
        }

        return $query->get();
    }
}
