<?php

namespace Sautor\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\Addons\AddonService;

class AddonsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('AddonService', function ($app) {
            return new AddonService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
