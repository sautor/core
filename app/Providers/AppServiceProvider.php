<?php

namespace Sautor\Core\Providers;

use Cron\CronExpression;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Sautor\Core\Console\Commands\StorageLinkCommand;
use Sautor\Core\Models\Grupo;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_ALL, 'pt_PT.UTF-8', 'pt_PT.utf8', 'pt_PT', 'ptg_prt.UTF-8', 'ptg_prt.utf8', 'ptg_prt', 'ptg');
        View::composer('*', function ($view) {
            $bodyClass = $view->getData()['bodyClass'] ?? [];
            $bodyClass[] = 'v-'.str_replace('.', '-', str_replace('::', '_', $view->name()));
            $view->with('bodyClass', $bodyClass);
        });
        View::composer(
            'layouts.group-admin', 'Sautor\Core\Http\ViewComposers\GrupoComposer'
        );
        View::composer(
            'layouts.auth', 'Sautor\Core\Http\ViewComposers\AuthLayoutComposer'
        );
        $this->registerValidationRules();

        \Blade::if('manages', function (?Grupo $grupo = null) {
            return $grupo && \Auth::user() && $grupo->isManagedBy(\Auth::user());
        });

        Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            $splitTerm = explode(' ', $searchTerm);
            $this->where(function (Builder $query) use ($attributes, $splitTerm) {
                foreach (Arr::wrap($attributes) as $attribute) {
                    $query->when(
                        str_contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $splitTerm) {
                            [$relationName, $relationAttribute] = explode('.', $attribute);

                            $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $splitTerm) {
                                foreach ($splitTerm as $term) {
                                    $query->where($relationAttribute, 'LIKE', "%{$term}%");
                                }
                            });
                        },
                        function (Builder $query) use ($attribute, $splitTerm) {
                            $query->orWhere(function (Builder $query) use ($attribute, $splitTerm) {
                                foreach ($splitTerm as $term) {
                                    $query->where($attribute, 'LIKE', "%{$term}%");
                                }
                            });
                        }
                    );
                }
            });

            return $this;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Passport::ignoreMigrations();

        if (config('app.debug')) {
            if (class_exists('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider')) {
                $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
            }
            if (class_exists('Barryvdh\Debugbar\ServiceProvider')) {
                $this->app->register('Barryvdh\Debugbar\ServiceProvider');
            }
            if (class_exists('Laracademy\Commands\MakeServiceProvider')) {
                $this->app->register('Laracademy\Commands\MakeServiceProvider');
            }
        }

        if ($this->app->runningInConsole()) {
            $this->app->extend('command.storage.link', function () {
                return new StorageLinkCommand;
            });
        }
    }

    private function registerValidationRules(): void
    {
        Validator::extend('cron', function ($attribute, $value, $parameters, $validator) {
            return CronExpression::isValidExpression($value);
        });
        Validator::extend('base64', function ($attribute, $value, $parameters, $validator) {
            if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $value)) {
                return true;
            } else {
                return false;
            }
        });
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'jpeg'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                [
                    '', '', '',
                ],
                $explode[0]
            );
            // check file format
            if (! in_array($format, $allow)) {
                return false;
            }
            // check base64 format
            if (! preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                return false;
            }

            return true;
        });
    }
}
