<?php

namespace Sautor\Core\Providers;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Validated;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Sautor\Core\Listeners\TwoFactorListener;
use Sautor\Core\Models\Evento;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Noticia;
use Sautor\Core\Models\Pagina;
use Sautor\Core\Models\Pessoa;
use Sautor\Core\Policies\EventosPolicy;
use Sautor\Core\Policies\GruposPolicy;
use Sautor\Core\Policies\NoticiasPolicy;
use Sautor\Core\Policies\PaginasPolicy;
use Sautor\Core\Policies\PessoasPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Grupo::class => GruposPolicy::class,
        Pessoa::class => PessoasPolicy::class,
        Noticia::class => NoticiasPolicy::class,
        Pagina::class => PaginasPolicy::class,
        Evento::class => EventosPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Repository $config, Dispatcher $dispatcher)
    {
        $this->registerPolicies();

        $this->registerListener($config, $dispatcher);

        Passport::enableImplicitGrant();

        Passport::tokensCan([
            'perfil' => 'Ver o seu perfil',
            'grupos' => 'Ver e gerir os seus grupos',
            'noticias' => 'Gerir as notícias do site',
            'paginas' => 'Gerir as páginas do site',
            'eventos' => 'Gerir os eventos do site',
        ]);
    }

    /**
     * Register a listeners to tackle authentication.
     *
     * @return void
     */
    protected function registerListener(Repository $config, Dispatcher $dispatcher)
    {
        $this->app->singleton(TwoFactorListener::class, function ($app) {
            return new TwoFactorListener($app['config'], $app['request']);
        });

        $dispatcher->listen(Attempting::class, TwoFactorListener::class.'@saveCredentials');
        $dispatcher->listen(Validated::class, TwoFactorListener::class.'@checkTwoFactor');
    }
}
