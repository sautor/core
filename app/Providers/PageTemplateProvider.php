<?php

namespace Sautor\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\PageTemplates\PageTemplate;
use Sautor\Core\Services\PageTemplates\PageTemplateService;

class PageTemplateProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('PageTemplateService', function ($app) {
            return new PageTemplateService;
        });

        \PageTemplateService::register(
            PageTemplate::create('grupos', 'Grupos', PageTemplate::MAIN, 'pages.templates.groups', 'Lista de grupos')
                ->setOptions([
                    ['type' => 'switch', 'label' => 'Mostrar em lista', 'name' => 'list'],
                ])
                ->setNoContent(true)
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
