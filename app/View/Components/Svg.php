<?php

namespace Sautor\Core\View\Components;

use Illuminate\View\Component;

class Svg extends Component
{
    /**
     * The SVG file path.
     *
     * @var string
     */
    public $file;

    /**
     * Create a new component instance.
     *
     * @param  string  $file
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $content = file_get_contents($this->file);

        return $content;
    }
}
