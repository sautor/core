<?php

namespace Sautor\Core\View\Components;

use Illuminate\View\Component;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Models\Pagina;

class Menu extends Component
{
    /**
     * The group to render the menu for.
     */
    public ?Grupo $group = null;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($group = null)
    {
        $this->group = $group;
    }

    private function getMainMenu()
    {
        $menu = \Setting::get('menu');
        if (! empty($menu)) {
            $menu = json_decode($menu);
        }

        return $menu;
    }

    private function getAddons()
    {
        return \AddonService::forMenu();
    }

    private function getGroupMenu()
    {
        $isManagedByUser = \Auth::user() ? $this->group->isManagedBy(\Auth::user()) : null;
        $menu = [];

        if (! (empty($this->group->descricao) && empty($this->group->email) && empty($this->group->url))) {
            $menu[] = (object) [
                'label' => 'Início',
                'url' => $this->group->route('index'),
            ];
        }

        if ($this->group->showChildren) {
            $menu[] = (object) [
                'label' => \Sautor\pluralize($this->group->designacao_grupo),
                'badge' => $isManagedByUser ? $this->group->grupos()->count() : null,
                'url' => $this->group->route('grupos'),
            ];
        }

        $menu_setting = \Setting::get('grupo-'.$this->group->id.'-menu', null);
        if (! empty($menu_setting)) {

            if (str_starts_with($menu_setting, '[')) {
                $menu = array_merge($menu, json_decode($menu_setting));
            } else {
                $page_ids = explode(',', $menu_setting);
                $paginas = $this->group->paginas()->whereIn('id', $page_ids)->get();
                foreach ($paginas as $pagina) {
                    if (\Gate::allows('view', $pagina)) {
                        $menu[] = (object) [
                            'label' => $pagina->titulo,
                            'url' => $this->group->route('paginas.show', $pagina->slug),
                        ];
                    }
                }
            }
        }

        $menu[] = (object) [
            'label' => 'Notícias',
            'url' => $this->group->route('noticias'),
        ];

        return $menu;
    }

    private function getGroupAddons()
    {
        return \AddonService::forGroupMenu($this->group, false);
    }

    private function sanitizeMenuItem($item, $level = 0)
    {
        if (! empty($item->pagina_id)) {
            $pagina_query = Pagina::where('id', $item->pagina_id);
            if (! empty($this->group)) {
                $pagina_query = $pagina_query->where('grupo_id', $this->group->id);
            }
            $pagina = $pagina_query->first();
            if (! $pagina && ! \Gate::allows('view', $pagina)) {
                return null;
            }

            if ($level < 1) {
                $filhos = $pagina->filhos->filter(fn ($pg) => \Gate::allows('view', $pg));

                if ($filhos->isNotEmpty()) {
                    return (object) [
                        'label' => $item->label !== '' ? $item->label : $pagina->titulo,
                        'children' => $filhos->map(fn ($pg) => (object) [
                            'label' => $pg->titulo,
                            'url' => empty($this->group) ? route('paginas.show', $pg->slug) : $pg->grupo->route('paginas.show', $pg->slug),
                        ]),
                    ];
                }
            }

            return (object) [
                'label' => $item->label !== '' ? $item->label : $pagina->titulo,
                'url' => empty($this->group) ? route('paginas.show', $pagina->slug) : $pagina->grupo->route('paginas.show', $pagina->slug),
            ];
        }

        if (! empty($item->submenu)) {
            $item->submenu = $this->sanitizeMenu($item->submenu, $level + 1);
        }

        return $item;
    }

    private function sanitizeMenu($menu, $level = 0)
    {
        return array_filter(array_map(fn ($item) => $this->sanitizeMenuItem($item, $level), $menu));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $data = $this->group ? [
            'menu' => $this->getGroupMenu(),
            'addons' => $this->getGroupAddons(),
        ] : [
            'menu' => $this->getMainMenu(),
            'addons' => $this->getAddons(),
        ];

        $data['menu'] = $this->sanitizeMenu($data['menu']);

        return view('components.menu', $data);
    }
}
