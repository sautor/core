<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Pagina extends Model
{
    use Searchable;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'publico' => 'boolean',
        'options' => 'array',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['grupo'];

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function pai()
    {
        return $this->belongsTo(self::class, 'pagina_pai');
    }

    public function filhos()
    {
        return $this->hasMany(self::class, 'pagina_pai');
    }

    public function getTemplateAttribute()
    {
        if (! $this->page_template_id) {
            return null;
        }

        return \PageTemplateService::get($this->page_template_id);
    }

    public function toSearchableArray()
    {
        return $this->only(['id', 'titulo', 'conteudo']);
    }

    public function scopeNoGroup(Builder $query)
    {
        return $query->whereNull('grupo_id');
    }
}
