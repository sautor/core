<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class Noticia extends Model
{
    use Searchable;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['excerto'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'datetime',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['grupo'];

    public function getExcertoAttribute()
    {
        return Str::words(strip_tags($this->conteudo), 20);
    }

    public function autor()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function toSearchableArray()
    {
        return $this->only('id', 'titulo', 'conteudo');
    }

    public function showRoute()
    {
        return ($this->grupo && $this->grupo->dominio && $this->grupo->dominio_confirmado) ? route('grupo-dominio.noticia', [$this->grupo, $this]) : route('noticias.show', $this);
    }
}
