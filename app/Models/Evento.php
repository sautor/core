<?php

namespace Sautor\Core\Models;

use Carbon\Carbon;
use Cron\CronExpression;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'exceto' => 'array',
        'visivel_no_inicio' => 'boolean',
        'from' => 'datetime',
        'to' => 'datetime',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    private function dateInExcept(Carbon $date)
    {
        if (! is_array($this->exceto)) {
            return false;
        }
        if (in_array($date->format('Y-m-d'), $this->exceto)) {
            return true;
        }
        foreach (array_filter($this->exceto, 'is_array') as $interval) {
            if (strtotime($interval[0]) <= $date->timestamp && $date->timestamp <= strtotime($interval[1].' 23:59:59')) {
                return true;
            }
        }

        return false;
    }

    public function runDatesBetween(Carbon $start, Carbon $end)
    {
        $ahead = 0;
        if (($this->to && $start->isAfter($this->to)) || ($this->from && $end->isBefore($this->from))) {
            return [];
        }
        $dates = [];
        if ($this->from && $start->isBefore($this->from)) {
            $start = $this->from->copy();
        }
        if ($this->to && $end->isAfter($this->to)) {
            $end = $this->to->copy();
        }
        $cron = CronExpression::factory($this->cron);
        $init = Carbon::instance($cron->getNextRunDate($start, $ahead));
        while ($init->lte($end)) {
            if (! $this->dateInExcept($init) && (! $this->ano || $this->ano == $init->year)) {
                $dates[] = $init;
            }
            $ahead++;
            $init = Carbon::instance($cron->getNextRunDate($start, $ahead));
        }

        return $dates;
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }
}
