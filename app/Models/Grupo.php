<?php

namespace Sautor\Core\Models;

use Database\Factories\GrupoFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;
use SVG\SVG;

class Grupo extends Model
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['logo', 'logo_horizontal', 'cor'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['pai'];

    public function getLogoAttribute()
    {
        return \Setting::get('grupo-'.$this->id.'-logo');
    }

    public function getLogoHorizontalAttribute()
    {
        return \Setting::get('grupo-'.$this->id.'-logo-horizontal');
    }

    public function getIconAttribute()
    {
        return \Setting::get('grupo-'.$this->id.'-icon');
    }

    public function getCorAttribute()
    {
        $setting = \Setting::get('grupo-'.$this->id.'-color');
        if ($setting) {
            return $setting;
        }
        if (! $this->logo) {
            return null;
        }
        $cache_key = md5($this->logo);
        $cache_setting = \Setting::get('logo-color-'.$cache_key);
        if ($cache_setting) {
            return $cache_setting;
        }
        $isSvg = str_contains($this->logo, '.svg');
        if ($isSvg) {
            $image = SVG::fromFile($this->logo[0] === '/' ? public_path($this->logo) : $this->logo);
            $gdImage = $image->toRasterImage(200, 200);
        } else {
            $gdImage = imagecreatefrompng($this->logo[0] === '/' ? public_path($this->logo) : $this->logo);
        }

        $palette = Palette::fromGD($gdImage);
        $extractor = new ColorExtractor($palette);
        $colors = $extractor->extract();
        $color = Color::fromIntToHex($colors[0]);

        if (in_array($color, ['#000000', '#ffffff'])) {
            return null;
        }

        \Setting::set('logo-color-'.$cache_key, $color);
        \Setting::save();

        return $color;
    }

    public function getNomeCurtoAttribute($value)
    {
        if (empty($value)) {
            return $this->nome;
        }

        return $value;
    }

    public function getDesignacaoGrupoAttribute($value)
    {
        if (empty($value)) {
            return 'Grupo';
        }

        return $value;
    }

    public function getDesignacaoResponsavelAttribute($value)
    {
        if (empty($value)) {
            return 'Responsável';
        }

        return $value;
    }

    public function getShowChildrenAttribute()
    {
        return \Setting::get('grupo-'.$this->id.'-show-children', false) && $this->grupos()->count() > 0;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function inscricoes()
    {
        return $this->hasMany(Inscricao::class)
            ->where(function ($q) {
                $q->where('ano_letivo', \Sautor\anoLetivo())
                    ->orWhereNull('ano_letivo');
            });
    }

    public function inscritos()
    {
        return $this->belongsToMany(Pessoa::class, 'inscricoes')
            ->using(Inscricao::class)
            ->as('inscricao')
            ->withPivot(Inscricao::COLUMNS)
            ->withTimestamps()
            ->where(function ($q) {
                $q->where('inscricoes.ano_letivo', \Sautor\anoLetivo())
                    ->orWhere('inscricoes.ano_letivo', null);
            })
            ->whereNull('inscricoes.deleted_at')
            ->orderBy('inscricoes.responsavel')
            ->orderBy('nome');
    }

    public function inscritosNaoResponsaveis()
    {
        return $this->inscritos()
            ->wherePivot('responsavel', false);
    }

    public function responsaveis()
    {
        return $this->inscritos()
            ->wherePivot('responsavel', true);
    }

    public function pai()
    {
        return $this->belongsTo(Grupo::class, 'grupo_pai');
    }

    public function grupos()
    {
        return $this->hasMany(Grupo::class, 'grupo_pai')->where('visibilidade', '!=', 'oculto')
            ->orderBy('ordem')->orderBy('nome');
    }

    public function gruposOcultos()
    {
        return $this->hasMany(Grupo::class, 'grupo_pai')->where('visibilidade', 'oculto')
            ->orderBy('ordem')->orderBy('nome');
    }

    public function gruposEliminados()
    {
        return $this->hasMany(Grupo::class, 'grupo_pai')->onlyTrashed()
            ->orderBy('ordem')->orderBy('nome');
    }

    public function paginas()
    {
        return $this->hasMany(Pagina::class);
    }

    public function noticias()
    {
        return $this->hasMany(Noticia::class);
    }

    public function isManagedBy(?Pessoa $user)
    {
        if (! $user) {
            return false;
        }

        if ($user->can('gerir grupos')) {
            return true;
        }

        $grupo = $this;
        do {
            foreach ($grupo->responsaveis as $resp) {
                /* @var $resp Pessoa */
                if ($resp->is($user)) {
                    return true;
                }
            }
        } while ($grupo = $grupo->pai);

        return false;
    }

    public function getSiteGroupAttribute()
    {
        $setting = \Setting::get('grupo-'.$this->id.'-microsite', false);
        if ($setting) {
            return $this;
        }

        return $this->pai ? $this->pai->siteGroup : null;
    }

    public function toSearchableArray()
    {
        return $this->only(['id', 'nome', 'nome_curto', 'descricao', 'slug']);
    }

    public function route($name, ...$parameters)
    {
        if ($this->dominio && $this->dominio_confirmado) return route('grupo-dominio.'.$name, [$this, ...$parameters]);

        $route = route('grupo.'.$name, [$this, ...$parameters]);
        $site_group = $this->siteGroup;

        if ($site_group && $site_group->dominio && $site_group->dominio_confirmado) {
            // Replace domain with site_group domain
            $url_components = parse_url($route);
            return str_replace($url_components['host'], $site_group->dominio, $route);
        }

        return $route;
    }

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return GrupoFactory::new();
    }
}
