<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Passo extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'datetime',
    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }
}
