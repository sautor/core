<?php

namespace Sautor\Core\Models;

use Database\Factories\PessoaFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laragear\TwoFactor\Contracts\TwoFactorAuthenticatable;
use Laragear\TwoFactor\TwoFactorAuthentication;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use Propaganistas\LaravelPhone\Casts\RawPhoneNumberCast;
use Sautor\Core\Notifications\RedefinirPassword;
use Spatie\Permission\Traits\HasRoles;

class Pessoa extends Authenticatable implements TwoFactorAuthenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use Notifiable;
    use Searchable;
    use TwoFactorAuthentication;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data_nascimento' => 'datetime',
        'telefone' => RawPhoneNumberCast::class.':INTERNATIONAL,PT',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['remember_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['nome_curto', 'nome_exibicao'];

    const ORDEM_LABELS = [
        'sacerdote' => 'Padre',
        'diacono' => 'Diácono',
    ];

    const ORDEM_LABELS_SHORT = [
        'sacerdote' => 'Pe.',
        'diacono' => 'Diác.',
    ];

    public function getNomeExibicaoAttribute()
    {
        if ($this->ordem) {
            return self::ORDEM_LABELS[$this->ordem].' '.$this->nome;
        }

        return $this->nome;
    }

    public function getNomeCurtoAttribute()
    {
        $nome = $this->nome;
        $anome = explode(' ', $nome);
        if (count($anome) <= 2) {
            return $nome;
        }
        $nomec = $anome[0].' '.array_pop($anome);

        if ($this->ordem) {
            return self::ORDEM_LABELS_SHORT[$this->ordem].' '.$nomec;
        }

        return $nomec;
    }

    public function getFotoAttribute($value)
    {
        if ($value) {
            if (Str::startsWith($value, '/')) {
                return url($value);
            }

            return $value;
        }

        return 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($this->email))).'?d=identicon&s=250';
    }

    public function getIdadeAttribute()
    {
        if (empty($this->data_nascimento)) {
            return null;
        }

        return floor($this->data_nascimento->diffInYears());
    }

    public function inscricoes()
    {
        return $this->hasMany(Inscricao::class)->where('ano_letivo', \Sautor\anoLetivo())->orWhereNull('ano_letivo');
    }

    public function grupos()
    {
        return $this->belongsToMany(Grupo::class, 'inscricoes')
            ->using(Inscricao::class)
            ->as('inscricao')
            ->withPivot(Inscricao::COLUMNS)
            ->whereNull('inscricoes.deleted_at')
            ->where(function ($q) {
                $q->where('inscricoes.ano_letivo', \Sautor\anoLetivo())->orWhereNull('inscricoes.ano_letivo');
            })
            ->orderBy('ordem')
            ->orderBy('nome');
    }

    public function historial()
    {
        return $this->belongsToMany(Grupo::class, 'inscricoes')
            ->using(Inscricao::class)
            ->as('inscricao')
            ->withPivot(Inscricao::COLUMNS)
            ->where(function ($q) {
                $q->where('inscricoes.ano_letivo', '<', \Sautor\anoLetivo())
                    ->orWhereNotNull('inscricoes.deleted_at');
            })
            ->orderByDesc('inscricoes.ano_letivo')
            ->orderBy('ordem')
            ->orderBy('nome');
    }

    public function isInGrupo(Grupo $grupo)
    {
        return $this->grupos()->where('grupo_id', $grupo->id)->exists();
    }

    public function relacoesAscendentes()
    {
        return $this->hasMany(Relacao::class, 'pessoa2_id');
    }

    public function ascendentes()
    {
        return $this->belongsToMany(Pessoa::class, 'relacoes', 'pessoa2_id', 'pessoa1_id')
            ->using(Relacao::class)
            ->as('relacao')
            ->withPivot(Relacao::COLUMNS)
            ->orderBy('nome');
    }

    public function relacoesDescendentes()
    {
        return $this->hasMany(Relacao::class, 'pessoa1_id');
    }

    public function descendentes()
    {
        return $this->belongsToMany(Pessoa::class, 'relacoes', 'pessoa1_id', 'pessoa2_id')
            ->using(Relacao::class)
            ->as('relacao')
            ->withPivot(Relacao::COLUMNS)
            ->orderBy('nome');
    }

    public function irmaos()
    {
        $paisIds = $this->relacoesAscendentes()->where('relacao', 'pai')->select('pessoa1_id')->get();
        $irmaosIds = Relacao::whereIn('pessoa1_id', $paisIds)->where('pessoa2_id', '!=', $this->id)->select('pessoa2_id')->distinct()->get();

        return Pessoa::findMany($irmaosIds);
    }

    public function familiares()
    {
        return $this->ascendentes->concat($this->irmaos())->concat($this->descendentes);
    }

    public function passos()
    {
        return $this->hasMany(Passo::class)->orderBy('data');
    }

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function consentimentos()
    {
        return $this->hasMany(Consentimento::class);
    }

    public function consentimentosAssinados()
    {
        return $this->hasMany(Consentimento::class, 'assinado_por_id');
    }

    public function allConsentimentos()
    {
        return $this->consentimentos->merge($this->consentimentosAssinados)->sortBy('created_at');
    }

    public function hasActiveConsent()
    {
        return $this->consentimentos()->active()->exists() or $this->consentimentosAssinados()->active()->exists();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new RedefinirPassword($token));
    }

    public function toSearchableArray()
    {
        return $this->only(['id', 'nome', 'nome_exibicao', 'nome_curto', 'email', 'morada', 'telefone', 'data_nascimento', 'profissao', 'nic', 'nus', 'nif']);
    }

    protected function getDefaultGuardName(): string
    {
        return 'web';
    }

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return PessoaFactory::new();
    }
}
