<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Redirection extends Model
{
    protected $table = 'redirections';

    public $timestamps = true;

    protected $fillable = [
        'old_url',
        'new_url',
        'status_code',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(static function (self $model) {
            if (strtolower(trim($model->old_url, '/')) === strtolower(trim($model->new_url, '/'))) {
                throw new \Exception('Os URLs não podem ser iguais.');
            }

            // to prevent a redirection loop, we delete all that conflicting url
            static::whereOldUrl($model->new_url)->whereNewUrl($model->old_url)->delete();

            $model->syncOldRedirects($model, $model->new_url);
        });
    }

    /**
     * Get all redirect statuses defined inside the "config/redirects.php" file.
     */
    public static function getStatuses(): array
    {
        return [
            301 => 'Permanente (301)',
            302 => 'Normal (302)',
            307 => 'Temporário (307)',
        ];
    }

    /**
     * Filter the query by an old url.
     */
    public function scopeWhereOldUrl(Builder $query, string $url): Builder
    {
        return $query->where('old_url', strtolower($url));
    }

    /**
     * Filter the query by a new url.
     */
    public function scopeWhereNewUrl(Builder $query, string $url): Builder
    {
        return $query->where('new_url', strtolower($url));
    }

    /**
     * The mutator to set the "old_url" attribute.
     *
     * @returns void
     */
    public function setOldUrlAttribute(string $value): void
    {
        $value = trim(parse_url($value)['path'], '/');
        $this->attributes['old_url'] = strtolower($value);
    }

    /**
     * The mutator to set the "new_url" attribute.
     *
     * @returns void
     */
    public function setNewUrlAttribute(string $value): void
    {
        $value = trim(parse_url($value)['path'], '/');
        $this->attributes['new_url'] = strtolower($value);
    }

    /**
     * Sync old redirects to point to the new (final) url.
     */
    public function syncOldRedirects(Redirection $model, string $finalUrl): void
    {
        $items = static::whereNewUrl($model->old_url)->get();

        foreach ($items as $item) {
            $item->update(['new_url' => $finalUrl]);
            $item->syncOldRedirects($model, $finalUrl);
        }
    }

    /**
     * Return a valid redirect entity for a given path (old url).
     * A redirect is valid if:
     * - it has an url to redirect to (new url)
     * - it's status code is one of the statuses defined on this model.
     */
    public static function findValidOrNull(string $path): ?Redirection
    {
        $path = ($path === '/' ? $path : trim($path, '/'));

        return static::where('old_url', strtolower($path))
            ->whereNotNull('new_url')
            ->whereIn('status_code', array_keys(self::getStatuses()))
            ->latest()
            ->first();
    }
}
