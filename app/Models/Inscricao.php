<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscricao extends Pivot
{
    public const COLUMNS = [
        'id',
        'responsavel',
        'cargo',
        'ano_letivo',
        'deleted_at',
    ];

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inscricoes';

    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }

    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('ano_letivo', \Sautor\anoLetivo())->orWhereNull('ano_letivo');
    }
}
