<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Consentimento extends Model
{
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'revoked_at' => 'datetime',
    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function assinado_por()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function added_by()
    {
        return $this->belongsTo(Pessoa::class);
    }

    /**
     * Scope a query to only include active consents.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->whereNull('revoked_at');
    }
}
