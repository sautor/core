<?php

namespace Sautor\Core\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Relacao extends Pivot
{
    const COLUMNS = [
        'relacao',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'relacoes';

    const ASC_NAMES = [
        'pai' => ['M' => 'pai', 'F' => 'mãe'],
        'avo' => ['M' => 'avô', 'F' => 'avó'],
        'padrinho' => ['M' => 'padrinho', 'F' => 'madrinha'],
        'padrasto' => ['M' => 'padrasto', 'F' => 'madrasta'],
    ];

    const DESC_NAMES = [
        'pai' => ['M' => 'filho', 'F' => 'filha'],
        'avo' => ['M' => 'neto', 'F' => 'neta'],
        'padrinho' => ['M' => 'afilhado', 'F' => 'afilhada'],
        'padrasto' => ['M' => 'enteado', 'F' => 'enteada'],
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function ascendente()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa1_id');
    }

    public function descendente()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa2_id');
    }

    public function nomeFromAsc()
    {
        return self::DESC_NAMES[$this->relacao][$this->descendente->genero];
    }

    public function nomeFromDesc()
    {
        return self::ASC_NAMES[$this->relacao][$this->ascendente->genero];
    }

    public function nomeFrom(Pessoa $pessoa)
    {
        if ($pessoa->id === $this->pessoa1_id) {
            return $this->nomeFromAsc();
        }

        return $this->nomeFromDesc();
    }
}
