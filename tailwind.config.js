import filamentPreset from './vendor/filament/support/tailwind.config.preset'

import preset from './tailwind.preset.js'
import forms from '@tailwindcss/forms'
import typography from '@tailwindcss/typography'

/** @type {import('tailwindcss').Config} */
export default {
  presets: [filamentPreset, preset],
  darkMode: 'class',
  corePlugins: {
    preflight: true
  },
  plugins: [
    forms,
    typography
  ]
}
